# RGF 编程接口(API)

-- 窗口相关 --

1. [RgfCore](./api/window/RgfCore.md)(核心窗口)
2. [RgfBase](./api/window/RgfBase.md)(基础窗口)

-- 渲染相关(设备无关资源) --

1. [MemBitmap](./api/devIndepRes/MemBitmap.md)(内存位图)
2. [Matrix](./api/devIndepRes/Matrix.md)(矩阵)

-- 渲染相关(设备相关资源) --

1. [Image](./api/devRes/Image.md)(图像)
2. [Bitmap](./api/devRes/Bitmap.md)(位图)
3. [Layer](./api/devRes/Layer.md)(图层)
4. [Brush](./api/devRes/Brush.md)(画刷)
5. [BitmapBrush](./api/devRes/BitmapBrush.md)(位图画刷)
6. [LinearGradientBrush](./api/devRes/LinearGradientBrush.md)(线性渐变画刷)
7. [SolidColorBrush](./api/devRes/SolidColorBrush.md)(纯色画刷)
8. [Path](./api/devRes/Path.md)(路径)
9. [StrokeStyle](./api/devRes/StrokeStyle.md)(线段风格)
10. [TextCore](./api/devRes/TextCore.md)(文本渲染核心)
11. [TextInlineObj](./api/devRes/TextInlineObj.md)(文本内联对象)
12. [TextFormat](./api/devRes/TextFormat.md)(文本格式)
13. [TextLayout](./api/devRes/TextLayout.md)(文本布局)

-- 渲染表面 --

1. [RgfRenderer](./api/renderer/RgfRenderer.md)(渲染表面)
2. [RgfBindRenderer](./api/renderer/RgfBindRenderer.md)(绑定式渲染表面)

-- 接口 --

1. [IDrawContent](./api/interface/IDrawContent.md)(绘制内容接口)
2. [IDrawCreater](./api/interface/IDrawCreater.md)(绘制创建者接口)
3. [IEventMethod](./api/interface/IEventMethod.md)(事件方法接口)
3. [IRgfBaseMethod](./api/interface/IRgfBaseMethod.md)(窗口基本方法接口)

-- 工具 --

1. [RgfColorUtils](./api/utils/RgfColorUtils.md)(色彩运算)
2. [RgfRectUtils](./api/utils/RgfRectUtils.md)(矩形运算)

-- 结构 -- 

1. RgfSize(尺寸)
2. RgfRect(矩形)
3. RgfPoint(点位)
4. RgfFRect(浮点矩形)
5. RgfFColor(浮点色彩)
6. RgfFPoint(浮点点位)
7. Rgf3X3Matrix(3X3矩阵)
8. RgfPctColor(百分比色彩)
9. RgfContext(RGF上下文)
10. RgfWinContext(窗口上下文)