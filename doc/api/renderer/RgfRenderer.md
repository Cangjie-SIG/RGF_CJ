# RgfRenderer <: [IDrawContent](../interface/IDrawContent.md) & [IDrawCreater](../interface/IDrawCreater.md)

## 成员方法

### 设置矩阵
```cangjie
/**
*  @brief 设置矩阵
*  @param transform 矩阵
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setTransform(transform:Matrix):Unit
```

### 获取矩阵
```cangjie
/**
*  @brief 获取矩阵
*  @param transform 矩阵
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getTransform(transform:Matrix):Unit
```

### 设置抗锯齿模式
```cangjie
/**
*  @brief 设置抗锯齿模式
*  @param antialias 抗锯齿模式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setAntialiasMode(antialias:RgfAntialiasMode):Unit
```

### 获取抗锯齿模式
```cangjie
/**
*  @brief 获取抗锯齿模式
*  @return 抗锯齿模式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getAntialiasMode():RgfAntialiasMode
```

### 保存当前渲染内容到文件中
```cangjie
/**
*  @brief 保存当前渲染内容到文件中
*  @param left   左边
*  @param top    顶边
*  @param width  宽度
*  @param height 高度
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(left:Int32, top:Int32, width:Int32, height:Int32,uri:String, format:RgfImgFormat):Bool
```

### 保存当前渲染内容到文件中
```cangjie
/**
*  @brief 保存当前渲染内容到文件中
*  @param rect   矩形
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(rect:RgfRect,uri:String, format:RgfImgFormat):Bool
```

### 保存当前渲染内容到文件中
```cangjie
/**
*  @brief 保存当前渲染内容到文件中
*  @param rect   浮点矩形
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(rect:RgfFRect,uri:String, format:RgfImgFormat):Bool
```

### 保存当前渲染内容到内存位图中
```cangjie
/**
*  @brief 保存当前渲染内容到内存位图中
*  @param ref    内存位图
*  @param left   左边
*  @param top    顶边
*  @param width  宽度
*  @param height 高度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,left:Int32, top:Int32, width:Int32, height:Int32):Bool
```

### 保存当前渲染内容到内存位图中
```cangjie
/**
*  @brief 保存当前渲染内容到内存位图中
*  @param ref    内存位图
*  @param rect   矩形
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,rect:RgfRect):Bool
```

### 保存当前渲染内容到内存位图中
```cangjie
/**
*  @brief 保存当前渲染内容到内存位图中
*  @param ref    内存位图
*  @param rect   浮点矩形
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,rect:RgfFRect):Bool
```

### 保存 位图内容 到文件中
```cangjie
/**
*  @brief 保存 位图内容 到文件中
*  @param left   左边
*  @param top    顶边
*  @param width  宽度
*  @param height 高度
*  @param bitmap 位图
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(left:Int32, top:Int32, width:Int32, height:Int32,bitmap:Bitmap,uri:String, format:RgfImgFormat):Bool
```

### 保存 位图内容 到文件中
```cangjie
/**
*  @brief 保存 位图内容 到文件中
*  @param rect   矩形
*  @param bitmap 位图
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(rect:RgfRect,bitmap:Bitmap,uri:String, format:RgfImgFormat):Bool
```

### 保存 位图内容 到文件中
```cangjie
/**
*  @brief 保存 位图内容 到文件中
*  @param rect   浮点矩形
*  @param bitmap 位图
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(rect:RgfFRect,bitmap:Bitmap,uri:String, format:RgfImgFormat):Bool
```

### 保存 图层内容 到文件中
```cangjie
/**
*  @brief 保存 图层内容 到文件中
*  @param left   左边
*  @param top    顶边
*  @param width  宽度
*  @param height 高度
*  @param layer  图层
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(left:Int32, top:Int32, width:Int32, height:Int32,layer:Layer,uri:String, format:RgfImgFormat):Bool
```

### 保存 图层内容 到文件中
```cangjie
/**
*  @brief 保存 图层内容 到文件中
*  @param rect   矩形
*  @param layer  图层
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(rect:RgfRect,layer:Layer,uri:String, format:RgfImgFormat):Bool
```

### 保存 图层内容 到文件中
```cangjie
/**
*  @brief 保存 图层内容 到文件中
*  @param rect   浮点矩形
*  @param layer  图层
*  @param uri    路径
*  @param format 格式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToFile(rect:RgfFRect,layer:Layer,uri:String, format:RgfImgFormat):Bool
```

### 保存 位图内容 到内存位图中
```cangjie
/**
*  @brief 保存 位图内容 到内存位图中
*  @param ref    内存位图
*  @param left   左边
*  @param top    顶边
*  @param width  宽度
*  @param height 高度
*  @param bitmap 位图
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,left:Int32, top:Int32, width:Int32, height:Int32,bitmap:Bitmap):Bool
```

### 保存 位图内容 到内存位图中
```cangjie
/**
*  @brief 保存 位图内容 到内存位图中
*  @param ref    内存位图
*  @param rect   矩形
*  @param bitmap 位图
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,rect:RgfRect,bitmap:Bitmap):Bool
```

### 保存 位图内容 到内存位图中
```cangjie
/**
*  @brief 保存 位图内容 到内存位图中
*  @param ref    内存位图
*  @param rect   浮点矩形
*  @param bitmap 位图
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,rect:RgfFRect,bitmap:Bitmap):Bool
```

### 保存 图层内容 到内存位图中
```cangjie
/**
*  @brief 保存 图层内容 到内存位图中
*  @param ref    内存位图
*  @param left   左边
*  @param top    顶边
*  @param width  宽度
*  @param height 高度
*  @param layer  图层
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,left:Int32, top:Int32, width:Int32, height:Int32,layer:Layer):Bool
```

### 保存 图层内容 到内存位图中
```cangjie
/**
*  @brief 保存 图层内容 到内存位图中
*  @param ref    内存位图
*  @param rect   矩形
*  @param layer  图层
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,rect:RgfRect,layer:Layer):Bool
```

### 保存 图层内容 到内存位图中
```cangjie
/**
*  @brief 保存 图层内容 到内存位图中
*  @param ref    内存位图
*  @param rect   浮点矩形
*  @param layer  图层
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note (会阻塞渲染)
*/
public func saveImgToMember(ref:MemBitmap,rect:RgfFRect,layer:Layer):Bool