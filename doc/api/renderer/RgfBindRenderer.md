
# RgfBindRenderer <: [RgfRenderer](./RgfRenderer.md)

## 成员方法

### 绑定
```cangjie
*  @param token       Rgf全局上下文(令牌)
*  @param hdc         设备上下文
*  @param width       宽度
*  @param height      高度
*  @param substrate   底层技术 (推荐:Direct2D)
*  @param dpiX        横向DPI
*  @param dpiY        纵向DPI
*  @return 是否成功
*/
public open func bind(token:RgfContext, hdc:RgfHdc, width:Int32, height:Int32, substrate!:RgfSubstrate,dpiX!:Float32, dpiY!:Float32):Bool
```

### 绑定
```cangjie
*  @param token       Rgf全局上下文(令牌)
*  @param hdc         设备上下文
*  @param size        尺寸
*  @param substrate   底层技术 (推荐:Direct2D)
*  @param dpiX        横向DPI
*  @param dpiY        纵向DPI
*  @return 是否成功
*/
public open func bind(token:RgfContext, hdc:RgfHdc, size:RgfSize, substrate!:RgfSubstrate,dpiX!:Float32, dpiY!:Float32):Bool
```

### 释放对象
```cangjie
*  @return 是否成功
*/
public override open func release():Bool
```

### 重置尺寸
```cangjie
*  @param width       宽度
*  @param height      高度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public open func reset (width:Int32 , height:Int32):Bool
```

### 重置尺寸
```cangjie
*  @param size        尺寸
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public open func reset (size:RgfSize):Bool
```

### 调用渲染
```cangjie
*  @param wRect     需要重绘的矩形 (留空表示全量重绘)
*  @return 是否成功调用
*  @note 调用成功并不代表渲染次数加一，为了确保渲染线程安全，当高并发渲染和窗口尺寸调整时，会抛弃跳过冲突的部分渲染，仅保留最近一次渲染请求等待渲染
*/
public open func draw (wRect:Option<RgfRect>):Bool
```

### 混合渲染
```cangjie
*  @param destX         目标左边
*  @param destY         目标顶边
*  @param destWidth     目标宽度
*  @param destHeight    目标高度
*  @param srcX          源左边
*  @param srcY          源顶边
*  @param srcWidth      源宽度
*  @param srcHeight     源高度
*  @param alpha         0.0~1.0 透明度
*  @return 是否成功
*/
public open func blend (destX:Int32 ,destY:Int32 ,destW:Int32 ,destH:Int32 ,srcX:Int32 ,srcY:Int32 ,srcW:Int32 ,srcH:Int32 , alpha!:Float32 ):Bool
```

### 混合渲染
```cangjie
*  @param destRect  目标矩形
*  @param srcRect   源矩形
*  @param alpha     0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note 本函数仅在调试模式下输出异常
*/
public open func blend (destRect:RgfRect, srcRect:RgfRect, alpha!:Float32 ):Bool
```

### 混合渲染
```cangjie
*  @param destRect  目标浮点矩形
*  @param srcRect   源浮点矩形
*  @param alpha     0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note 本函数仅在调试模式下输出异常
*/
public open func blend (destRect:RgfFRect, srcRect:RgfFRect, alpha!:Float32 ):Bool
```

## 类继承式回调


### 创建设备相关/渲染相关资源
```cangjie
/**
*  @note 本事件为核心生命周期，此事件有可能会在一个窗口周期中被多次调用，开发者应确保此处创建的资源被正确释放
*/
public open func createDeviceResources():Bool
```

### 绘制
```cangjie
/**
*  @param wRect     需要重绘的矩形
*  @note 当窗口需要重新绘制时，会通过本事件传递需要重绘的部分
*/
public open func onPaint(wRect:RgfRect):Unit
```

### 销毁设备相关/渲染相关资源
```cangjie
/**
*  @note 本事件为核心生命周期，此事件有可能会在一个窗口周期中被多次调用，开发者应在此处销毁核心生命周期中的设备资源
*/
public open func destroyDeviceResources():Bool
```

## 监听式回调

### 创建设备相关/渲染相关资源
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenCreateDeviceResources(callback:()->(Bool,Bool))
```

### 绘制
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenOnPaint(callback:(wRect:RgfRect)->Bool)
```

### 销毁设备相关/渲染相关资源
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenDestroyDeviceResources(callback:()->(Bool,Bool))
```