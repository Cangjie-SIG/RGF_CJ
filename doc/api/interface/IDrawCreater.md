# IDrawCreater

## 成员方法

### 创建纯色画刷
```cangjie
*  @param r         0.0~1.0 红
*  @param g         0.0~1.0 绿
*  @param b         0.0~1.0 蓝
*  @param a         0.0~1.0 透明度
*  @param width     线宽
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createSolidColorBrush(r:Float32 , g:Float32, b:Float32, a:Float32, width:Float32):SolidColorBrush
```

### 创建纯色画刷
```cangjie
*  @param rgba      色彩值
*  @param width     线宽
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createSolidColorBrush(color:RgfFColor, width:Float32):SolidColorBrush
```

### 创建纯色画刷
```cangjie
*  @param ref       对象引用
*  @param r         0.0~1.0 红
*  @param g         0.0~1.0 绿
*  @param b         0.0~1.0 蓝
*  @param a         0.0~1.0 透明度
*  @param width     线宽
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createSolidColorBrush(ref:SolidColorBrush,r:Float32 , g:Float32, b:Float32, a:Float32, width:Float32):Unit
```

### 创建纯色画刷
```cangjie
*  @param ref       对象引用
*  @param rgba      色彩值
*  @param width     线宽
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createSolidColorBrush(ref:SolidColorBrush,color:RgfFColor, width:Float32):Unit



```

### 创建线性渐变画刷
```cangjie
*  @param startColor            开始颜色
*  @param endColor              结束颜色
*  @param startPoint            开始点
*  @param endPoint              结束点
*  @param midPctColor           中间点与颜色
*  @param useGammaCorrection    是否启用伽马校正
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createLinearGradientBrush (
    startColor:RgfFColor, 
    endColor:RgfFColor, 
    startPoint:RgfFPoint, 
    endPoint:RgfFPoint,
    midPctColor!:Array<RgfPctColor>,
    useGammaCorrection!:Bool
):LinearGradientBrush
```

### 创建线性渐变画刷
```cangjie
*  @param ref                   对象引用
*  @param startColor            开始颜色
*  @param endColor              结束颜色
*  @param startPoint            开始点
*  @param endPoint              结束点
*  @param midPctColor           中间点与颜色
*  @param useGammaCorrection    是否启用伽马校正
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createLinearGradientBrush (
    ref:LinearGradientBrush,
    startColor:RgfFColor, 
    endColor:RgfFColor, 
    startPoint:RgfFPoint, 
    endPoint:RgfFPoint,
    midPctColor!:Array<RgfPctColor>,
    useGammaCorrection!:Bool
):Unit



```

### 创建位图画刷
```cangjie
*  @param img 位图
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createBitmapBrush (img:Image):BitmapBrush
```

### 创建位图画刷
```cangjie
*  @param ref 对象引用
*  @param img 位图
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createBitmapBrush (ref:BitmapBrush,img:Image):Unit



```

### 创建线段风格
```cangjie
*  @param startCap      开始线条帽样式
*  @param endCap        结束线条帽样式
*  @param dashCap       虚线条帽样式
*  @param lineJoin      线条联接样式
*  @param miterLimit    斜接长度
*  @param dashStyle     虚线样式
*  @param dashOffset    虚线偏移量
*  @param dashes        虚线段定义数组
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createStrokeStyle(
    startCap!:RgfCapStyle,
    endCap!:RgfCapStyle,
    dashCap!:RgfCapStyle,
    lineJoin!:RgfLineJoin,
    miterLimit!:Float32,
    dashStyle!:RgfDashStyle,
    dashOffset!:Float32,
    dashes!:Array<Float32>
):StrokeStyle
```

### 创建线段风格
```cangjie
*  @param ref           对象引用
*  @param startCap      开始线条帽样式
*  @param endCap        结束线条帽样式
*  @param dashCap       虚线条帽样式
*  @param lineJoin      线条联接样式
*  @param miterLimit    斜接长度
*  @param dashStyle     虚线样式
*  @param dashOffset    虚线偏移量
*  @param dashes        虚线段定义数组
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createStrokeStyle(
    ref:StrokeStyle,
    startCap!:RgfCapStyle,
    endCap!:RgfCapStyle,
    dashCap!:RgfCapStyle,
    lineJoin!:RgfLineJoin,
    miterLimit!:Float32,
    dashStyle!:RgfDashStyle,
    dashOffset!:Float32,
    dashes!:Array<Float32>
):Unit



```

### 创建位图
```cangjie
*  @param width  宽度
*  @param height 高度
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createBitmap(width:Int32, height:Int32):Bitmap
```

### 创建位图
```cangjie
*  @param size  尺寸
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createBitmap(size:RgfSize):Bitmap
```

### 创建位图
```cangjie
*  @param ref    对象引用
*  @param width  宽度
*  @param height 高度
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createBitmap(ref:Bitmap,width:Int32, height:Int32):Unit
```

### 创建位图
```cangjie
*  @param size  尺寸
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createBitmap(ref:Bitmap,size:RgfSize):Unit



```

### 创建位图(从文件)
```cangjie
*  @param uri   路径字符串
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createBitmapFromFile(uri:String):Bitmap
```

### 创建位图(从文件)
```cangjie
*  @param ref   对象引用
*  @param uri   路径字符串
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createBitmapFromFile(ref:Bitmap,uri:String):Unit



```

### 创建位图(从内存位图)
```cangjie
*  @param pMemBmp   内存位图
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createBitmapFromMemory (pMemBmp:MemBitmap):Bitmap
```

### 创建位图(从内存位图)
```cangjie
*  @param ref       对象引用
*  @param pMemBmp   内存位图
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createBitmapFromMemory (ref:Bitmap,pMemBmp:MemBitmap):Unit



```

### 创建路径
```cangjie
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createPath():Path
```

### 创建路径
```cangjie
*  @param ref       对象引用
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createPath(ref:Path):Unit
```



### 创建图层
```cangjie
*  @param width  宽度
*  @param height 高度
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createLayer (width:Int32, height:Int32):Layer
```

### 创建图层
```cangjie
*  @param size  尺寸
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createLayer (size:RgfSize):Layer
```

### 创建图层
```cangjie
*  @param ref    对象引用
*  @param width  宽度
*  @param height 高度
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createLayer (ref:Layer,width:Int32, height:Int32):Unit
```

### 创建图层
```cangjie
*  @param ref   对象引用
*  @param size  尺寸
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createLayer (ref:Layer,size:RgfSize):Unit



```

### 重置图层尺寸
```cangjie
*  @param layer         图层对象
*  @param width         宽度
*  @param height        高度
*  @param copyBitmap    是否拷贝源位图内容
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func resetLayerSize (layer:Layer,width:Int32, height:Int32, copyBitmap!:Bool):Bool
```

### 重置图层尺寸
```cangjie
*  @param layer         图层对象
*  @param size          尺寸
*  @param copyBitmap    是否拷贝源位图内容
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func resetLayerSize (layer:Layer,size:RgfSize, copyBitmap!:Bool):Bool



```

### 创建文本核心
```cangjie
*  @param gamma             伽马值
*  @param enhancedContrast  增强对比度 0.0~1.0
*  @param clearTypeLevel    清晰度级别(字体平滑) 0.0~1.0
*  @param renderingMode     渲染模式
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createTextCore(gamma!:Float32,enhancedContrast!:Float32,clearTypeLevel!:Float32,renderingMode!:RgfTxtRenderingMode):TextCore
```

### 创建文本核心
```cangjie
*  @param ref               对象引用
*  @param gamma             伽马值
*  @param enhancedContrast  增强对比度 0.0~1.0
*  @param clearTypeLevel    清晰度级别(字体平滑) 0.0~1.0
*  @param renderingMode     渲染模式
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createTextCore(ref:TextCore,gamma!:Float32,enhancedContrast!:Float32,clearTypeLevel!:Float32,renderingMode!:RgfTxtRenderingMode):Unit



```

### 创建文本行内对象
```cangjie
*  @param x         左边
*  @param y         顶边
*  @param width     宽度
*  @param height    高度
*  @param img       图像
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createTextInlineObject(x:Float32,y:Float32,width:Float32,height:Float32,img:Image):TextInlineObj
```

### 创建文本行内对象
```cangjie
*  @param rect  浮点矩形
*  @param img   图像
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createTextInlineObject(rect:RgfFRect, img:Image):TextInlineObj
```

### 创建文本行内对象
```cangjie
*  @param rect  矩形
*  @param img   图像
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createTextInlineObject(rect:RgfRect, img:Image):TextInlineObj
```

### 创建文本行内对象
```cangjie
*  @param ref       对象引用
*  @param x         左边
*  @param y         顶边
*  @param width     宽度
*  @param height    高度
*  @param img       图像
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createTextInlineObject(ref:TextInlineObj,x:Float32,y:Float32,width:Float32,height:Float32,img:Image):Unit
```

### 创建文本行内对象
```cangjie
*  @param ref   对象引用
*  @param rect  浮点矩形
*  @param img   图像
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createTextInlineObject(ref:TextInlineObj,rect:RgfFRect, img:Image):Unit
```

### 创建文本行内对象
```cangjie
*  @param ref   对象引用
*  @param rect  矩形
*  @param img   图像
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createTextInlineObject(ref:TextInlineObj,rect:RgfRect, img:Image):Unit



```

### 创建文本格式
```cangjie
*  @param size          尺寸
*  @param familyName    字体族
*  @param locale        区域设置名称
*  @param weight        字体粗细
*  @param style         字体样式
*  @param stretch       字体拉伸
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createTextFormat(size:Float32,familyName!:String,locale!:String,weight!:RgfTxtFontWeight,style!:RgfTxtFontStyle,stretch!:RgfTxtFontStretch):TextFormat;
```

### 创建文本格式
```cangjie
*  @param ref           对象引用
*  @param size          尺寸
*  @param familyName    字体族
*  @param locale        区域设置名称
*  @param weight        字体粗细
*  @param style         字体样式
*  @param stretch       字体拉伸
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createTextFormat(ref:TextFormat,size:Float32,familyName!:String,locale!:String,weight!:RgfTxtFontWeight,style!:RgfTxtFontStyle,stretch!:RgfTxtFontStretch):Unit;



```

### 创建文本布局
```cangjie
*  @param string    字符串内容
*  @param format    文本格式
*  @param maxWidth  最大宽度  
*  @param maxHeight 最大高度
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createTextLayout(string:String,format:TextFormat,maxWidth:Float32,maxHeight:Float32):TextLayout;
```

### 创建文本布局
```cangjie
*  @param string    字符串内容
*  @param format    文本格式
*  @param maxSize   最大尺寸 
*  @return 返回对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func createTextLayout(string:String,format:TextFormat,maxSize:RgfSize):TextLayout;
```

### 创建文本布局
```cangjie
*  @param ref       对象引用
*  @param string    字符串内容
*  @param format    文本格式
*  @param maxWidth  最大宽度  
*  @param maxHeight 最大高度
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createTextLayout(ref:TextLayout,string:String,format:TextFormat,maxWidth:Float32,maxHeight:Float32):Unit;
```

### 创建文本布局
```cangjie
*  @param ref       对象引用
*  @param string    字符串内容
*  @param format    文本格式
*  @param maxSize   最大尺寸 
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
*/
func createTextLayout(ref:TextLayout,string:String,format:TextFormat,maxSize:RgfSize):Unit;
```