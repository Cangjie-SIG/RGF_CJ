# IRgfBaseMethod

## 成员属性

### 窗口句柄
```cangjie
public prop hWnd:RgfHwnd
```

## 成员方法

### 获取客户区矩形
```cangjie
/**
*  @return RgfRect
*  @exception 若请求时窗口未创建，则抛出异常；若底层Windows api出现预期外结果，则抛出异常
*/
func getClientRect():RgfRect
```

### 获取窗口矩形
```cangjie
/**
*  @return RgfRect
*  @exception 若请求时窗口未创建，则抛出异常；若底层Windows api出现预期外结果，则抛出异常
*/
func getWindowRect():RgfRect
```

### 判断当前窗口是否为指定窗口的子窗口
```cangjie
/**
*  @param parent 父窗口核心类
*  @return 是否为子窗口
*  @exception 若请求时窗口或父窗口未创建，则抛出异常
*/
func isChild(parent:RgfCore):Bool
```

### 判断当前窗口是否为指定窗口的子窗口
```cangjie
/**
*  @param parent 父窗口接口
*  @return 是否为子窗口
*  @exception 若请求时窗口或父窗口未创建，则抛出异常
*/
func isChild(parent:IRgfBaseMethod):Bool
```

### 判断当前窗口是否为指定窗口的子窗口
```cangjie
/**
*  @param parent 父窗口句柄
*  @return 是否为子窗口
*  @exception 若请求时窗口或父窗口未创建，则抛出异常
*/
func isChild(parent:RgfHwnd):Bool
```

### 获取当前窗口的父窗口
```cangjie
/**
*  @return 父窗口句柄
*  @exception 若请求时窗口未创建，则抛出异常
*/
func getParent():RgfHwnd
```

### 为窗口设置新父窗口
```cangjie
/**
*  @param parent 父窗口类
*  @return 原父窗口句柄，失败则为 0
*  @exception 若请求时窗口或父窗口未创建，则抛出异常
*/
func setParent(parent:RgfCore):RgfHwnd
```

### 为窗口设置新父窗口
```cangjie
/**
*  @param parent 父窗口接口
*  @return 原父窗口句柄，失败则为 0
*  @exception 若请求时窗口或父窗口未创建，则抛出异常
*/
func setParent(parent:IRgfBaseMethod):RgfHwnd
```

### 为窗口设置新父窗口
```cangjie
/**
*  @param parent 父窗口句柄
*  @return 原父窗口句柄，失败则为 0
*  @exception 若请求时窗口或父窗口未创建，则抛出异常
*/
func setParent(parent:RgfHwnd):RgfHwnd
```

### 设置窗口获得焦点
```cangjie
/**
*  @return 原持有焦点的窗口句柄
*  @exception 若请求时窗口未创建，则抛出异常
*/
func setFocus():RgfHwnd
```

### 获取当前持有焦点的窗口
```cangjie
/**
*  @return 窗口句柄
*/
func getFocus():RgfHwnd
```

### 捕获鼠标
```cangjie
/**
*  @return 是否成功
*  @exception 若请求时窗口未创建，则抛出异常
*/
func mouseCapture():Bool
```

### 释放鼠标
```cangjie
/**
*  @return 是否成功
*  @exception 若请求时窗口未创建，则抛出异常
*/
func mouseRelease():Bool
```

### 设置监听鼠标悬停
```cangjie
/**
*  @param hoverTime 悬停触发时间
*  @param noNClient 是否为非客户区
*  @return 是否成功
*  @exception 若请求时窗口未创建，则抛出异常
*/
func mouseHover(hoverTime: UInt32):Bool
```

### 设置监听鼠标离开
```cangjie
/**
*  @brief 设置监听鼠标离开
*  @param use       是否监听
*  @param noNClient 是否为非客户区
*  @return 是否成功
*  @exception 若请求时窗口未创建，则抛出异常
*/
func mouseLeave(use: Bool,noNClient: Bool):Bool
```

### 设置窗口激活
```cangjie
/**
*  @return 原激活窗口句柄
*  @exception 若请求时窗口未创建，则抛出异常
*/
func setActiveWindow():RgfHwnd
```

### 获取当前激活的窗口句柄
```cangjie
/**
*  @return 窗口句柄
*/
func getActiveWindow():RgfHwnd
```

### 设置窗口置顶
```cangjie
/**
*  @param top 是否置顶
*  @return 是否成功
*  @exception 若请求时窗口未创建，则抛出异常
*/
func setWindowTop(top:Bool):Bool
```

### 设置窗口是否可用
```cangjie
/**
*  @param enable 是否可用
*  @exception 若请求时窗口未创建，则抛出异常
*/
func enable(enable:Bool):Unit
```

### 获取窗口是否可用
```cangjie
/**
*  @return 是否可用
*  @exception 若请求时窗口未创建，则抛出异常
*/
func isEnable():Bool
```

### 获取窗口是否可用
```cangjie
/**
*  @param show 是否显示
*  @exception 若请求时窗口未创建，则抛出异常
*/
func show(show:Bool):Unit
```

### 获取窗口是否显示
```cangjie
/**
*  @return 是否显示
*  @exception 若请求时窗口未创建，则抛出异常
*/
func isShow():Bool
```

### 移动窗口
```cangjie
/**
*  @param x         左边
*  @param y         顶边
*  @param nWidth    宽度
*  @param nHeight   高度
*  @return 是否成功
*  @exception 若请求时窗口未创建，则抛出异常
*/
func move(x: Int32, y: Int32, nWidth: Int32, nHeight: Int32):Bool
```

### 移动窗口
```cangjie
/**
*  @param rect 矩形
*  @return 是否成功
*  @exception 若请求时窗口未创建，则抛出异常
*/
func move(rect: RgfRect):Bool
```

### 移动窗口
```cangjie
/**
*  @param rect 浮点矩形
*  @return 是否成功
*  @exception 若请求时窗口未创建，则抛出异常
*/
func move(rect: RgfFRect):Bool
```

### 客户区转屏幕
```cangjie
/**
*  将当前客户区坐标转换为屏幕坐标
*  @param point 点
*  @return 转换后点
*  @exception 若请求时窗口未创建，则抛出异常；若底层Windows api出现预期外结果，则抛出异常
*/
func clientToScreen(point:RgfPoint):RgfPoint
```

### 屏幕转客户区
```cangjie
/**
*  将当前屏幕坐标转换为客户区坐标
*  @param point 点
*  @return 转换后点
*  @exception 若请求时窗口未创建，则抛出异常；若底层Windows api出现预期外结果，则抛出异常
*/
func screenToClient(point:RgfPoint):RgfPoint
```

### 获取窗口DC
```cangjie
/**
*  @return 窗口DC
*  @attention 凡是获取的窗口DC都一定要通过 hdcRelease 释放
*  @exception 若请求时窗口未创建，则抛出异常
*/
func hdcAcquire():RgfHdc
```

### 释放窗口DC
```cangjie
/**
*  @param 窗口DC
*  @exception 若请求时窗口未创建，则抛出异常
*/
func hdcRelease(hdc:RgfHdc):Int32
```