# IEventMethod

## 继承式回调

### 窗口创建
```cangjie
func onCreate(e:RgfEvCreate):Bool
```
### 窗口非客户区创建
```cangjie
func onNCCreate(e:RgfEvCreate):Bool```
### 窗口销毁
```cangjie
func onDestroy(e:RgfEvCore):Unit
```
### 窗口非客户区销毁
```cangjie
func onNCDestroy(e:RgfEvCore):Unit
```
### 窗口移动
```cangjie
func onMove(e:RgfEvMove):Unit
```
### 窗口尺寸调整
```cangjie
func onSize(e:RgfEvSize):Unit
```
### 窗口激活
```cangjie
func onActivate(e:RgfEvActivate):Unit
```
### 窗口获得焦点
```cangjie
func onSetFocus(e:RgfEvSetFocus):Unit
```
### 窗口失去焦点
```cangjie
func onKillFocus(e:RgfEvKillFocus):Unit
```
### 窗口启用或禁用
```cangjie
func onEnable(e:RgfEvEnable):Unit
```
### 窗口是否可重绘
```cangjie
func onSetRedraw(e:RgfEvSetRedraw):Unit
```
### 窗口设置文本
```cangjie
func onSetText(e:RgfEvSetText):Bool
```
### 获取窗口指定长度字符串
```cangjie
func onGetText(e:RgfEvGetText):Int64
```
### 获取字符串长度
```cangjie
func onGetTextlength(e:RgfEvCore):Int64
```
### 获取字符串长度
```cangjie
func onCloss(e:RgfEvCore):Unit
```
### 当用户选择结束会话或应用程序调用某个系统关闭函数时触发
```cangjie
func onQueryEndSession(e:RgfEvQueryEndSession):Bool
```
### 当用户请求将窗口还原到其以前的大小和位置时，发送到图标
```cangjie
func onQueryOpen(e:RgfEvCore):Bool
```
### 系统处理onQueryEndSession的结果后，会将onEndSession发送到应用程序。 onEndSession消息通知应用程序会话是否已结束
```cangjie
func onEndSession(e:RgfEvEndSession):Unit
```
### 系统配色更改
```cangjie
func onSysColorChange(e:RgfEvCore):Unit
```
### 窗口显示或隐藏
```cangjie
func onShowWindow(e:RgfEvShow):Unit
```
### 系统在参数设置更改时触发
```cangjie
func onSettingChange(e:RgfEvSettingChange):Unit
```
### 在用户更改设备模式设置时触发
```cangjie
func onDevModeChange(e:RgfEvDevModeChange):Unit
```
### 激活应用状态更改
```cangjie
func onActivateApp(e:RgfEvActivateApp):Unit
```
### 系统字体资源更改
```cangjie
func onFontChange(e:RgfEvCore):Unit
```
### 系统时间被更改
```cangjie
func onTimeChange(e:RgfEvCore):Unit
```
### 取消某些模式，例如鼠标捕获。或当显示对话框或消息框时触发
```cangjie
func onCancelMode(e:RgfEvCore):Unit
```
### 未捕获鼠标输入并且鼠标光标在窗口内移动
```cangjie
func onSetCursor(e:RgfEvSetCursor):Unit
```
### 鼠标激活窗口
```cangjie
func onMouseActivate(e:RgfEvMouseActivate):RgfEmMouseActivate
```
### 如果窗口是多文档接口 (MDI) 子窗口，则当用户单击窗口的标题栏或者当激活、移动窗口或调整其大小时触发
```cangjie
func onChildActivate(e:RgfEvCore):Unit
```
### 由基于计算机的训练 (CBT) 应用程序发送触发
```cangjie
func onQueueSync(e:RgfEvCore):Unit
```
### 当窗口的大小或位置即将更改时，发送到窗口。 应用程序可以使用此消息来替代窗口的默认最大大小和位置，或者其默认的最小或最大跟踪大小
```cangjie
func onGetMinMaxInfo(e:RgfEvGetMinMaxInfo):Unit
```
### 发送到对话框过程，以将键盘焦点设置为对话框中的其他控件
```cangjie
func onNextDlgCtl(e:RgfEvNextDlgCtl):Unit
```
### 当按钮、组合框、列表框或菜单的可视方面发生更改时，发送到所有者绘制的按钮、组合框、列表框或菜单的父窗口
```cangjie
func onDrawItem(e:RgfEvDrawItem):Bool
```
### 通知系统所有者绘制的控件或菜单项的尺寸。 这允许系统正确处理用户与控件的交互
```cangjie
func onMeasureItem(e:RgfEvMeasureItem):Bool
```
### 框架调用此成员函数来告知所有者绘制列表框或组合框的所有者该列表框或组合框已销毁，或者项已经被销毁
```cangjie
func onDeleteItem(e:RgfEvDeleteItem):Bool
```
### 列表框发送给其所有者，以响应 KeyDown 事件
```cangjie
/**
    * 列表框发送给其所有者，以响应 KeyDown 事件
    * 返回值 -2 表示应用程序处理了选择项的所有方面并且不需要列表框的进一步操作。
    * 返回值 -1 表示列表框应执行默认操作来响应击键。
    * 不小于 0 的返回值指定列表框中项目的索引，且表示列表框应当为针对指定项目的击键执行默认操作
*/
func onVKeyToItem(e:RgfEvVKeyToItem):Int64
```
### 设置控件在绘制文本时要使用的字体
```cangjie
func onSetFont(e:RgfEvSetFont):Unit
```
### 获取控件在绘制文本时要使用的字体
```cangjie
func onGetFont(e:RgfEvCore):CPointer<Unit>
```
### 用户按下热键
```cangjie
/**
    * 发送到窗口以将热键与窗口相关联。 当用户按下热键时，系统将激活窗口
    * 返回值 -1 函数不成功;热键无效。
    * 返回值 0 函数不成功;窗口无效。
    * 返回值 1 函数成功，并且没有其他窗口具有相同的热键。
    * 返回值 2 函数成功，但另一个窗口已具有相同的热键。
*/
func onSetHotKey(e:RgfEvSetHotKey):Int64
```
### 获取与窗口关联的热键
```cangjie
/**
    * 获取与窗口关联的热键
    * 返回值 -1 函数不成功;热键无效。
    * 返回值 0 函数不成功;窗口无效。
    * 返回值 1 函数成功，并且没有其他窗口具有相同的热键。
    * 返回值 2 函数成功，但另一个窗口已具有相同的热键。
*/
func onGetHotKey(e:RgfEvCore):Int64
```
### 获取拖动图标
```cangjie
/**
    * 发送到最小化 (图标) 窗口。 该窗口即将由用户拖动，但没有为其类定义图标。 
    * 应用程序可以将句柄返回到图标或光标。 当用户拖动图标时，系统将显示此光标或图标
    * 应用程序应返回光标或图标的句柄，系统将在用户拖动图标时显示该光标或图标。 
    * 该光标或图标必须与显示驱动程序的分辨率兼容。 
    * 如果应用程序返回 NULL，则系统将显示默认游标
*/ 
func onQueryDragIcon(e:RgfEvCore):CPointer<Unit>
```
### 新项在已排序的所有者绘制子组合框或列表框中的相对位置计算方法
```cangjie
func onCompareItem(e:RgfEvCompareItem):RgfEmCompare
```
### 请求获取对象
```cangjie
func onGetObject(e:RgfEvGetObject):Int64
```
### 系统内存不足，当系统在 30 到 60 秒间隔内检测到超过 12.5% 的系统时间用于压缩内存时触发
```cangjie
func onCompacting(e:RgfEvCompacting):Unit
```
### 即将更改大小、位置或 Z 顺序时触发
```cangjie
func onWindowPosChanging(e:RgfEvWindowPosChange):Unit
```
### 更改大小、位置或 Z 顺序时触发
```cangjie
func onWindowPosChanged(e:RgfEvWindowPosChange):Unit
```
### 将数据从一个应用程序复制到另一个应用程序
```cangjie
func onCopyData(e:RgfEvCopyData):Bool
```
### 消息通知，当事件发生或控件需要一些信息时，由公共控件发送到其父窗口
```cangjie
func onNotify(e:RgfEvCore):Int64
```
### 当用户选择新的输入语言时触发
```cangjie
func onInputLangChangeRequest(e:RgfEvInputLangChangeRequest):Unit
```
### 更改应用程序的输入语言后触发
```cangjie
func onInputLangChange(e:RgfEvInputLangChange):Unit
```
### 当用户单击可创作型按钮时触发
```cangjie
func onTCard(e:RgfEvTCard):Unit
```
### 用户按下 F1 打开帮助
```cangjie
func onHelp(e:RgfEvHelp):Unit
```
### 确定窗口是否接受 OnNotify 中的 ANSI 或 Unicode 结构
```cangjie
func onNotifyFormat(e:RgfEvNotifyFormat):RgfEmNotifyRtv
```
### 通知窗口用户希望显示上下文菜单。 用户可能单击了鼠标右键 (在窗口中右键单击) ，按下了 Shift+F10 或按下了应用程序键， (上下文菜单键) 某些键盘上可用
```cangjie
func onContextMenu(e:RgfEvContextMenu):Unit
```
### 将更改窗口的一个或多个样式时触发
```cangjie
func onStyleChanging(e:RgfEvStyleChange):Unit
```
### 窗口的一个或多个样式已更改时触发
```cangjie
func onStyleChanged(e:RgfEvStyleChange):Unit
```
### 系统显示分辨率更改
```cangjie
func onDisplayChange(e:RgfEvDisplayChange):Unit
```
### 获取图标
```cangjie
func onGetIcon(e:RgfEvGetIcon):CPointer<Unit>
```
### 设置新图标
```cangjie
func onSetIcon(e:RgfEvSetIcon):CPointer<Unit>
```
### 在必须计算窗口工作区的大小和位置时触发
```cangjie
/**
    当 e.calcValidRects 为 true 时，需要返回以下值之一或组合
    WVR_ALIGNTOP 0x0010 指定保留窗口的工作区，并与窗口新位置的顶部对齐。 例如，若要将工作区与左上角对齐，请返回WVR_ALIGNTOP和 WVR_ALIGNLEFT 值。
    WVR_ALIGNRIGHT 0x0080 指定保留窗口的工作区，并与窗口新位置的右侧对齐。 例如，若要将工作区与右下角对齐，请返回 WVR_ALIGNRIGHT 和WVR_ALIGNBOTTOM值。
    WVR_ALIGNLEFT 0x0020 指定保留窗口的工作区，并与窗口新位置的左侧对齐。 例如，若要将工作区与左下角对齐，请返回 WVR_ALIGNLEFT 和 WVR_ALIGNBOTTOM 值。
    WVR_ALIGNBOTTOM 0x0040 指定保留窗口的工作区，并与窗口新位置的底部对齐。 例如，若要将工作区与左上角对齐，请返回WVR_ALIGNTOP和 WVR_ALIGNLEFT 值。
    WVR_HREDRAW 0x0100 与除 WVR_VALIDRECTS以外的任何其他值结合使用，如果客户端矩形水平更改大小，则窗口将完全重新绘制。 此值类似于 CS_HREDRAW 类样式
    WVR_VREDRAW 0x0200 如果客户端矩形垂直更改大小，则与其他任何值（ WVR_VALIDRECTS除外）结合使用会导致完全重绘窗口。 此值类似于 CS_VREDRAW 类样式
    WVR_REDRAW 0x0300 此值会导致重新绘制整个窗口。 它是 WVR_HREDRAW 和 WVR_VREDRAW 值的组合。
    WVR_VALIDRECTS 0x0400 此值指示从WM_NCCALCSIZE返回时，由NCCALCSIZE_PARAMS结构的rgrc[1] 和 rgrc[2] 成员指定的矩形分别包含有效的目标矩形和源区域矩形。 系统将合并这些矩形，以计算要保留的窗口面积。 系统会复制源矩形内窗口图像的任何部分，并将图像剪辑到目标矩形。 这两个矩形都采用父相对坐标或屏幕相对坐标。 此标志不能与任何其他标志组合使用。
    此返回值允许应用程序实现更详细的工作区保留策略，例如居中或保留工作区的子集。
*/
func onCalcSize(e:RgfEvCalcSize):Int64 
```
### 非客户区命中测试
```cangjie
func onNcHitTest(e:RgfEvNcHitTest):RgfEmHitTest
```
### 绘制非客户区
```cangjie
func onNcPaint(e:RgfEvNcPaint):Unit
```
### 非客户区激活
```cangjie
func onNcActivate(e:RgfEvNcActivate):Bool
```
### 获取控件代码
```cangjie
/**
    针对控件进行调用，以便控件可以自己处理箭头键和 TAB 键输入
    DLGC_BUTTON 0x2000 按钮。
    DLGC_DEFPUSHBUTTON 0x0010 默认按下按钮。
    DLGC_HASSETSEL 0x0008 EM_SETSEL 消息。
    DLGC_RADIOBUTTON 0x0040 单选按钮。
    DLGC_STATIC 0x0100 静态控件。
    DLGC_UNDEFPUSHBUTTON 0x0020 非默认的按下按钮。
    DLGC_WANTALLKEYS 0x0004 所有键盘输入。
    DLGC_WANTARROWS 0x0001 方向键。
    DLGC_WANTCHARS 0x0080 WM_CHAR 消息。
    DLGC_WANTMESSAGE 0x0004 所有键盘输入 (应用程序将此消息在 MSG 结构中传递给控件) 。
    DLGC_WANTTAB 0x0002
*/
func onGetDlgCode(e:RgfEvGetDlgCode):Int64
```
### 用于同步绘制，同时避免链接独立的 GUI 线程
```cangjie
func onSyncPaint(e:RgfEvCore):Int64
```
### 非客户区光标移动
```cangjie
func onNcMouseMove(e:RgfEvNcMouse):Unit
```
### 非客户区按下鼠标左键
```cangjie
func onNcLButtonDown(e:RgfEvNcMouse):Unit
```
### 非客户区释放鼠标左键
```cangjie
func onNcLButtonUp(e:RgfEvNcMouse):Unit
```
### 非客户区双击鼠标左键
```cangjie
func onNcLButtonDblClk(e:RgfEvNcMouse):Unit
```
### 非客户区按下鼠标右键
```cangjie
func onNcRButtonDown(e:RgfEvNcMouse):Unit
```
### 非客户区释放鼠标右键
```cangjie
func onNcRButtonUp(e:RgfEvNcMouse):Unit
```
### 非客户区双击鼠标右键
```cangjie
func onNcRButtonDblClk(e:RgfEvNcMouse):Unit
```
### 非客户区按下鼠标中键
```cangjie
func onNcMButtonDown(e:RgfEvNcMouse):Unit
```
### 非客户区释放鼠标中键
```cangjie
func onNcMButtonUp(e:RgfEvNcMouse):Unit
```
### 非客户区双击鼠标中键
```cangjie
func onNcMButtonDblClk(e:RgfEvNcMouse):Unit
```
### 非客户区按下鼠标X键
```cangjie
func onNcXButtonDown(e:RgfEvNcXMouse):Unit
```
### 非客户区释放鼠标X键
```cangjie
func onNcXButtonUp(e:RgfEvNcXMouse):Unit
```
### 非客户区双击鼠标X键
```cangjie
func onNcXButtonDblClk(e:RgfEvNcXMouse):Unit
```
### 注册接收原始输入的窗口会触发此方法
```cangjie
func onInputDeviceChange(e:RgfEvInputDeviceChange):Unit
```
### 窗口正在获取原始输入
```cangjie
func onInput(e:RgfEvInput):Unit
```
### 键按下
```cangjie
func onKeyDown(e:RgfEvKey):Unit
```
### 键释放
```cangjie
func onKeyUp(e:RgfEvKey):Unit
```
### 字符输入
```cangjie
func onChar(e:RgfEvChar):Unit
```
### 死键字符输入（死键是与其他字符组合形成复合字符的键，例如变音符号（双点）字符。 例如：Ö 字符由死键、变音符号和 O 键组成）
```cangjie
func onDeadChar(e:RgfEvChar):Unit
```
### 系统键按下（当用户释放按住 Alt 键时按下的键）
```cangjie
func onSysKeyDown(e:RgfEvKey):Unit
```
### 系统键释放（当用户释放按住 Alt 键时按下的键）
```cangjie
func onSysKeyUp(e:RgfEvKey):Unit
```
### 系统字符输入
```cangjie
func onSysChar(e:RgfEvChar):Unit
```
### 系统死键字符输入（死键是与其他字符组合形成复合字符的键，例如变音符号（双点）字符。 例如：Ö 字符由死键、变音符号和 O 键组成）
```cangjie
func onSysDeadChar(e:RgfEvChar):Unit
```
### Unicode字符输入 UTF-32
```cangjie
func onUniChar(e:RgfEvChar):Unit
```
### 在输入法生成组合字符串之前立即触发
```cangjie
func onImeStartComposition(e:RgfEvCore):Unit
```
### 在输入法生成组合字符串之后触发
```cangjie
func onImeEndComposition(e:RgfEvCore):Unit
```
### 当输入法因击键而更改组合状态时触发
```cangjie
func onImeComposition(e:RgfEvImeComposition):Unit
```
### 初始化对话框触发
```cangjie
func onInitDialog(e:RgfEvInitDialog):Bool
```
### 命令触发
```cangjie
func onCommand(e:RgfEvCore):Int64
```
### 系统命令触发
```cangjie
func onSysCommand(e:RgfEvSysCommand):Int64
```
### 时钟事件
```cangjie
func onTimer(e:RgfEvTimer):Unit
```
### 水平滚动条事件
```cangjie
func onHScroll(e:RgfEvScroll):Unit
```
### 垂直滚动条事件
```cangjie
func onVScroll(e:RgfEvScroll):Unit
```
### 当菜单即将变为活动状态时发送
```cangjie
func onInitMenu(e:RgfEvInitMenu):Unit
```
### 弹出菜单即将变为获得状态
```cangjie
func onInitMenuPopup(e:RgfEvInitMenuPopup):Unit
```
### 选择菜单
```cangjie
func onMenuSelect(e:RgfEvMenuSelect):Unit
```
### 手势触发
```cangjie
func onGesture(e:RgfEvGesture):Unit
```
### 本消息可设置手势配置
```cangjie
func onGestureNotify(e:RgfEvGestureNotify):Unit
```
### 当用户按下不与当前菜单中任何预定义助记键匹配的菜单助记键字符时触发
```cangjie
func onMenuChar(e:RgfEvMenuChar):RgfEmMenuCharRet
```
### 对话框或消息框进入空闲状态
```cangjie
func onEnterIdle(e:RgfEvEnterIdle):Unit
```
### 菜单上释放鼠标右键
```cangjie
func onMenuRButtonUp(e:RgfEvMenuIdx):Unit
```
### 当用户拖动菜单项时触发
```cangjie
func onMenuDrag(e:RgfEvMenuIdx):RgfEmMenuDragRet
```
### 当鼠标光标进入菜单项或从项的中心移动到项的顶部或底部时触发
```cangjie
func onMenuGetObject(e:RgfEvMenuGetObject):RgfEmMenuGetObjectRet
```
### 在下拉菜单或子菜单已销毁时触发
```cangjie
func onUnInitMenuPopup(e:RgfEvUnInitMenuPopup):Unit
```
### 当用户从菜单进行选择时触发
```cangjie
func onMenuCommand(e:RgfEvMenuIdx):Unit
```
### 在应更改用户界面 (UI) 状态时触发
```cangjie
func onChangeUIState(e:RgfEvUIState):Unit
```
### 调用以更改指定窗口及其所有子窗口的用户界面 (UI) 状态
```cangjie
func onUpdateUIState(e:RgfEvUIState):Unit
```
### 获取UI状态
```cangjie
/**
    调用以检索窗口的用户界面 (UI) 状态
    如果焦点指示器和键盘快捷键可见，则返回值为 0
    UISF_ACTIVE 0x4 应以用于活动控件的样式绘制控件。
    UISF_HIDEACCEL 0x2 键盘加速键已隐藏。
    UISF_HIDEFOCUS 0x1 焦点指示器已隐藏。
*/
func onQueryUIState(e:RgfEvCore):Int64
```
### 在系统绘制消息框之前发送到父窗口，用于修改色彩，返回画刷句柄
```cangjie
func onCtlColorMsgBox(e:RgfEvCtlColor):CPointer<Unit>
```
### 在系统绘制编辑框之前发送到父窗口，用于修改色彩，返回画刷句柄
```cangjie
func onCtlColorEdit(e:RgfEvCtlColor):CPointer<Unit>
```
### 在系统绘制列表框之前发送到父窗口，用于修改色彩，返回画刷句柄
```cangjie
func onCtlColorListBox(e:RgfEvCtlColor):CPointer<Unit>
```
### 在系统绘制按钮之前发送到父窗口，用于修改色彩，返回画刷句柄
```cangjie
func onCtlColorBtn(e:RgfEvCtlColor):CPointer<Unit>
```
### 在系统绘制对话框之前发送到父窗口，用于修改色彩，返回画刷句柄
```cangjie
func onCtlColorDlg(e:RgfEvCtlColor):CPointer<Unit>
```
### 在系统绘制滚动条之前发送到父窗口，用于修改色彩，返回画刷句柄
```cangjie
func onCtlColorScrollBar(e:RgfEvCtlColor):CPointer<Unit>
```
### 在系统绘制静态控件之前发送到父窗口，用于修改色彩，返回画刷句柄
```cangjie
func onCtlColorStatic(e:RgfEvCtlColor):CPointer<Unit>
```
### 客户区光标移动
```cangjie
func onMouseMove(e:RgfEvMouse):Unit
```
### 客户区按下鼠标左键
```cangjie
func onLButtonDown(e:RgfEvMouse):Unit
```
### 客户区释放鼠标左键
```cangjie
func onLButtonUp(e:RgfEvMouse):Unit
```
### 客户区双击鼠标左键
```cangjie
func onLButtonDblClk(e:RgfEvMouse):Unit
```
### 客户区按下鼠标右键
```cangjie
func onRButtonDown(e:RgfEvMouse):Unit
```
### 客户区释放鼠标右键
```cangjie
func onRButtonUp(e:RgfEvMouse):Unit
```
### 客户区双击鼠标右键
```cangjie
func onRButtonDblClk(e:RgfEvMouse):Unit
```
### 客户区按下鼠标中键
```cangjie
func onMButtonDown(e:RgfEvMouse):Unit
```
### 客户区释放鼠标中键
```cangjie
func onMButtonUp(e:RgfEvMouse):Unit
```
### 客户区双击鼠标中键
```cangjie
func onMButtonDblClk(e:RgfEvMouse):Unit
```
### 当鼠标滚轮倾斜或旋转时触发
```cangjie
func onMouseWheel(e:RgfEvMouseWheel):Unit
```
### 客户区按下鼠标X键
```cangjie
func onXButtonDown(e:RgfEvXMouse):Unit
```
### 客户区释放鼠标X键
```cangjie
func onXButtonUp(e:RgfEvXMouse):Unit
```
### 客户区双击鼠标X键
```cangjie
func onXButtonDblClk(e:RgfEvXMouse):Unit
```
### 当鼠标水平滚轮倾斜或旋转时触发
```cangjie
func onMouseHWheel(e:RgfEvMouseWheel):Unit
```
### 当创建或销毁父级的子窗口时，或者在光标位于子窗口上的情况下当用户单击鼠标按钮时触发
```cangjie
func onParentNotify(e:RgfEvParentNotify):Unit
```
### 进入菜单模式循环后触发
```cangjie
func onEnterMenuLoop(e:RgfEvMenuLoop):Unit
```
### 退出菜单模式循环后触发
```cangjie
func onExitMenuLoop(e:RgfEvMenuLoop):Unit
```
### 使用向右键或向左键在菜单栏和系统菜单之间切换时触发
```cangjie
func onNextMenu(e:RgfEvNextMenu):Unit
```
### 窗口正在调整尺寸中
```cangjie
func onSizing(e:RgfEvSizing):Unit
```
### 丢失鼠标捕获时触发
```cangjie
func onCaptureChanged(e:RgfEvCaptureChanged):Unit
```
### 窗口正在移动中
```cangjie
func onMoving(e:RgfEvMoving):Unit
```
### 通知应用程序发生了某个电源管理事件
```cangjie
func onPowerBroadcast(e:RgfEvPowerBroadcast):Unit
```
### 通知应用程序设备或计算机的硬件配置发生更改
```cangjie
func onDeviceChange(e:RgfEvDeviceChange):Bool
```
### MDI窗口创建
```cangjie
func onMDICreate(e:RgfEvMDICreate):Bool
```
### MDI窗口销毁
```cangjie
func onMDIDestroy(e:RgfEvMDIHwnd):Unit
```
### MDI窗口激活
```cangjie
func onMDIActivate(e:RgfEvMDIHwnd):Unit
```
### MDI窗口恢复
```cangjie
func onMDIRestore(e:RgfEvMDIHwnd):Unit
```
### MDI激活下一个或上一个子窗口
```cangjie
func onMDINext(e:RgfEvMDINext):Unit
```
### MDI客户端窗口最大化 MDI 子窗口
```cangjie
func onMDIMaximize(e:RgfEvMDIHwnd):Unit
```
### MDI 子窗口平铺
```cangjie
func onMDITile(e:RgfEvMDIDistributed):Bool
```
### MDI 子窗口层叠
```cangjie
func onMDICascade(e:RgfEvMDIDistributed):Bool
```
### MDI 客户端窗口排列所有最小化的 MDI 子窗口
```cangjie
func onMDIIconArrange(e:RgfEvCore):Unit
```
### 检索活动 MDI 子窗口的句柄
```cangjie
func onMDIGetActive(e:RgfEvCore):RgfHwnd
```
### MDI 客户端窗口来替换 MDI 框架窗口的整个菜单或框架窗口的窗口菜单
```cangjie
func onMDISetMenu(e:RgfEvMDISetMenu):CPointer<Unit>
```
### 进入窗口移动或缩放周期
```cangjie
func onEnterSizeMove(e:RgfEvCore):Unit
```
### 退出窗口移动或缩放周期
```cangjie
func onExitSizeMove(e:RgfEvCore):Unit
```
### 当用户在一个已将自身注册为所放置文件的接收方的窗口上释放鼠标左键时触发
```cangjie
func onDropFiles(e:RgfEvDropFiles):Unit
```
### 刷新MDI框架窗口菜单
```cangjie
func onMDIRefreshMenu(e:RgfEvCore):CPointer<Unit>
```
### 当附加了数字化器的监视器的设置发生更改时触发
```cangjie
func onPointerDeviceChange(e:RgfEvCore):Unit
```
### 在输入数字化器的监视器的范围内检测到指针设备时触发
```cangjie
func onPointerDeviceInRange(e:RgfEvCore):Unit
```
### 当设备离开输入数字化器的监视器的范围时时触发
```cangjie
func onPointerDeviceOutOfRange(e:RgfEvCore):Unit
```
### 触屏事件
```cangjie
func onTouch(e:RgfEvTouch):Unit
```
### 当指针在窗口的非客户端区域上产生接触，或悬停的未捕获到的触点移动到窗口的非客户端区域上方时触发
```cangjie
func onNcPointerUpdate(e:RgfEvNcPointer):Unit
```
### 当指针在窗口的非工作区上触摸时触发
```cangjie
func onNcPointerDown(e:RgfEvNcPointer):Unit
```
### 在窗口的非客户端区域上进行触摸的指针断开触摸时触发
```cangjie
func onNcPointerUp(e:RgfEvNcPointer):Unit
```
### 当指针在窗口的客户端区域上产生接触，或悬停的未捕获到的触点移动到窗口的客户端区域上方时触发
```cangjie
func onPointerUpdate(e:RgfEvPointer):Unit
```
### 当指针在窗口的工作区上触摸时触发
```cangjie
func onPointerDown(e:RgfEvPointer):Unit
```
### 在窗口的客户端区域上进行触摸的指针断开触摸时触发
```cangjie
func onPointerUp(e:RgfEvPointer):Unit
```
### 当触摸点指针进入时触发
```cangjie
func onPointerEnter(e:RgfEvPointer):Unit
```
### 当触摸点指针离开时触发
```cangjie
func onPointerLeave(e:RgfEvPointer):Unit
```
### 当触摸点指针激活窗口时触发
```cangjie
func onPointerActivate(e:RgfEvPointerActivate):RgfEmPointerActivate
```
### 当触摸点指针被窗口捕获时触发
```cangjie
func onPointerCaptureChanged(e:RgfEvPointerCaptureChanged):Unit
```
### 向下发送到触摸的窗口，以确定最有可能的触摸目标
```cangjie
func onTouchHitTesting(e:RgfEvTouchHitTesting):Int64
```
### 旋转滚轮时，使用前台键盘焦点发布到窗口
```cangjie
func onPointerWheel(e:RgfEvPointerWheel):Unit
```
### 在水平滚轮旋转时，使用前台键盘焦点发布到窗口
```cangjie
func onPointerHWheel(e:RgfEvPointerWheel):Unit
```
### 首次检测到指针输入时发送到窗口
```cangjie
func onPointerHitTest(e:RgfEvCore):Unit
```
### 在正在进行的指针输入时发送，对于现有指针 ID，跨为跨进程链接配置的内容从一个进程转换到另一个进程。此消息将发送到当前未接收指针输入的进程
```cangjie
func onPointerRoutedTo(e:RgfEvCore):Unit
```
### 当指针输入从一个进程转换到另一个进程之间跨为跨进程链接配置的内容时触发
```cangjie
func onPointerRoutedAway(e:RgfEvCore):Unit
```
### 在当前进程上收到 PointerUp 消息时，发送到所有进程
```cangjie
func onPointerRoutedReleased(e:RgfEvCore):Unit
```
### 激活窗口时发送到输入法应用程序
```cangjie
func onImeSetContext(e:RgfEvImeSetContext):Int64
```
### 通知其输入法窗口变动的消息
```cangjie
func onImeNotify(e:RgfEvCore):Int64
```
### 指示 IME 窗口执行请求的命令
```cangjie
func onImeControl(e:RgfEvCore):Int64
```
### 当 IME 窗口找不到用于扩展合成窗口区域的空间时触发
```cangjie
func onImeCompositionFull(e:RgfEvCore):Unit
```
### 当操作系统即将更改当前 IME 时触发
```cangjie
func onImeSelect(e:RgfEvImeSelect):Unit
```
### IME 字符输入
```cangjie
func onImeChar(e:RgfEvChar):Unit
```
### 提供 IME 命令和请求信息
```cangjie
func onImeRequest(e:RgfEvCore):Int64
```
### IME 键按下
```cangjie
func onImeKeyDown(e:RgfEvKey):Unit
```
### IME 键释放
```cangjie
func onImeKeyUp(e:RgfEvKey):Unit
```
### 当光标将鼠标悬停在窗口的工作区上时触发
```cangjie
func onMouseHover(e:RgfEvMouse):Unit
```
### 当光标离开指定注册的窗口工作区上时触发
```cangjie
func onMouseLeave(e:RgfEvCore):Unit
```
### 当光标将鼠标悬停在窗口的非工作区上时触发
```cangjie
func onNcMouseHover(e:RgfEvNcMouseHit):Unit
```
### 当光标离开指定注册的窗口非工作区上时触发
```cangjie
func onNcMouseLeave(e:RgfEvCore):Unit
```
### 通知应用程序会话状态的更改
```cangjie
func onWtsSessionChange(e:RgfEvWtsSessionChange):Unit
```
### DPI 已更改
```cangjie
func onDpiChanged(e:RgfEvDpiChanged):Unit
```
### DPI 已更改前所有子组件触发
```cangjie
func onDpiChangedBeforeParent(e:RgfEvCore):Unit
```
### DPI 已更改后所有子组件触发
```cangjie
func onDpiChangedAfterParent(e:RgfEvCore):Unit
```
### 操作系统告知窗口的大小将调整为默认值以外的其他尺寸
```cangjie
func onGetDpiScaledSize(e:RgfEvGetDpiScaledSize):Bool
```
### 剪切到剪贴板
```cangjie
func onCut(e:RgfEvCore):Unit
```
### 复制到剪贴板
```cangjie
func onCopy(e:RgfEvCore):Unit
```
### 从剪贴板粘贴
```cangjie
func onPaste(e:RgfEvCore):Unit
```
### 清除当前选择
```cangjie
func onClear(e:RgfEvCore):Unit
```
### 撤销编辑控件的上一次操作
```cangjie
func onUnDo(e:RgfEvCore):Bool
```
### 呈现剪贴板触发
```cangjie
func onRenderFormat(e:RgfEvRenderFormat):Unit
```
### 如果剪贴板所有者延迟了呈现一种或多种剪贴板格式，在销毁剪贴板之前将其发送给剪贴板所有者
```cangjie
func onRenderAllFormats(e:RgfEvCore):Unit
```
### 当对 EmptyClipboard 函数的调用清空剪贴板时，发送给剪贴板所有者
```cangjie
func onDestroyClipboard(e:RgfEvCore):Unit
```
### 当剪贴板的内容发生更改时，发送到剪贴板查看器链中的第一个窗口
```cangjie
func onDrawClipboard(e:RgfEvCore):Unit
```
### 当剪贴板包含 OwnerDisplay 格式的数据并且剪贴板查看器的工作区需要重新绘制时触发
```cangjie
func onPaintClipboard(e:RgfEvPaintClipboard):Unit
```
### 剪贴板查看器垂直滚动条事件
```cangjie
func onVScrollClipboard(e:RgfEvScrollClipboard):Unit
```
### 剪贴板查看器尺寸更改
```cangjie
func onSizeClipboard(e:RgfEvSizeClipboard):Unit
```
### 剪贴板查看器窗口发送给剪贴板所有者
```cangjie
func onAskCbFormatName(e:RgfEvAskCbFormatName):Unit
```
### 从链中删除窗口时，发送到剪贴板查看器链中的第一个窗口
```cangjie
func onChangeCbChain(e:RgfEvChangeCbChain):Unit
```
### 剪贴板查看器水平滚动条事件
```cangjie
func onHScrollClipboard(e:RgfEvScrollClipboard):Unit
```
### 窗口即将接收键盘焦点，使窗口有机会在接收焦点时实现其逻辑调色板
```cangjie
func onQueryNewPalette(e:RgfEvCore):Bool
```
### 通知应用程序应用程序将实现其逻辑调色板
```cangjie
func onPalletteIsChanging(e:RgfEvPallette):Unit
```
### 在具有键盘焦点的窗口实现其逻辑调色板后，触发到所有顶级窗口和重叠窗口
```cangjie
func onPalletteChanged(e:RgfEvPallette):Unit
```
### 当用户按下注册的热键时触发
```cangjie
func onHotKey(e:RgfEvHotKey):Unit
```
### 请求它在指定的设备上下文（最常见的是打印机设备上下文）中绘制自身
```cangjie
func onPrint(e:RgfEvPrint):Unit
```
### 请求它在指定的设备上下文中绘制其工作区，最常见的是在打印机设备上下文中
```cangjie
func onPrintClient(e:RgfEvPrint):Unit
```
### 通知窗口用户生成应用程序命令事件
```cangjie
func onAppCommand(e:RgfEvAppCommand):Unit
```
### 在系统主题更改事件后触发
```cangjie
func onThemeChanged(e:RgfEvCore):Unit
```
### 剪贴板的内容发生更改时触发
```cangjie
func onClipboardUpdate(e:RgfEvCore):Unit
```
### 桌面窗口管理器 (DWM) 组合已启用或禁用时触发
```cangjie
func onDwmCompositionChanged(e:RgfEvCore):Unit
```
### 桌面窗口管理器 (DWM) 组合已启用或禁用时触发
```cangjie
func onDwmNcRenderingChanged(e:RgfEvDwmNcRenderingChanged):Unit
```
### 通知所有顶级窗口着色颜色已更改
```cangjie
func onDwmColorizationColorChanged(e:RgfEvDwmColorizationColorChanged):Unit
```
### 当桌面窗口管理器 (DWM) 组合窗口最大化时发送
```cangjie
func onDwmWindowMaximizedChange(e:RgfEvDwmWindowMaximizedChange):Unit
```
### 指示窗口提供一个静态位图，用作该窗口的缩略图表示形式
```cangjie
func onDwmSendIconicThumbnail(e:RgfEvDwmSendIconicThumbnail):Unit
```
### 指示窗口提供静态位图，用作 实时预览 (也称为该窗口的 速览预览)
```cangjie
func onDwmSendIconicLivePreviewBitmap(e:RgfEvCore):Unit
```
### 发送请求扩展标题栏信息
```cangjie
func onGetTitleBarInfoEx(e:RgfEvGetTitleBarInfoEx):Unit
```