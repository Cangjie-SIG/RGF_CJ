# IDrawContent

## 成员方法

### 开始绘制
```cangjie
/**
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 所有绘制行为都应该在 beginDraw() 和 endDraw() 中进行
*/
func beginDraw():Bool
```

### 结束绘制
```cangjie
/**
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 所有绘制行为都应该在 beginDraw() 和 endDraw() 中进行
*/
func endDraw():Bool
// 清除画布
```

### 清除画布
```cangjie
/**
*  @param r         0.0~1.0 红
*  @param g         0.0~1.0 绿
*  @param b         0.0~1.0 蓝
*  @param a         0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func clear(r:Float32,g:Float32,b:Float32,a:Float32):Bool
```

### 清除画布
```cangjie
/**
*  @param rgba      色彩值
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func clear(rgba:RgfFColor):Bool
```

### 绘制直线
```cangjie
/**
*  @param x1            坐标x
*  @param y1            坐标y
*  @param x2            坐标x
*  @param y2            坐标y
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawLine (x1:Float32, y1:Float32, x2:Float32, y2:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制直线
```cangjie
/**
*  @param pos1          浮点坐标1
*  @param pos2          浮点坐标2
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawLine (pos1:RgfFPoint, pos2:RgfFPoint, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制直线
```cangjie
/**
*  @param pos1          坐标1
*  @param pos2          坐标2
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawLine (pos1:RgfPoint, pos2:RgfPoint, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制矩形
```cangjie
/**
*  @param rect          浮点矩形
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawRectangle (rect:RgfFRect, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制矩形
```cangjie
/**
*  @param rect          矩形
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawRectangle (rect:RgfRect, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制矩形
```cangjie
/**
*  @param x             左边
*  @param y             顶边
*  @param width         宽度
*  @param height        高度
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawRectangle (x:Float32, y:Float32, width:Float32, height:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制椭圆
```cangjie
/**
*  @param rect          浮点矩形
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawEllipse (rect:RgfFRect, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制椭圆
```cangjie
/**
*  @param rect          矩形
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawEllipse (rect:RgfRect, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制椭圆
```cangjie
/**
*  @param x             左边
*  @param y             顶边
*  @param width         宽度
*  @param height        高度
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawEllipse (x:Float32, y:Float32, width:Float32, height:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制椭圆
```cangjie
/**
*  @param x             左边
*  @param y             顶边
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawEllipseRadius (x:Float32, y:Float32, xRadius:Float32, yRadius:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制椭圆
```cangjie
/**
*  @param point         浮点坐标点
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawEllipseRadius (point:RgfFPoint, xRadius:Float32, yRadius:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制椭圆
```cangjie
/**
*  @param point         坐标点
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawEllipseRadius (point:RgfPoint, xRadius:Float32, yRadius:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```


### 绘制圆角矩形
```cangjie
/**
*  @param rect          浮点矩形
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawRoundedRectangle (rect:RgfFRect, xRadius:Float32, yRadius:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制圆角矩形
```cangjie
/**
*  @param rect          矩形
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawRoundedRectangle (rect:RgfRect, xRadius:Float32, yRadius:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制圆角矩形
```cangjie
/**
*  @param x             左边
*  @param y             顶边
*  @param width         宽度
*  @param height        高度
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawRoundedRectangle (x:Float32, y:Float32, width:Float32, height:Float32, xRadius:Float32, yRadius:Float32, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 绘制路径
```cangjie
/**
*  @param path          路径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawPath (path:Path, pBrush:Brush, pStrokeStyle!:Option<StrokeStyle>):Bool;
```

### 填充矩形
```cangjie
/**
*  @param rect          浮点矩形
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillRectangle (rect:RgfFRect, pBrush:Brush):Bool;
```

### 填充矩形
```cangjie
/**
*  @param rect          矩形
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillRectangle (rect:RgfRect, pBrush:Brush):Bool;
```

### 填充矩形
```cangjie
/**
*  @param x             左边
*  @param y             顶边
*  @param width         宽度
*  @param height        高度
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillRectangle (x:Float32, y:Float32, width:Float32, height:Float32, pBrush:Brush):Bool;
```

### 填充椭圆
```cangjie
/**
*  @param rect          浮点矩形
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillEllipse (rect:RgfFRect, pBrush:Brush):Bool;
```

### 填充椭圆
```cangjie
/**
*  @param rect          矩形
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillEllipse (rect:RgfRect, pBrush:Brush):Bool;
```

### 填充椭圆
```cangjie
/**
*  @param x             左边
*  @param y             顶边
*  @param width         宽度
*  @param height        高度
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillEllipse (x:Float32, y:Float32, width:Float32, height:Float32, pBrush:Brush):Bool;
```

### 填充椭圆
```cangjie
/**
*  @param x             左边
*  @param y             顶边
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillEllipseRadius (x:Float32, y:Float32, xRadius:Float32, yRadius:Float32, pBrush:Brush):Bool;
```

### 填充椭圆
```cangjie
/**
*  @param point         浮点坐标点
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillEllipseRadius (point:RgfFPoint, xRadius:Float32, yRadius:Float32, pBrush:Brush):Bool;
```

### 填充椭圆
```cangjie
/**
*  @param point         坐标点
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillEllipseRadius (point:RgfPoint, xRadius:Float32, yRadius:Float32, pBrush:Brush):Bool;
```

### 填充圆角矩形
```cangjie
/**
*  @param rect          浮点矩形
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillRoundedRectangle (rect:RgfFRect, xRadius:Float32, yRadius:Float32, pBrush:Brush):Bool;
```

### 填充圆角矩形
```cangjie
/**
*  @param rect          矩形
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillRoundedRectangle (rect:RgfRect, xRadius:Float32, yRadius:Float32, pBrush:Brush):Bool;
```

### 填充圆角矩形
```cangjie
/**
*  @param x             左边
*  @param y             顶边
*  @param width         宽度
*  @param height        高度
*  @param xRadius       横向半径
*  @param yRadius       纵向半径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillRoundedRectangle (x:Float32, y:Float32, width:Float32, height:Float32, xRadius:Float32, yRadius:Float32, pBrush:Brush):Bool;
```

### 填充路径
```cangjie
/**
*  @param path          路径
*  @param pBrush        画刷
*  @param pStrokeStyle  样式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillPath (path:Path, pBrush:Brush):Bool;
```

### 绘制图层
```cangjie
/**
*  @param destRect  目标浮点矩形
*  @param pLayer    图层
*  @param srcRect   源浮点矩形
*  @param alpha     0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func pushLayer (destRect:RgfFRect, pLayer:Layer, srcRect:RgfFRect, alpha!:Float32):Bool;
```

### 绘制图层
```cangjie
/**
*  @param destRect  目标矩形
*  @param pLayer    图层
*  @param srcRect   源矩形
*  @param alpha     0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func pushLayer (destRect:RgfRect, pLayer:Layer, srcRect:RgfRect, alpha!:Float32):Bool;
```

### 绘制图层
```cangjie
/**
*  @param destX         目标左边
*  @param destY         目标顶边
*  @param destWidth     目标宽度
*  @param destHeight    目标高度
*  @param pLayer        图层
*  @param srcX          源左边
*  @param srcY          源顶边
*  @param srcWidth      源宽度
*  @param srcHeight     源高度
*  @param alpha         0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func pushLayer (destX:Float32, destY:Float32, destWidth:Float32, destHeight:Float32, pLayer:Layer, srcX:Float32, srcY:Float32, srcWidth:Float32, srcHeight:Float32, alpha!:Float32):Bool;
```

### 绘制图层
```cangjie
/**
*  @param rectDestSrc   目标源浮点矩形
*  @param pLayer        图层
*  @param alpha         0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func pushLayer (rectDestSrc:RgfFRect, pLayer:Layer, alpha!:Float32):Bool;
```

### 绘制图层
```cangjie
/**
*  @param rectDestSrc   目标源矩形
*  @param pLayer        图层
*  @param alpha         0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func pushLayer (rectDestSrc:RgfRect, pLayer:Layer, alpha!:Float32):Bool;
```

### 绘制图层
```cangjie
/**
*  @param xDestSrc          目标源左边
*  @param yDestSrc          目标源顶边
*  @param widthDestSrc      目标源宽度
*  @param HeightDestSrc     目标源高度
*  @param pLayer            图层
*  @param alpha             0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func pushLayer (xDestSrc:Float32, yDestSrc:Float32, widthDestSrc:Float32, HeightDestSrc:Float32, pLayer:Layer, alpha!:Float32):Bool;
```

### 蒙版绘图
```cangjie
/**
*  @param destRect  目标浮点矩形
*  @param mask      蒙版
*  @param brush     画刷
*  @param destRect  源浮点矩形
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillOpacityMask (destRect:RgfFRect, mask:Image, brush:Brush, srcRect:RgfFRect):Bool;
```

### 蒙版绘图
```cangjie
/**
*  @param destRect  目标矩形
*  @param mask      蒙版
*  @param brush     画刷
*  @param destRect  源矩形
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillOpacityMask (destRect:RgfRect, mask:Image, brush:Brush, srcRect:RgfRect):Bool;
```

### 蒙版绘图
```cangjie
/**
*  @param destX         目标左边
*  @param destY         目标顶边
*  @param destWidth     目标宽度
*  @param destHeight    目标高度
*  @param mask          蒙版
*  @param brush         画刷
*  @param srcX          源左边
*  @param srcY          源顶边
*  @param srcWidth      源宽度
*  @param srcHeight     源高度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillOpacityMask (destX:Float32, destY:Float32, destWidth:Float32, destHeight:Float32, mask:Image, brush:Brush, srcX:Float32, srcY:Float32, srcWidth:Float32, srcHeight:Float32):Bool;
```

### 蒙版绘图
```cangjie
/**
*  @param rectDestSrc   目标源浮点矩形
*  @param mask          蒙版
*  @param brush         画刷
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillOpacityMask (rectDestSrc:RgfFRect, mask:Image, brush:Brush):Bool;
```

### 蒙版绘图
```cangjie
/**
*  @param rectDestSrc   目标源矩形
*  @param mask          蒙版
*  @param brush         画刷
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillOpacityMask (rectDestSrc:RgfRect, mask:Image, brush:Brush):Bool;
```

### 蒙版绘图
```cangjie
/**
*  @param destX         目标源左边
*  @param destY         目标源顶边
*  @param destWidth     目标源宽度
*  @param destHeight    目标源高度
*  @param mask          蒙版
*  @param brush         画刷
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func fillOpacityMask (xDestSrc:Float32, yDestSrc:Float32, widthDestSrc:Float32, HeightDestSrc:Float32, mask:Image, brush:Brush):Bool;
```

### 绘制位图
```cangjie
/**
*  @param destRect  目标浮点矩形
*  @param bitmap    位图
*  @param srcRect   源浮点矩形
*  @param alpha     0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawBitmap (destRect:RgfFRect, bitmap:Bitmap, srcRect:RgfFRect, alpha!:Float32):Bool;
```

### 绘制位图
```cangjie
/**
*  @param destRect  目标矩形
*  @param bitmap    位图
*  @param srcRect   源矩形
*  @param alpha     0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawBitmap (destRect:RgfRect, bitmap:Bitmap, srcRect:RgfRect, alpha!:Float32):Bool;
```

### 绘制位图
```cangjie
/**
*  @param destX         目标左边
*  @param destY         目标顶边
*  @param destWidth     目标宽度
*  @param destHeight    目标高度
*  @param bitmap        位图
*  @param srcX          源左边
*  @param srcY          源顶边
*  @param srcWidth      源宽度
*  @param srcHeight     源高度
*  @param alpha         0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawBitmap (destX:Float32, destY:Float32, destWidth:Float32, destHeight:Float32, bitmap:Bitmap, srcX:Float32, srcY:Float32, srcWidth:Float32, srcHeight:Float32, alpha!:Float32):Bool;
```

### 绘制位图
```cangjie
/**
*  @param rectDestSrc   目标源浮点矩形
*  @param bitmap        位图
*  @param alpha         0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawBitmap (rectDestSrc:RgfFRect, bitmap:Bitmap, alpha!:Float32):Bool;
```

### 绘制位图
```cangjie
/**
*  @param rectDestSrc   目标源矩形
*  @param bitmap        位图
*  @param alpha         0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawBitmap (rectDestSrc:RgfRect, bitmap:Bitmap, alpha!:Float32):Bool;
```

### 绘制位图
```cangjie
/**
*  @param xDestSrc          目标源左边
*  @param yDestSrc          目标源顶边
*  @param widthDestSrc      目标源宽度
*  @param HeightDestSrc     目标源高度
*  @param bitmap            位图
*  @param alpha             0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawBitmap (xDestSrc:Float32, yDestSrc:Float32, widthDestSrc:Float32, HeightDestSrc:Float32, bitmap:Bitmap, alpha!:Float32):Bool;
```

### 绘制文本
```cangjie
/**
*  @param pTcore    文本核心
*  @param pBrush    画刷
*  @param pDwTxtLy  文本布局
*  @param originX   横向偏移
*  @param originY   纵向偏移
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func drawTexts(pTcore:TextCore,pBrush:Brush,pDwTxtLy:TextLayout,originX!:Float32,originY!:Float32):Bool;
```

### 设置矩阵
```cangjie
/**
*  @param transform 矩阵
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func setTransform(transform:Matrix):Unit;
```

### 获取矩阵
```cangjie
/**
*  @param transform 矩阵引用
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func getTransform(transform:Matrix):Unit;
```

### 设置抗锯齿
```cangjie
/**
*  @param antialias 抗锯齿模式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func setAntialiasMode(antialias:RgfAntialiasMode):Unit;
```

### 获取抗锯齿
```cangjie
/**
*  @return 抗锯齿模式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
func getAntialiasMode():RgfAntialiasMode;