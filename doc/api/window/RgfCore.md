# RgfCore

## 成员属性

### 窗口句柄
```cangjie
public prop hWnd:RgfHwnd
```

## 成员方法

### 创建窗口
```cangjie
/**
*  @param winCtxt       窗口类上下文
*  @param lpWindowName  窗口名
*  @param x             左边
*  @param y             顶边
*  @param nWidth        宽度
*  @param nHeight       高度
*  @param hWndParent    父窗口句柄 (0 则为桌面)
*  @param dwExStyle     窗口扩展风格
*  @param dwStyle       窗口风格
*  @param token         Rgf全局上下文(令牌)
*  @param substrate     底层技术 (推荐:Direct2D)
*  @param layered       窗口是否半透明
*  @param bgMod         背景模式 (是否在擦除事件中也进行渲染，建议否)
*  @param drawInterval  绘制间隔 (控件填0, 游戏填写需要的间隔，单位毫秒)
*  @return 窗口句柄
*/

public open func createWin(
    winCtxt:RgfWinContext,lpWindowName:String,
    x:Int32, y:Int32, nWidth:Int32, nHeight:Int32,
    hWndParent:RgfHwnd,
    dwExStyle:DWORD,
    dwStyle:DWORD,
    token:RgfContext, 
    substrate!:RgfSubstrate,
    layered!:Bool,
    bgMod!:RgfBackGroundMod, 
    drawInterval!:UInt32
):RgfHwnd
```

### 销毁窗口
```cangjie
/**
*  @note 本方法用于程序主动触发销毁，当用户关闭窗口或是父窗口被销毁时，当前窗口都会自动销毁并且释放相关资源
*/
public open func destroyWin()
```

### 调用渲染
```cangjie
/**
*  @param wRect     需要重绘的矩形 (留空表示全量重绘)
*  @return 是否成功调用
*  @note 调用成功并不代表渲染次数加一，为了确保渲染线程安全，当并发渲染请求过多时，会抛弃跳过冲突的部分渲染，仅保留最近一次渲染请求等待渲染
*/
public func draw(wRect!:Option<RgfRect> = Option.None):Bool
```

### 窗口是否已经创建
```cangjie
/**
*  @return 是否创建
*/
public func isCreated():Bool
```

### 当前窗口是否允许渲染
```cangjie
/**
*  @return 是否允许渲染
*/
public func isRenderingAllowed():Bool
```

### 转文本
```cangjie
/**
*  @return 格式化文本对象
*  本方法将内部内容转换为格式化字符串
*/
public override open func toString():String
```

## 类继承式回调

### 窗口已创建
```cangjie
/**
*  @param hwnd      窗口句柄
*  @param x         左边
*  @param y         顶边
*  @param nWidth    宽度
*  @param nHeight   高度
*  @note 当窗口创建完成后，会触发本事件
*/
public open func created(hwnd:RgfHwnd,x:Int32, y:Int32, nWidth:Int32, nHeight:Int32):Unit
```

### 窗口主过程函数
```cangjie
/**
*  @param hwnd      窗口句柄
*  @param uMsg      窗口消息
*  @param wParam    参数
*  @param lParam    参数
*  @note 当窗口开始创建的那一刻，所有窗口相关消息都会触发本事件
*/
public open func wndProc(hwnd:RgfHwnd, uMsg:UInt32, wParam:UInt64, lParam:Int64):Int64
```

### 创建之前
```cangjie
/**
*  @note 当调用 createWin 后，本事件最先触发，但此时渲染表面尚未创建
*/
public open func beforeCreate():Unit
```

### 渲染表面已创建
```cangjie
/**
*  @param cSfc      渲染表面对象 (在后续事件中通过 rgfRenderer 成员变量访问)
*  @note 本事件触发在渲染表面创建后，但此时设备相关资源尚未创建
*/
public open func rendererCreated(cSfc:RgfRenderer):Unit
```

### 创建设备相关/渲染相关资源
```cangjie
/**
*  @note 本事件为核心生命周期，此事件有可能会在一个窗口周期中被多次调用，开发者应确保此处创建的资源被正确释放
*/
public open func createDeviceResources():Bool
```

### 绘制
```cangjie
/**
*  @param wRect     需要重绘的矩形
*  @note 当窗口需要重新绘制时，会通过本事件传递需要重绘的部分
*/
public open func onPaint(wRect:RgfRect):Unit
```

### 销毁设备相关/渲染相关资源
```cangjie
/**
*  @note 本事件为核心生命周期，此事件有可能会在一个窗口周期中被多次调用，开发者应在此处销毁核心生命周期中的设备资源
*/
public open func destroyDeviceResources():Bool
```

### 窗口渲染表面将释放
```cangjie
/**
*  @note 本事件触发在渲染表面即将创建前，此时设备相关资源已经释放
*/
public open func rendererDestroyed():Unit
```

### 窗口销毁前
```cangjie
/**
*  @note 本事件触发在窗口即将销毁之前，此时渲染表面已经释放
*/
public open func beforeDestroy():Unit
```

### 窗口已销毁
```cangjie
/**
*  @note 本事件为窗口生命周期的终结，此时窗口已被释放
*/
public open func destroyed():Unit
```

## 监听式回调

### 创建之前
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenBeforeCreate(callback:()->Bool)
```

### 渲染表面已创建
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenRendererCreated(callback:(cSfc:RgfRenderer)->Bool)
```

### 创建设备相关/渲染相关资源
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenCreateDeviceResources(callback:()->(Bool,Bool))
```

### 绘制
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenOnPaint(callback:(wRect:RgfRect)->Bool)
```

### 销毁设备相关/渲染相关资源
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenDestroyDeviceResources(callback:()->(Bool,Bool))
```

### 窗口渲染表面将释放
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenRendererDestroyed(callback:()->Bool)
```

### 窗口销毁前
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenBeforeDestroy(callback:()->Bool)
```

### 窗口已销毁
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenDestroyed(callback:()->Bool)
```

### 窗口已创建
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenCreated(callback:(hwnd:RgfHwnd,x:Int32, y:Int32, nWidth:Int32, nHeight:Int32)->Bool)
```

### 窗口主过程函数
```cangjie
/** 末位返回 Bool 表示是否继续执行类自身回调函数 */
public func listenWndProc(callback:(hwnd:RgfHwnd, uMsg:UInt32, wParam:UInt64, lParam:Int64)->(Int64,Bool))
```