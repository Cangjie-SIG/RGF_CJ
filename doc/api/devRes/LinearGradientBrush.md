# LinearGradientBrush <: [Brush](./Brush.md)

## 成员方法

### 是否启用伽马校正
```cangjie
/**
*  @return 是否启用
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getGammaCorrection ():Bool
```

### 获取矩形
```cangjie
/**
*  @return 矩形
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getRectangle ():RgfFRect
```

### 转文本
```cangjie
/**
*  @return 格式化文本对象
*  本方法将内部内容转换为格式化字符串
*/
public override open func toString():String
```