# SolidColorBrush <: [Brush](./Brush.md)

## 成员方法

### 获取画刷颜色
```cangjie
/**
*  @return 颜色
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getColor ():RgfFColor
```

### 设置画刷颜色
```cangjie
/**
*  @param r 0.0~1.0 红色
*  @param g 0.0~1.0 绿色
*  @param b 0.0~1.0 蓝色
*  @param a 0.0~1.0 透明度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setColor (r:Float32 , g:Float32, b:Float32, a:Float32):Bool
```

### 设置画刷颜色
```cangjie
/**
*  @param color 颜色值
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setColor (color:RgfFColor):Bool
```

### 转文本
```cangjie
/**
*  @return 格式化文本对象
*  本方法将内部内容转换为格式化字符串
*/
public override open func toString():String
```