# StrokeStyle

## 成员方法

### 获取开始线条帽样式
```cangjie
*  @return 线条帽样式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getStartCap ():RgfCapStyle
```

### 获取结束线条帽样式
```cangjie
*  @return 线条帽样式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getEndCap ():RgfCapStyle
```

### 获取虚线条帽样式
```cangjie
*  @return 线条帽样式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getDashCap ():RgfCapStyle
```

### 获取虚线样式
```cangjie
*  @return 线条帽样式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getDashStyle ():RgfDashStyle
```

### 获取线条联接样式
```cangjie
*  @return 联接样式
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getLineJoin ():RgfLineJoin
```

### 获取虚线偏移量
```cangjie
*  @return 偏移量
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getDashOffset ():Float32
```

### 获取斜接长度
```cangjie
*  @return 长度
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getMiterLimit ():Float32
```

### 获取虚线段定义数组
```cangjie
*  @return 数组
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getDashes():Array<Float32>
```

### 获取虚线段段数
```cangjie
*  @return 虚线段数组长度
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getDashesCount():Int64
```