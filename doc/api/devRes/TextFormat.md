# TextFormat

## 成员方法

### 获取文字流方向
```cangjie
*  @return 文字流方向
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFlowDirection():RgfTxtFlowDirection
```

### 设置文字流方向
```cangjie
*  @param  flowDirection 文字流方向
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setFlowDirection(flowDirection:RgfTxtFlowDirection):Bool
```

### 获取字体族名
```cangjie
*  @return 文字流方向
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontFamilyName():String
```

### 获取字体尺寸(DPI单位)
```cangjie
*  @return 字体尺寸
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontSize():Float32
```

### 获取字体延展
```cangjie
*  @return 字体延展
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontStretch():RgfTxtFontStretch
```

### 获取字体风格
```cangjie
*  @return 字体风格
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontStyle():RgfTxtFontStyle
```

### 获取字体权重
```cangjie
*  @return 字体权重
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontWeight():RgfTxtFontWeight
```

### 获取增量制表位值
```cangjie
*  @return 增量制表位值
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getIncrementalTabStop():Float32
```

### 设置增量制表位值
```cangjie
*  @return 增量制表位值
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setIncrementalTabStop(incrementalTabStop:Float32):Bool
```

### 获取行间距
```cangjie
*  @return 行间距
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getLineSpacing():Option<RgfTxtLineSpacing>
```

### 设置行间距
```cangjie
*  @param  lineSpacingMethod 行高确定模型
*  @param  lineSpacing       基线间距
*  @param  baseline          线条顶部至基线的距离
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setLineSpacing(lineSpacingMethod:RgfTxtLineSpacingMethod,lineSpacing:Float32,baseline:Float32):Bool
```

### 设置行间距
```cangjie
*  @param  lineSpacin 行间距
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setLineSpacing(lineSpacin:RgfTxtLineSpacing):Bool
```

### 获取区域设置名
```cangjie
*  @return 区域设置名
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getLocaleName():String
```

### 获取当前段落对齐选项
```cangjie
*  @return 段落对齐选项
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getParagraphAlignment():RgfTxtParagraphAlignment
```

### 设置当前段落对齐选项
```cangjie
*  @param paragraphAlignment 段落对齐选项
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setParagraphAlignment(paragraphAlignment:RgfTxtParagraphAlignment):Bool
```

### 获取阅读方向
```cangjie
*  @return 阅读方向
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getReadingDirection():RgfTxtReadingDirection
```

### 设置阅读方向
```cangjie
*  @param readingDirection 阅读方向
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setReadingDirection(readingDirection:RgfTxtReadingDirection):Bool
```

### 获取文本对齐
```cangjie
*  @return 文本对齐
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getTextAlignment():RgfTxtTextAlignment
```

### 设置文本对齐
```cangjie
*  @param textAlignment 文本对齐
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setTextAlignment(textAlignment:RgfTxtTextAlignment):Bool
```

### 获取换行选项
```cangjie
*  @return 换行选项
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getWordWrapping():RgfTxtWordWrapping
```

### 设置换行选项
```cangjie
*  @param wordWrapping 换行选项
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setWordWrapping(wordWrapping:RgfTxtWordWrapping):Bool
```

### 设置行裁剪
```cangjie
*  为溢出布局宽度的文本设置剪裁选项
*  @param trimmingOptions 裁剪选项
*  @param 文本内联对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setTrimming(trimmingOptions:RgfTxtTrimming,trimmingSign:Option<TextInlineObj>):Bool
```

### 设置行裁剪
```cangjie
*  为溢出布局宽度的文本设置剪裁选项
*  @param trimming 行裁剪
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setTrimming(trimming:RgfTxtTrimmingPck):Bool
```

### 获取行裁剪
```cangjie
*  @return 行裁剪
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getTrimming():Option<RgfTxtTrimmingPck>
```