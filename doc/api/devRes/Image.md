# Image

## 成员方法

### 获取图像矩形
```cangjie
/**
*  @return 矩形
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getRect():RgfRect
```

### 获取图像宽度
```cangjie
/**
*  @return 宽度
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public open func getWidth ():Float32
```

### 获取图像高度
```cangjie
/**
*  @return 高度
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public open func getHeight ():Float32
```

### 获取图像横向DPI
```cangjie
/**
*  @return DPI
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public open func getDpiX ():Float32
```

### 获取图像纵向DPI
```cangjie
/**
*  @return DPI
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public open func getDpiY ():Float32
```