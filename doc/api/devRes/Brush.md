# Brush

## 成员方法

### 获取矩阵
```cangjie
/**
*  @brief 获取画刷线宽
*  @return 线宽
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getLineWidth ():Float32
```

### 获取矩阵
```cangjie
/**
*  @brief 设置画刷线宽
*  @param width 线宽
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setLineWidth (width:Float32):Bool
```