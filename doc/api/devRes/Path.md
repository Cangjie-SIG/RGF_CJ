# Path

## 成员方法

### 设置填充模式
```cangjie
*  @param mode 填充模式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setFillMode (mode:RgfFillMode):Bool
```

### 开始描绘
```cangjie
*  @param sx x坐标
*  @param sy y坐标
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note beginFigure 和 endFigure 之间的绘制被视为一个路径主体
*/
public func beginFigure (sx:Float32,sy:Float32):Bool
```

### 添加贝塞尔曲线
```cangjie
*  @param cx1 x坐标
*  @param cy1 y坐标
*  @param cx2 x坐标
*  @param cy2 y坐标
*  @param ex  控制点坐标
*  @param ey  控制点坐标
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func addBezier ( cx1:Float32, cy1:Float32, cx2:Float32, cy2:Float32, ex:Float32, ey:Float32):Bool
```

### 添加直线
```cangjie
*  @param cx1 x坐标
*  @param cy1 y坐标
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func addLine (ex:Float32, ey:Float32):Bool
```

### 结束描绘
```cangjie
*  @param figureEnd 结束类型
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func endFigure (figureEnd:RgfFigureEnd):Bool
```

### 关闭
```cangjie
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*  @attention 当所有路径主体都绘制完成后应该调用close关闭路径描绘
*/
public func close ():Bool
```