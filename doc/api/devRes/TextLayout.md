# TextLayout <: [TextFormat](./TextFormat.md)

## 成员方法

### 获取字体权重
```cangjie
*  @param  currentPosition 当前位置
*  @return 字体权重
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontWeight(currentPosition:UInt32):Option<RgfTxtRangeFontWeight>
```

### 设置字体权重
```cangjie
*  @param  fontWeight   字体权重
*  @param  textRange    文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setFontWeight(fontWeight:RgfTxtFontWeight,textRange:RgfTxtTextRange):Bool
```

### 获取区域名
```cangjie
*  @param  currentPosition 当前位置
*  @return 区域名
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getLocaleName(currentPosition:UInt32):Option<RgfTxtRangeName>
```

### 设置区域名
```cangjie
*  @param  localeName   区域名
*  @param  textRange    文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setLocaleName(localeName:String,textRange:RgfTxtTextRange):Bool
```

### 获取字体族名
```cangjie
*  @param  currentPosition 当前位置
*  @return 字体族名
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontFamilyName(currentPosition:UInt32):Option<RgfTxtRangeName>
```

### 设置字体族名
```cangjie
*  @param  fontFamilyName   字体族名
*  @param  textRange        文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setFontFamilyName(fontFamilyName:String,textRange:RgfTxtTextRange):Bool
```

### 设置字号
```cangjie
*  @param  fontSize     字号
*  @param  textRange    文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setFontSize(fontSize:Float32,textRange:RgfTxtTextRange):Bool
```

### 获取字号
```cangjie
*  @param  currentPosition 当前位置
*  @return 字号
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontSize(currentPosition:UInt32):Option<RgfTxtRangeTextSize>
```

### 设置字体延展
```cangjie
*  @param  fontStretch  字体延展
*  @param  textRange    文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setFontStretch(fontStretch:RgfTxtFontStretch,textRange:RgfTxtTextRange):Bool
```

### 获取字体延展
```cangjie
*  @param  currentPosition 当前位置
*  @return 字体延展
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontStretch(currentPosition:UInt32):Option<RgfTxtRangeFontStretch>
```

### 设置字体风格
```cangjie
*  @param  fontStyle  字体风格
*  @param  textRange  文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setFontStyle(fontStyle:RgfTxtFontStyle,textRange:RgfTxtTextRange):Bool
```

### 获取字体风格
```cangjie
*  @param  currentPosition 当前位置
*  @return 字体风格
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getFontStyle(currentPosition:UInt32):Option<RgfTxtRangeFontStyle>
```

### 获取布局可以设置的最小宽度
```cangjie
*  @return 最小宽度
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func determineMinWidth():Option<Float32>
```

### 获取测量数据
```cangjie
*  @return 文本测量数据
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getMetrics():Option<RgfTxtTextMetrics>
```

### 获取每个字符的测量数据
```cangjie
*  @return 字符测量数据
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getClusterMetrics():Option<Array<RgfTxtClusterMetrics>>
```

### 获取每一行的测量数据
```cangjie
*  @return 行测量数据
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getLineMetrics():Option<Array<RgfTxtLineMetrics>>
```

### 获取最大宽度
```cangjie
*  @param  currentPosition 当前位置
*  @return 最大宽度
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getMaxWidth():Float32
```

### 设置最大宽度
```cangjie
*  @param  maxWidth  最大宽度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setMaxWidth(maxWidth:Float32):Bool
```

### 获取最大高度
```cangjie
*  @param  currentPosition 当前位置
*  @return 最大高度
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getMaxHeight():Float32
```

### 设置最大高度
```cangjie
*  @param  maxWidth  最大高度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setMaxHeight(maxHeight:Float32):Bool
```

### 获取布局及其包含的所有对象的悬垂
```cangjie
*  获取布局及其包含的所有对象的悬垂（以设备独立像素，DIPs 为单位），包括文本字形和内联对象。
*  @return 悬垂
*  @exception 若请求时底层对象未创建，则抛出异常
*  @note 悬垂 (Overhangs)：指的是对象超出其指定边界框的部分。在文本渲染中，某些字符可能会有部分区域延伸到它们的主要边界之外。
*/
public func getOverhangMetrics():Option<RgfTxtOverhangMetrics>
```

### 获取删除线
```cangjie
*  @param  currentPosition 当前位置
*  @return 删除线
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getStrikethrough(currentPosition:UInt32):Option<RgfTxtTextRange>
```

### 获取下划线
```cangjie
*  @param  currentPosition 当前位置
*  @return 下划线
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getUnderline(currentPosition:UInt32):Option<RgfTxtTextRange>
```

### 测试坐标
```cangjie
*  应用程序调用此函数传入相对于布局框左上角位置的特定像素位置，并获取发生命中测试的文本字符串的对应命中测试指标的信息
*  @param  pointX x轴位置
*  @param  pointY y轴位置
*  @return 测试结果
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func hitTestPoint(pointX:Float32,pointY:Float32):Option<RgfTxtHitTestMetricsPosPck>
```

### 测试文本位置
```cangjie
*  应用程序调用此函数以获取相对于布局框左上角的像素位置（给定文本位置和位置的逻辑侧）
*  @param  textPosition  文本位置
*  @param  isTrailingHit 尾
*  @return 测试结果
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func hitTestTextPosition(textPosition:UInt32,isTrailingHit:Bool):Option<RgfTxtHitTestMetricsTxtPck>
```

### 测试文本范围
```cangjie
*  应用程序调用此函数以获取相对于布局框左上角的像素位置（给定文本位置和位置的逻辑侧）
*  @param  textPosition  文本位置
*  @param  textLength    文本长度
*  @param  originX       x轴偏移量
*  @param  originY       x轴偏移量
*  @return 测试结果
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func hitTestTextRange(textPosition:UInt32,textLength:UInt32,originX:Float32,originY:Float32):Option<Array<RgfTxtHitTestMetrics>>
```

### 设置删除线
```cangjie
*  @param  hasStrikethrough 是否启用
*  @param  textRange        文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setStrikethrough(hasStrikethrough:Bool,textRange:RgfTxtTextRange):Bool
```

### 设置删除线
```cangjie
*  @param  hasStrikethrough 是否启用
*  @param  startPosition    开始位置
*  @param  length           长度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setStrikethrough(hasStrikethrough:Bool,startPosition:UInt32,length:UInt32):Bool
```

### 设置下划线
```cangjie
*  @param  hasUnderline 是否启用
*  @param  textRange    文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setUnderline(hasUnderline:Bool,textRange:RgfTxtTextRange):Bool
```

### 设置下划线
```cangjie
*  @param  hasUnderline  是否启用
*  @param  startPosition 开始位置
*  @param  length        长度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setUnderline(hasUnderline:Bool,startPosition:UInt32,length:UInt32):Bool
```

### 设置行内对象
```cangjie
*  @param  object  行内对象
*  @param  textRange  文本范围
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setInlineObject(object:TextInlineObj,startPosition:UInt32,length:UInt32):Bool
```

### 设置行内对象
```cangjie
*  @param  object           行内对象
*  @param  startPosition    开始位置
*  @param  length           长度
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setInlineObject(object:TextInlineObj,textRange:RgfTxtTextRange):Bool
```

### 获取行内对象
```cangjie
*  @param  currentPosition 当前位置
*  @return 行内对象
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getInlineObject(currentPosition:UInt32):Option<RgfTxtRangeInlineObj>
```