# MemBitmap

## 成员方法

### 从文件加载位图
```cangjie
/**
*  @param uri   文件路径
*/
public init(uri:String)
```

### 从文件加载位图
```cangjie
/**
*  @param uri   文件路径
*  @return 是否加载成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func loadFromFile (uri:String):Bool
```

### 从屏幕截取图像作为位图
```cangjie
/**
*  @return 是否加载成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func loadFromScreen ():Bool
```

### 将内存位图保存到文件中
```cangjie
/**
*  @brief 将内存位图保存到文件中
*  @return 是否加载成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func saveToFile (uri:String, format:RgfImgFormat):Bool
```

### 根据指定模式遍历像素
```cangjie
/**
*  @brief 根据指定模式遍历像素
*  @param x             左边
*  @param y             顶边
*  @param width         宽度
*  @param height        高度
*  @param callback      像素色彩回调函数
*  @param lockMod       访问模式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func traversePixels(x:UInt32, y:UInt32, width:UInt32 , height:UInt32,callback:(color:Box<RgfFColor>) -> Unit,lockMod:RgfImgLockMod):Bool
```

### 根据指定模式遍历像素
```cangjie
/**
*  @brief 根据指定模式遍历像素
*  @param rect          矩形
*  @param callback      像素色彩回调函数
*  @param lockMod       访问模式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func traversePixels(rect:RgfRect,callback:(color:Box<RgfFColor>) -> Unit,lockMod:RgfImgLockMod):Bool
```

### 根据指定模式遍历像素
```cangjie
/**
*  @brief 根据指定模式遍历像素
*  @param rect          浮点矩形
*  @param callback      像素色彩回调函数
*  @param lockMod       访问模式
*  @return 是否成功
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func traversePixels(rect:RgfFRect,callback:(color:Box<RgfFColor>) -> Unit,lockMod:RgfImgLockMod):Bool
```