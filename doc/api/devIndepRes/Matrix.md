# Matrix

## 成员方法

### 创建矩阵
```cangjie
/** 
*  @brief 创建变换矩阵
*  @param m11  1行1列值
*  @param m12  1行2列值
*  @param m21  2行1列值
*  @param m22  2行2列值
*  @param dx   3行1列值
*  @param dy   3行2列值
*/
public init(m11!:Float32 = 1.0, m12!:Float32 = 0.0, m21!:Float32 = 0.0, m22!:Float32 = 1.0, dx!:Float32 = 0.0, dy!:Float32 = 0.0)
```

### 设置为单位矩阵
```cangjie
/**
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func identity ():Unit
```

### 判断是否为单位矩阵
```cangjie
/**
*  @return 是否为单位矩阵
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func isIdentity ():Bool
```

### 判断是否为逆转矩阵
```cangjie
/**
*  @return 是否为逆转矩阵
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func isInvertible ():Bool
```

### 逆转矩阵
```cangjie
/**
*  @return 是否成功逆转矩阵
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func invert ():Bool
```

### 旋转
```cangjie
/**
*  @param angle 角度
*  @param centerX 中心点X轴坐标
*  @param centerY 中心点Y轴坐标
*  @param ord 顺序
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func rotation (angle:Float32, centerX:Float32, centerY:Float32, ord:RgfMatrixOrder):Unit
```

### 缩放
```cangjie
/**
*  @param scaleX X轴缩放值
*  @param scaleY Y轴缩放值
*  @param ord 顺序
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func scale (scaleX:Float32, scaleY:Float32, ord:RgfMatrixOrder):Unit
```

### 平移
```cangjie
/**
*  @param offsetX X轴偏移量
*  @param offsetY Y轴偏移量
*  @param ord 顺序
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func translation (offsetX:Float32, offsetY:Float32, ord:RgfMatrixOrder):Unit
```

### 斜切
```cangjie
/**
*  @param shearX X轴的扭曲角度
*  @param shearY Y轴倾斜角度
*  @param ord 顺序
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func shear (shearX:Float32, shearY:Float32, ord:RgfMatrixOrder):Unit
```

### 使用此矩阵转换指定的点并返回结果
```cangjie
/**
*  @param x x坐标
*  @param y y坐标
*  @return 浮点坐标值
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func transformPoint (x:Float32, y:Float32):RgfFPoint
```

### 将两个矩阵相乘，并将结果存储在此矩阵中
```cangjie
/**
*  @param matrix 另一个矩阵
*  @param ord 顺序
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func setProduct (matrix:Matrix, ord:RgfMatrixOrder):Unit
```

### 获取当前矩阵值
```cangjie
/**
*  @return 3X3矩阵
*  @exception 若请求时底层对象未创建，则抛出异常
*/
public func getMatrix ():Rgf3X3Matrix
```