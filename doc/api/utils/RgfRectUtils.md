# RgfRectUtils

## 成员方法

### 转文本
```cangjie
*  @return 格式化文本对象
*  本方法将内部内容转换为格式化字符串
*/
public static func toString<T>(rect:RgfRect)
```

### 检查两个矩形是否相交
```cangjie
*  @param rect1 矩形
*  @param rect2 矩形
*  @return 返回相交部分
*/
public static func intersect(rect1:RgfRect, rect2:RgfRect):Option<RgfRect>
```

### 检查两个矩形是否并肩，即横向时顶底一致，纵向时左右一致
```cangjie
*  @param rect1 矩形
*  @param rect2 矩形
*  @return 0 无; 1;横向并列 2;总向并列
*/
public static func abreast(rect1:RgfRect, rect2:RgfRect):Int8
```

### 计算两个RECT相减后的剩余区域矩形集合
```cangjie
*  @param rect1 矩形
*  @param rect2 矩形
*  @param haveIntersect 若已有相交部分则传入
*  @return 返回减去后的矩形数组
*/
public static func subtract(rect1:RgfRect, rect2:RgfRect, haveIntersect!:Option<RgfRect> = Option<RgfRect>.None): Array<RgfRect>
```

### 计算两个RECT相加的结果
```cangjie
*  @param rect1 矩形
*  @param rect2 矩形
*  @param haveIntersect 若已有相交部分则传入
*  @return 成功则返回相加后的矩形
*/
public static func addition(rect1:RgfRect, rect2:RgfRect, haveAbreast!:Option<Int8> = Option<Int8>.None):Option<RgfRect>
```

### 计算所有矩形的外接矩形
```cangjie
*  @param arr 矩形数组
*  @return 成功则返回相加后的矩形
*/
public static func bounding(arr:Array<RgfRect>):Option<RgfRect>
```

### 判断一个点是否在矩形内（包括边界情况）
```cangjie
*  @param rect 矩形数组
*  @param pos  坐标点
*  @return 是否存在
*/
public static func posInRect(rect:RgfRect,pos:RgfPoint):Bool
```