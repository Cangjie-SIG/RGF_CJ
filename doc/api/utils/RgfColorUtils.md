# RgfColorUtils

## 成员方法

### 色彩线性差值
```cangjie
*  @param color1 色彩1
*  @param color2 色彩2
*  @param t      步值 [0,1]
*  @return 插值后的浮点色彩值
*/
public static func lerp(color1:RgfFColor, color2:RgfFColor, t:Float32):RgfFColor
```