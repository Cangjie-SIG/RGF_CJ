# RGF 核心设计

`RGF`是一个`Windows`系统下基于`Direct3D`、`Direct2D`、`DXGI`、`DirectWrite`、`WIC`、`GDI`、`GDIplus`等技术开发的通用渲染框架。`RGF`屏蔽了底层技术的差异，确保不论开发者选择何种底层技术都能得到高度一致的渲染结果。此外，`RGF`将`Windows`窗口过程封装入类，确保各窗口消息队列之间的独立性，并且提供生命周期函数，简化开发过程。`RGF`基于`Windows`系统线程为核心`UI`线程，渲染与仓颉用户态线程分离。`RGF`还提供便于开发的相关方法，诸如：缓动函数、色彩插值、矩形运算等。关于`RGF`的核心设计内容详见下文。

## 技术架构图

<img alt="" src="./assets/architecture.png" style="display: inline-block;" width=100%/>

## 生命周期

<img alt="" src="./assets/lifecycle.png" style="display: inline-block;" width=100%/>

## RGF 类 / 接口概述

主要类和函数接口说明，详见 [API](./feature_api.md)

```shell

-- 窗口相关 --

RgfCore             // 本类为RGF核心，是窗口/组件的基本类，本类包含了RGF的基础生命周期回调
RgfBase             // 本类继承了RgfCore并且实现IRgfBaseMethod，是GUI开发中最常用的基类

-- 渲染相关(设备无关资源) --

MemBitmap           // 本类提供内存位图相关操作
Matrix              // 本类提供矩阵运算相关操作

-- 渲染相关(设备相关资源) --

Image               // 图像基类
Bitmap              // 本类提供位图相关操作
Layer               // 本类提供图层相关操作
Brush               // 画刷基类
BitmapBrush         // 本类提供位图画刷相关操作
LinearGradientBrush // 本类提供渐变画刷相关操作
SolidColorBrush     // 本类提供纯色画刷相关操作
Path                // 本类提供路径相关操作
StrokeStyle         // 本类提供线段风格相关操作
TextCore            // 本类提供文本渲染核心
TextInlineObj       // 本类提供文本内联对象相关操作
TextFormat          // 本类提供文本格式相关操作
TextLayout          // 本类提供文本布局相关操作

-- 渲染表面 --

RgfRenderer         // 窗口渲染表面，随 RgfCore 基类自动管理
RgfBindRenderer     // 绑定式渲染表面，需要自行绑定到渲染设备上，自带生命周期

-- 工具 --

RgfColorUtils       // 色彩运算工具类
RgfRectUtils        // 矩形运算工具类

-- 接口相关 --

IObjLink            // 本接口是RGF仓颉实现C/C++侧安全管理的基础接口
IRgfBaseMethod      // 本接口为基础方法接口，其中提供了常用的方法
IDrawContent        // 绘制接口
IDrawCreater        // 资源创建接口

-- 结构 -- 

RgfSize             // 尺寸
RgfRect             // 矩形
RgfPoint            // 点位
RgfFRect            // 浮点矩形
RgfFColor           // 浮点颜色
RgfFPoint           // 浮点点位
Rgf3X3Matrix        // 3X3矩阵
RgfPctColor         // 百分比色彩
RgfContext          // RGF上下文
RgfWinContext       // 窗口上下文
```

## 使用建议

开发者在使用`RGF`时，可通过本文档及接口文档开发。但随着涉及领域的深入，开发者可能会需要深入了解底层技术。因此，以下列举出开发者可能会使用的相关知识链接：

 - [Direct2D](https://learn.microsoft.com/zh-cn/windows/win32/direct2d/getting-started-with-direct2d?redirectedfrom=MSDN)
 - [GDI](https://learn.microsoft.com/zh-cn/windows/win32/gdi/windows-gdi)
 - [GDI+](https://learn.microsoft.com/zh-cn/windows/win32/gdiplus/-gdiplus-gdi-start)
 - [DXGI](https://learn.microsoft.com/zh-cn/windows/win32/direct3ddxgi/dx-graphics-dxgi)
 - [Direct3D](https://learn.microsoft.com/zh-cn/windows/win32/getting-started-with-direct3d)
 - [DirectWrite](https://learn.microsoft.com/zh-cn/windows/win32/directwrite/direct-write-portal)
 - [WIC](https://learn.microsoft.com/zh-cn/windows/win32/wic/-wic-programming-guide)
 - [PPL](https://learn.microsoft.com/zh-cn/cpp/parallel/concrt/parallel-patterns-library-ppl?view=msvc-170)
 - [Win32](https://learn.microsoft.com/zh-cn/windows/win32/desktop-programming)