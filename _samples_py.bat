@echo off
setlocal enabledelayedexpansion

REM 获取当前目录
set "current_dir=%cd%"

cd %current_dir%\tool\python

:: 执行 python 脚本
python proj_clear_build_test.py -lang zh-cn --nsty --sp


echo script execution completed...
pause
endlocal