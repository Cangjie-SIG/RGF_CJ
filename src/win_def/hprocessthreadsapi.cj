/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package rgf.win_def


@C
public struct SECURITY_ATTRIBUTES {
    public var nLength:DWORD;
    public var lpSecurityDescriptor:LPVOID;
    public var bInheritHandle:WINBOOL;
    public init() {
        nLength = 0;
        lpSecurityDescriptor = CPointer<Unit>();
        bInheritHandle = 0;
    }
}
public type PSECURITY_ATTRIBUTES = CPointer<SECURITY_ATTRIBUTES>;
public type LPSECURITY_ATTRIBUTES = CPointer<SECURITY_ATTRIBUTES>;
public type SIZE_T = ULONG_PTR 

public type PTHREAD_START_ROUTINE = CFunc<(lpThreadParameter:LPVOID) -> DWORD>
public type LPTHREAD_START_ROUTINE = PTHREAD_START_ROUTINE

@CallingConv[STDCALL]
foreign func CreateThread (lpThreadAttributes:LPSECURITY_ATTRIBUTES, dwStackSize:SIZE_T, lpStartAddress:LPTHREAD_START_ROUTINE, lpParameter:LPVOID , dwCreationFlags:DWORD , lpThreadId:LPDWORD ):HANDLE
public func winCreateThread (lpThreadAttributes:LPSECURITY_ATTRIBUTES, dwStackSize:SIZE_T, lpStartAddress:LPTHREAD_START_ROUTINE, lpParameter:LPVOID , dwCreationFlags:DWORD , lpThreadId:LPDWORD ):HANDLE { unsafe { CreateThread(lpThreadAttributes,dwStackSize,lpStartAddress,lpParameter,dwCreationFlags,lpThreadId) } }


