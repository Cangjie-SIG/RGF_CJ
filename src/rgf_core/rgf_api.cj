/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package rgf.rgf_core
import rgf.win_def.*

@CallingConv[STDCALL]
foreign func rApiCharStrToWcharStr (content:CString):LPWSTR;
/**
*  @brief 将Cangjie字符串转换为C宽字符串(含编码转换)
*  @param str 仓颉字符串
*  @return 宽字符串指针
*  @note 本函数创建的字符串需要使用  rgfReleaseWcharStr() 方法释放
*/
public func rgfStrToWcharStr (str: String):LPWSTR{
    let tmp:CString = unsafe{ LibC.mallocCString(str) }
    let wchar:LPCWSTR = unsafe{ rApiCharStrToWcharStr(tmp) }
    unsafe{ LibC.free(tmp) }
    return wchar
}

@CallingConv[STDCALL]
foreign func rApiWcharStrToCharStr (content:LPWSTR):CString;
/**
*  @brief 将C宽字符串转换为Cangjie字符串(含编码转换)
*  @param str 宽字符串指针
*  @return 仓颉字符串
*/
public func rgfWcharStrToCharStr (str: LPWSTR):String{
    let char:CString = unsafe{ rApiWcharStrToCharStr(str) }
    let wchar:String = char.toString()
    unsafe{ rgfFree(CPointer<Unit>(char.getChars())) }
    return wchar
}

@CallingConv[STDCALL]
foreign func rApiFree (cPtr:CPointer<Unit>):Unit;
/**
*  @brief 释放宽字符串
*  本函数用于释放 rgfStrToWcharStr 函数申请的内存空间
*  @param cPtr 宽字符串指针
*/
public func rgfFree (cPtr:CPointer<Unit>):Unit{ unsafe{ rApiFree(cPtr) } }
internal func rApiReleaseWcharStr(cPtr:LPWSTR):Unit{ unsafe{ rgfFree(CPointer<Unit>(cPtr)) } }
@CallingConv[STDCALL]
foreign func rApiWcharslen(wcharPtr:LPWSTR):UInt32
/**
*  @brief 获取C宽字符串长度
*  @return 字符串长度（宽字符数）
*/
public func rgfWcharslen(wcharPtr:LPWSTR):UInt32{ unsafe{ rApiWcharslen(wcharPtr) } }

@CallingConv[STDCALL]
@FastNative
foreign func rApiUll2Ptr(ptrVal:UIntNative):CPointer<Unit>
/**
*  @brief 将无符号数值转换为C侧指针
*  本函数用于明确指针地址值时，将地址恢复为指针
*  @param ptrVal 指针地址
*  @return 指针
*/
public func rgfUll2Ptr(ptrVal:UIntNative):CPointer<Unit>{ unsafe{ rApiUll2Ptr(ptrVal) } }

@CallingConv[STDCALL]
@FastNative
foreign func rApill2Ptr(ptrVal:IntNative):CPointer<Unit>
/**
*  @brief 将有符号数值转换为C侧指针
*  本函数用于明确指针地址值时，将地址恢复为指针（极少数情况下使用）
*  @param ptrVal 指针地址
*  @return 指针
*/
public func rgfll2Ptr(ptrVal:IntNative):CPointer<Unit>{ unsafe{ rApill2Ptr(ptrVal) } }

@CallingConv[STDCALL]
foreign func rApiTween (percentage:Float32, StartValue:Float32, TargetValue:Float32, CurveType:UInt8):Float32;
/**
*  @brief RGF缓动函数
*  本函数用于将线性运动转为非线性运动
*  @param percentage  百分比(0.~1.)
*  @param startValue  起始值
*  @param targetValue 目标值
*  @param curveType   缓动函数类型
*  @return 指针
*/
public func rgfTweenFun(percentage:Float32, startValue:Float32, targetValue:Float32, curveType:rgfTween):Float32{ unsafe{ rApiTween(percentage,startValue,targetValue,rgfTween.e2Num(curveType)) } }

@CallingConv[STDCALL]
foreign func rApiSetCurrentDirectoryToExe():Unit;
/**
*  @brief 模块当前路径配置
*  本函数将当前程序的存储路径作为相对路径的起始点
*/
public func rgfSetCurrentDirectoryToModule():Unit{ unsafe{ rApiSetCurrentDirectoryToExe() } }


@CallingConv[STDCALL]
foreign func rApiInitialize ():RgfContextPtr;
/**
*  @brief RGF绘制库全局初始化
*  @return Token RGF全局上下文
*  本函数创建的上下文环境需要使用 rgfDestroy() 方法释放
*/
public func rgfInitialize():RgfContextPtr{ unsafe{ rApiInitialize() } };
@CallingConv[STDCALL]
foreign func rApiDestroy (token:RgfContextPtr):Bool;
/**
*  @brief RGF绘制库全局释放
*  @param token RGF库全局上下文
*  @return 是否成功释放
*/
public func rgfDestroy (token:RgfContextPtr):Bool{ unsafe{ rApiDestroy(token) } };
@CallingConv[STDCALL]
foreign func rApiRegisterWinClass (className:LPCTSTR, hInstance:HINSTANCE, hbrBackground:INT, style:UINT):RgfWinCtxt;
/**
*  @brief RGF绘制库封装的窗口类注册方法
*  @param className        窗口类名
*  @param hInstance        程序实例
*  @param hbrBackground    窗口背景画刷
*  @param style            窗口风格
*  @note 非本类注册的窗口类需要手动注入窗口过程函数，否则RGF窗口类无法捕获系统事件
*/
public func rgfRegisterWinClass (ctxt:RgfWinContext,className:String, hInstance:RgfContext, hbrBackground:Int32, style:UInt32):Unit{ 
    let str:LPWSTR = unsafe{ rgfStrToWcharStr(className) }
    ctxt.winClassCTxtPtr = unsafe{ rApiRegisterWinClass(str,hInstance.hInst,hbrBackground,style) } 
    unsafe{
        rgfFree(CPointer<Unit>(str))
    }
}
@CallingConv[STDCALL]
foreign func rApiUnregisterWinClass (winClass:RgfWinCtxt):Bool;
/**
*  @brief RGF绘制库封装的窗口类注销方法
*  @param className        窗口类名
*  @param hInstance        程序实例
*  @note 凡注册的窗口类都应该由本方法注销
*/
public func rgfUnregisterWinClass (ctxt:RgfWinContext):Bool{ return unsafe{ rApiUnregisterWinClass(ctxt.winClassCTxtPtr) } }

// 以下为 类 相关函数

// -------------------------------------------------------------------------------------------------------- 主窗口类

@CallingConv[STDCALL]
foreign func rApiGfClassCreate (
    cThis:CContext, 
    wndproc:rgfClassWndProc,
    initFun:rgfClassInit,
    cDevRes:rgfClassCreateDeviceResources,
    dDevRes:rgfClassDestroyDeviceResources,
    onPaint:rgfClassOnPaint,
    created:rgfClassCreated,
    rendrCtd:rgfClassRendererCreated,
    rendrDtd:rgfClassRendererDestroyed,
    bfCreate:rgfClassBeforeCreate,
    bfDestory:rgfClassBeforeDestroy,
    destroyed:rgfClassDestroyed 
):PackageClass;
@CallingConv[STDCALL]
foreign func rApiGfClassRelease (rgfClass:PackageClass):Bool;
@CallingConv[STDCALL]
foreign func rApiGfClassCreateWin (
    rgfClass:PackageClass,
    winClass:RgfWinCtxt,lpWindowName:LPCTSTR,
    x:INT, y:INT, nWidth:INT, nHeight:INT,
    hWndParent:HWND,
    dwExStyle:DWORD,
    dwStyle:DWORD,
    bgMod:INT, 
    token:RgfContextPtr, 
    sdog:INT
):HWND;
@CallingConv[STDCALL]
foreign func rApiGfClassSetDrawNCarea (rgfClass:PackageClass,expand:Bool):Unit;
@CallingConv[STDCALL]
foreign func rApiGfClassDestroyWin (rgfClass:PackageClass):Unit;
@CallingConv[STDCALL]
foreign func rApiGfClassSetMainWin (rgfClass:PackageClass,isMainWin:Bool):Unit;
@CallingConv[STDCALL]
foreign func rApiGfClassGetMainWin (rgfClass:PackageClass):Bool;
@CallingConv[STDCALL]
foreign func rApiGfClassGetHWND    (rgfClass:PackageClass):HWND;
@CallingConv[STDCALL]
foreign func rApiGfClassCallPaint  (rgfClass:PackageClass,wRect:RECT):Unit;
@CallingConv[STDCALL]
foreign func rApiGfClassCallProc   (rgfClass:PackageClass,hWnd:HWND,msg:UINT,wParam:WPARAM,lParam:LPARAM):LRESULT
@CallingConv[STDCALL]
foreign func rApiGfClassDisableWinRenderEvent(rgfClass:PackageClass,disable:Bool):Unit

// 绑定式渲染表面
@CallingConv[STDCALL]
foreign func rApiBSfcClassCreate (hdc:HDC,nWidth:INT , nHeight:INT , dpiX:Float32 , dpiY:Float32 , SDOG:INT ):RgfBSfc;
@CallingConv[STDCALL]
foreign func rApiBSfcClassRelease (bindSfcClass:RgfBSfc):Bool;
@CallingConv[STDCALL]
foreign func rApiSfccorClassBlendDraw (bindSfcClass:RgfBSfc,destX:INT ,destY:INT ,destW:INT ,destH:INT ,srcX:INT ,srcY:INT ,srcW:INT ,srcH:INT , alpha:Float32 ):Bool;
// (SurfaceCore) 表面核心类相关
@CallingConv[STDCALL]
foreign func rApiSfccorClassGlobalInheritance (sfccorClass:RgfSfccor, token:RgfContextPtr):Bool;
@CallingConv[STDCALL]
foreign func rApiSfccorClassCreateDeviceIndependentResources_ (sfccorClass:RgfSfccor):Bool;
@CallingConv[STDCALL]
foreign func rApiSfccorClassDestroyDeviceIndependentResources_ (sfccorClass:RgfSfccor):Bool;
@CallingConv[STDCALL]
foreign func rApiSfccorClassCreateDeviceResources_ (sfccorClass:RgfSfccor):Bool;
@CallingConv[STDCALL]
foreign func rApiSfccorClassDestroyDeviceResources_ (sfccorClass:RgfSfccor):Bool;
@CallingConv[STDCALL]
foreign func rApiSfccorClassReSetSurfaceSize (sfccorClass:RgfSfccor,nWidth:INT , nHeight:INT):Bool;

// (SurfaceUtils) 表面工具类相关

@CallingConv[STDCALL]
foreign func rApiSfcutsClassSaveSurfaceToFile (sfcutsClass:RgfSfcuts, Left:INT, Top:INT, Width:INT, Height:INT,uri:LPCWSTR, saveType:UInt8):Bool;
@CallingConv[STDCALL]
foreign func rApiSfcutsClassSaveSurfaceToMemory (sfcutsClass:RgfSfcuts, Left:INT, Top:INT, Width:INT, Height:INT):RgfMemBitmap;
@CallingConv[STDCALL]
foreign func rApiSfcutsClassSaveBitmapToFile (sfcutsClass:RgfSfcuts, Left:INT, Top:INT, Width:INT, Height:INT,pImg:RgfBitmap,uri:LPCWSTR, saveType:UInt8):Bool;
@CallingConv[STDCALL]
foreign func rApiSfcutsClassSaveBitmapToMemory (sfcutsClass:RgfSfcuts, Left:INT, Top:INT, Width:INT, Height:INT,pImg:RgfBitmap):RgfMemBitmap;
@CallingConv[STDCALL]
foreign func rApiSfcutsClassSaveLayerToFile (sfcutsClass:RgfSfcuts, Left:INT, Top:INT, Width:INT, Height:INT,pImg:RgfLayer,uri:LPCWSTR, saveType:UInt8):Bool;
@CallingConv[STDCALL]
foreign func rApiSfcutsClassSaveLayerToMemory (sfcutsClass:RgfSfcuts, Left:INT, Top:INT, Width:INT, Height:INT,pImg:RgfLayer):RgfMemBitmap;
@CallingConv[STDCALL]
foreign func rApiSfcutsClassGetRect (sfcutsClass:RgfSfcuts):RECT;

// 表面创建的对象释放接口
@CallingConv[STDCALL]
foreign func rApiSfcSubObjRelease (subObjPtr:CPointer<Unit>,subObjType:UInt8):Unit;
// (DrawCreater) 表面创建类相关
@CallingConv[STDCALL]
foreign func rApiDctClassCreateSolidColorBrush (dctClass:RgfDct,r:Float32 , g:Float32, b:Float32, a:Float32, width:Float32):RgfSolidColorBrh;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateStrokeStyle (dctClass:RgfDct,
        startCap:UInt8,
        endCap:UInt8,
        dashCap:UInt8,
        lineJoin:UInt8,
        miterLimit:Float32,
        dashStyle:UInt8,
        dashOffset:Float32,
        dashes:CPointer<Float32>,// inout VArray<Float32, $*>
        dashesCount:UInt32
    ):RgfStrokeStyle;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateLinearGradientBrush (dctClass:RgfDct,
    startColor:RgfColorF, 
    endColor:RgfColorF, 
    startPoint:RgfPointF, 
    endPoint:RgfPointF,
    midColors:CPointer<RgfColorF>,
    midPercentage:CPointer<Float32>,
    count:INT,
    useGammaCorrection:Bool
):RgfLinGdtBrh;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateBitmap (dctClass:RgfDct, Width:INT, Height:INT):RgfBitmap;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateBitmapFromFile (dctClass:RgfDct, uri:LPWSTR):RgfBitmap;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateBitmapFromMemory (dctClass:RgfDct, pMemBmp:RgfMemBitmap):RgfBitmap;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateBitmapBrush (dctClass:RgfDct, img:RgfImage):RgfBitmapBrh;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateBitmapBrushLay (dctClass:RgfDct, img:RgfImage):RgfBitmapBrh;
@CallingConv[STDCALL]
foreign func rApiDctClassCreatePath (dctClass:RgfDct):RgfPath;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateLayer (dctClass:RgfDct, Width:INT, Height:INT):RgfLayer;
@CallingConv[STDCALL]
foreign func rApiDctClassReSetLayerSize (dctClass:RgfDct, rLayerPtr:RgfLayer, Width:INT, Height:INT , Copybitmap:Bool):RgfLayer;
@CallingConv[STDCALL]
foreign func rApiDctClassCreateTextCore(dctClass:RgfDct, gamma:Float32,enhancedContrast:Float32,clearTypeLevel:Float32,renderingMode:UInt8):RgfTxtCore
@CallingConv[STDCALL]
foreign func rApiDctClassCreateTextInlineObject(dctClass:RgfDct,x:Float32,y:Float32,width:Float32,height:Float32,bmp:RgfImage):RgfTxtInlineObj
@CallingConv[STDCALL]
foreign func rApiDctClassCreateTextInlineObjectLay(dctClass:RgfDct,x:Float32,y:Float32,width:Float32,height:Float32,bmp:RgfImage):RgfTxtInlineObj
@CallingConv[STDCALL]
foreign func rApiDctClassCreateTextFormat(dctClass:RgfDct,family_name:LPCTSTR,collection:RgfFontColtion,weight:UInt16,style:UInt8,stretch:UInt8,size:Float32,locale:LPCTSTR):RgfTxtFormat
@CallingConv[STDCALL]
foreign func rApiDctClassCreateTextLayout(dctClass:RgfDct,string:LPCTSTR,len:UInt32,format:RgfTxtFormat,max_width:Float32,max_height:Float32):RgfTxtLayout

// (DrawContent) 绘制类相关
@CallingConv[STDCALL]
foreign func rApiDttClassUnlockRender (dttClass:RgfDtt):Unit;
@CallingConv[STDCALL]
foreign func rApiDttClasslockDangerous (dttClass:RgfDtt):Unit;
@CallingConv[STDCALL]
foreign func rApiDttClassUnlockDangerous (dttClass:RgfDtt):Unit;


@CallingConv[STDCALL]
foreign func rApiDttClassBeginDraw (dttClass:RgfDtt):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassEndDraw (dttClass:RgfDtt):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassClear (dttClass:RgfDtt,r:Float32,g:Float32,b:Float32,a:Float32):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassDrawLine (dttClass:RgfDtt,x1:Float32, y1:Float32, x2:Float32, y2:Float32, pBrush:RgfBrh, pStrokeStyle:RgfStrokeStyle):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassDrawRectangle (dttClass:RgfDtt,x:Float32, y:Float32, width:Float32, height:Float32, pBrush:RgfBrh, pStrokeStyle:RgfStrokeStyle):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassDrawEllipseHW (dttClass:RgfDtt,x:Float32, y:Float32, width:Float32, height:Float32, pBrush:RgfBrh, pStrokeStyle:RgfStrokeStyle):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassDrawEllipseR (dttClass:RgfDtt,x:Float32, y:Float32, xRadius:Float32, yRadius:Float32, pBrush:RgfBrh, pStrokeStyle:RgfStrokeStyle):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassDrawRoundedRectangle (dttClass:RgfDtt,x:Float32, y:Float32, width:Float32, height:Float32, xRadius:Float32, yRadius:Float32, pBrush:RgfBrh, pStrokeStyle:RgfStrokeStyle):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassFillRectangle (dttClass:RgfDtt,x:Float32, y:Float32, width:Float32, height:Float32, pBrush:RgfBrh):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassFillEllipseHW (dttClass:RgfDtt,x:Float32, y:Float32, width:Float32, height:Float32, pBrush:RgfBrh):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassFillEllipseR (dttClass:RgfDtt,x:Float32, y:Float32, xRadius:Float32, yRadius:Float32, pBrush:RgfBrh):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassFillRoundedRectangle (dttClass:RgfDtt,x:Float32, y:Float32, width:Float32, height:Float32, xRadius:Float32, yRadius:Float32, pBrush:RgfBrh):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassDrawPath (dttClass:RgfDtt, path:RgfPath, pBrush:RgfBrh, pStrokeStyle:RgfStrokeStyle):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassFillPath (dttClass:RgfDtt, path:RgfPath, pBrush:RgfBrh):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassPushLayer (dttClass:RgfDtt,targetX:Float32, targetY:Float32, targetW:Float32, targetH:Float32, pLayer:RgfLayer, sourceX:Float32, sourceY:Float32, sourceW:Float32, sourceH:Float32):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassPushLayerA (dttClass:RgfDtt,targetX:Float32, targetY:Float32, targetW:Float32, targetH:Float32, pLayer:RgfLayer, sourceX:Float32, sourceY:Float32, sourceW:Float32, sourceH:Float32, alpha:Float32):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassFillOpacityMask (dttClass:RgfDtt,targetX:Float32, targetY:Float32, targetW:Float32, targetH:Float32, mask:RgfImage, brush:RgfBrh, sourceX:Float32, sourceY:Float32, sourceW:Float32, sourceH:Float32):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassDrawBitmap (dttClass:RgfDtt,targetX:Float32, targetY:Float32, targetW:Float32, targetH:Float32, bmp:RgfBitmap, sourceX:Float32, sourceY:Float32, sourceW:Float32, sourceH:Float32, alpha:Float32):Bool;
@CallingConv[STDCALL]
foreign func rApiDttClassDrawTexts(dttClass:RgfDtt,pTcore:RgfTxtCore,pBrush:RgfBrh,pDwTxtLy:RgfTxtLayout,originX:Float32,originY:Float32):Bool
@CallingConv[STDCALL]
foreign func rApiDttClassSetTransform (dttClass:RgfDtt, transform:RgfMatrix):Unit;
@CallingConv[STDCALL]
foreign func rApiDttClassGetTransform (dttClass:RgfDtt, transform:RgfMatrix):Unit;
@CallingConv[STDCALL]
foreign func rApiDttClassSetAntialiasMode (dttClass:RgfDtt, antialias:UInt32):Unit;
@CallingConv[STDCALL]
foreign func rApiDttClassGetAntialiasMode (dttClass:RgfDtt):UInt32;

// (Matrix) 矩阵相关

@CallingConv[STDCALL]
foreign func rApiMtxClassCreateMatrix (m11:Float32, m12:Float32, m21:Float32, m22:Float32, dx:Float32, dy:Float32):RgfMatrix;
@CallingConv[STDCALL]
foreign func rApiMtxClassIdentity (mtxClass:RgfMatrix):Unit;
@CallingConv[STDCALL]
foreign func rApiMtxClassIsIdentity (mtxClass:RgfMatrix):Bool;
@CallingConv[STDCALL]
foreign func rApiMtxClassIsInvertible (mtxClass:RgfMatrix):Bool;
@CallingConv[STDCALL]
foreign func rApiMtxClassInvert (mtxClass:RgfMatrix):Bool;
@CallingConv[STDCALL]
foreign func rApiMtxClassRotation (mtxClass:RgfMatrix,angle:Float32, centerX:Float32, centerY:Float32, ord:UInt8):Unit;
@CallingConv[STDCALL]
foreign func rApiMtxClassScale (mtxClass:RgfMatrix,scaleX:Float32, scaleY:Float32, ord:UInt8):Unit;
@CallingConv[STDCALL]
foreign func rApiMtxClassTranslation (mtxClass:RgfMatrix,offsetX:Float32, offsetY:Float32, ord:UInt8):Unit;
@CallingConv[STDCALL]
foreign func rApiMtxClassShear (mtxClass:RgfMatrix,shearX:Float32, shearY:Float32, ord:UInt8):Unit;
@CallingConv[STDCALL]
foreign func rApiMtxClassTransformPoint (mtxClass:RgfMatrix,x:Float32, y:Float32):RgfPointF;
@CallingConv[STDCALL]
foreign func rApiMtxClassSetProduct (mtxClass:RgfMatrix, matrix:RgfMatrix, ord:UInt8):Unit;
@CallingConv[STDCALL]
foreign func rApiMtxClassGetMatrix (mtxClass:RgfMatrix):RgfMatrix3X3;

// (Layer) 图层相关

@CallingConv[STDCALL]
foreign func rApiLayClassGetWidth (layClass:RgfLayer):Float32;
@CallingConv[STDCALL]
foreign func rApiLayClassGetHeight (layClass:RgfLayer):Float32;
@CallingConv[STDCALL]
foreign func rApiLayClassGetDpiX (layClass:RgfLayer):Float32;
@CallingConv[STDCALL]
foreign func rApiLayClassGetDpiY (layClass:RgfLayer):Float32;

// (Image) 图像相关

@CallingConv[STDCALL]
foreign func rApiImgClassGetWidth (imgClass:RgfImage):Float32;
@CallingConv[STDCALL]
foreign func rApiImgClassGetHeight (imgClass:RgfImage):Float32;
@CallingConv[STDCALL]
foreign func rApiImgClassGetDpiX (imgClass:RgfImage):Float32;
@CallingConv[STDCALL]
foreign func rApiImgClassGetDpiY (imgClass:RgfImage):Float32;

// (Path) 路径相关

@CallingConv[STDCALL]
foreign func rApiPathClassSetFillMode (pathClass:RgfPath,mode:UInt8):Bool;
@CallingConv[STDCALL]
foreign func rApiPathClassBeginFigure (pathClass:RgfPath,sx:Float32,sy:Float32):Bool;
@CallingConv[STDCALL]
foreign func rApiPathClassAddBezier (pathClass:RgfPath, cx1:Float32, cy1:Float32, cx2:Float32, cy2:Float32, ex:Float32, ey:Float32):Bool;
@CallingConv[STDCALL]
foreign func rApiPathClassAddLine (pathClass:RgfPath,ex:Float32, ey:Float32):Bool;
@CallingConv[STDCALL]
foreign func rApiPathClassEndFigure (pathClass:RgfPath,figureEnd:UInt8):Bool;
@CallingConv[STDCALL]
foreign func rApiPathClassClose (pathClass:RgfPath):Bool;

// (StrokeStyle) 线段风格相关

@CallingConv[STDCALL]
foreign func rApiSstyClassGetStartCap (sstyClass:RgfStrokeStyle):UInt8;
@CallingConv[STDCALL]
foreign func rApiSstyClassGetEndCap (sstyClass:RgfStrokeStyle):UInt8;
@CallingConv[STDCALL]
foreign func rApiSstyClassGetDashCap (sstyClass:RgfStrokeStyle):UInt8;
@CallingConv[STDCALL]
foreign func rApiSstyClassGetDashStyle (sstyClass:RgfStrokeStyle):UInt8;
@CallingConv[STDCALL]
foreign func rApiSstyClassGetLineJoin (sstyClass:RgfStrokeStyle):UInt8;
@CallingConv[STDCALL]
foreign func rApiSstyClassGetDashOffset (sstyClass:RgfStrokeStyle):Float32;
@CallingConv[STDCALL]
foreign func rApiSstyClassGetMiterLimit (sstyClass:RgfStrokeStyle):Float32;

// (Brush) 画刷相关

@CallingConv[STDCALL]
foreign func rApiBrhClassGetLineWidth (brhClass:RgfBrh):Float32;
@CallingConv[STDCALL]
foreign func rApiBrhClassSetLineWidth (brhClass:RgfBrh, width:Float32):Bool;

// (LinearGradientBrus---h) 渐变画刷相关

@CallingConv[STDCALL]
foreign func rApiLinGdtBrhClassGetGammaCorrection (linGdtbrhClass:RgfLinGdtBrh):Bool;
@CallingConv[STDCALL]
foreign func rApiLinGdtBrhClassGetRectangle (linGdtbrhClass:RgfLinGdtBrh):RgfRectF;

// (SolidColorBrush) 纯色画刷相关

@CallingConv[STDCALL]
foreign func rApiSolidBrhClassGetColor (solidBrhClass:RgfSolidColorBrh):RgfColorF;
@CallingConv[STDCALL]
foreign func rApiSolidBrhClassSetColor (solidBrhClass:RgfSolidColorBrh, r:Float32, g:Float32, b:Float32, a:Float32):Bool;

// (MemBitmap) 内存位图相关

@CallingConv[STDCALL]
foreign func rApiMemBmpClassCreate ():RgfMemBitmap;
@CallingConv[STDCALL]
foreign func rApiMemBmpClassLoadFromFile (memBmp:RgfMemBitmap, uri:LPWSTR):Bool;
@CallingConv[STDCALL]
foreign func rApiMemBmpClassLoadFromScreen (memBmp:RgfMemBitmap):Bool;
@CallingConv[STDCALL]
foreign func rApiMemBmpClassSaveToFile (memBmp:RgfMemBitmap,uri:LPCWSTR,saveType:UInt8):Bool;
@CallingConv[STDCALL]
foreign func rApiMemBmpClassTraversePixels (memBmp:RgfMemBitmap,x:UINT, y:UINT, width:UINT , height:UINT , 
    callback:CFunc<(r:CPointer<UInt8>,g:CPointer<UInt8>,b:CPointer<UInt8>,a:CPointer<UInt8>,other:UINT_PTR) -> Unit>,
    lockMod:UInt8,other:UINT_PTR
):Bool;