/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package rgf.rgf_core
import rgf.win_def.*
public open class RgfBindRenderer <: RgfRendererCore {
    private var reCreate = false
    private var callbackCreateDeviceResources:Option<()->(Bool,Bool)> = Option<()->(Bool,Bool)>.None
    private var callbackOnPaint:Option<(wRect:RgfRect)->Bool> = Option<(wRect:RgfRect)->Bool>.None
    private var callbackDestroyDeviceResources:Option<()->(Bool,Bool)> = Option<()->(Bool,Bool)>.None
    public init(){
        super(CPointer<Unit>(),RgfSfcSubObjType.BindRenderer)
    }
    /**
    *  @brief 绑定
    *  @param token       Rgf全局上下文(令牌)
    *  @param hdc         设备上下文
    *  @param width       宽度
    *  @param height      高度
    *  @param substrate   底层技术 (推荐:Direct2D)
    *  @param dpiX        横向DPI
    *  @param dpiY        纵向DPI
    *  @return 是否成功
    */
    public open func bind(token:RgfContext, hdc:RgfHdc, width:Int32, height:Int32, substrate!:RgfSubstrate,dpiX!:Float32, dpiY!:Float32):Bool{
        if(isValid()){ return true }
        this.objPtr = unsafe { 
            rApiBSfcClassCreate (
                HDC(rgfUll2Ptr(hdc)),
                width , height ,
                dpiX , dpiY , 
                match(substrate){
                    case RgfSubstrate.Direct2D => 0
                    case _ => 1
                }
            )
        }
        var retv:Bool = unsafe { rApiSfccorClassGlobalInheritance(this.objPtr, token.ctxtPtr) }
        retv = retv && unsafe { rApiSfccorClassCreateDeviceIndependentResources_(this.objPtr) }
        retv = retv && unsafe { rApiSfccorClassCreateDeviceResources_(this.objPtr) }
        retv = retv && createDeviceResources_()
        if(!retv){
            release()
            return false
        }else{
            return true
        }
    }
    /**
    *  @brief 绑定
    *  @param token       Rgf全局上下文(令牌)
    *  @param hdc         设备上下文
    *  @param size        尺寸
    *  @param substrate   底层技术 (推荐:Direct2D)
    *  @param dpiX        横向DPI
    *  @param dpiY        纵向DPI
    *  @return 是否成功
    */
    public open func bind(token:RgfContext, hdc:RgfHdc, size:RgfSize, substrate!:RgfSubstrate,dpiX!:Float32, dpiY!:Float32):Bool{
        return bind(token,hdc,size.width,size.height,substrate:substrate,dpiX:dpiX,dpiY:dpiY)
    }
    /**
    *  @brief 释放对象
    *  @return 是否成功
    */
    public override open func release():Bool{
        //super.release()
        var retv:Bool = false
        if(isValid()){
            retv = destroyDeviceResources_()
            retv = retv && unsafe { rApiSfccorClassDestroyDeviceResources_(this.objPtr) }
            retv = retv && unsafe { rApiSfccorClassDestroyDeviceIndependentResources_(this.objPtr) }
            // 释放对象
            if(retv && unsafe { rApiBSfcClassRelease(objPtr) }){
                objPtr = CPointer<Unit>()
            }
        }
        return retv
    }
    /**
    *  @brief 重置尺寸
    *  @param width       宽度
    *  @param height      高度
    *  @return 是否成功
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    public open func reset (width:Int32 , height:Int32):Bool{
        if(isValid()){
            unsafe{ rApiDttClasslockDangerous(this.objPtr) }
            let retv:Bool = unsafe{ rApiSfccorClassReSetSurfaceSize(this.objPtr,width,height) }
            unsafe{ rApiDttClassUnlockDangerous(this.objPtr) }
            return retv
        }
        throw Exception(rgfErrorWinUncreated)
    }
    /**
    *  @brief 重置尺寸
    *  @param size        尺寸
    *  @return 是否成功
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    public open func reset (size:RgfSize):Bool{
        return reset(size.width,size.height)
    }
    /**
    *  @brief 调用渲染
    *  @param wRect     需要重绘的矩形 (留空表示全量重绘)
    *  @return 是否成功调用
    *  @note 调用成功并不代表渲染次数加一，为了确保渲染线程安全，当高并发渲染和窗口尺寸调整时，会抛弃跳过冲突的部分渲染，仅保留最近一次渲染请求等待渲染
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    @When[debug]
    public open func draw (wRect:Option<RgfRect>):Bool{
        if (isValid()) {
            let rect:RgfRect = wRect ?? getRect()
            if(rect.right - rect.left > 0 && rect.bottom - rect.top > 0 && super.beginDraw()){
				if (reCreate) {
					reCreate = false;
					/*这里进行自己内容的创建*/
					createDeviceResources_();
				}
				onPaint_(rect);//调用虚函数
				if (!super.endDrawInner()) {
					/*此处先释放自己创建的内容*/
					destroyDeviceResources_();
					unsafe { rApiSfccorClassCreateDeviceResources_(this.objPtr) }
					reCreate = true;
				}
				// 解共享渲染锁
				unsafe{ rApiDttClassUnlockRender(this.objPtr) }
                return true
			}
            return false
		}
        throw Exception(rgfErrorWinUncreated + rgfWarningNote + rgfWarnValidRdObj)
        return false // 完整性补齐
    }
    /**
    *  @brief 调用渲染
    *  @param wRect     需要重绘的矩形 (留空表示全量重绘)
    *  @return 是否成功调用
    *  @note 调用成功并不代表渲染次数加一，为了确保渲染线程安全，当高并发渲染和窗口尺寸调整时，会抛弃跳过冲突的部分渲染，仅保留最近一次渲染请求等待渲染
    */
    @When[!debug]
    public open func draw (wRect:Option<RgfRect>):Bool{
        if (isValid()) {
            let rect:RgfRect = wRect ?? getRect()
            if(rect.right - rect.left > 0 && rect.bottom - rect.top > 0 && super.beginDraw()){
				if (reCreate) {
					reCreate = false;
					/*这里进行自己内容的创建*/
					createDeviceResources_();
				}
				onPaint_(rect);//调用虚函数
				if (!super.endDrawInner()) {
					/*此处先释放自己创建的内容*/
					destroyDeviceResources_();
					unsafe { rApiSfccorClassCreateDeviceResources_(this.objPtr) }
					reCreate = true;
				}
				// 解共享渲染锁
				unsafe{ rApiDttClassUnlockRender(this.objPtr) }
                return true
			}
            return false
		}
        return false
    }
    /**
    *  @brief 混合渲染
    *  @param destX         目标左边
    *  @param destY         目标顶边
    *  @param destWidth     目标宽度
    *  @param destHeight    目标高度
    *  @param srcX          源左边
    *  @param srcY          源顶边
    *  @param srcWidth      源宽度
    *  @param srcHeight     源高度
    *  @param alpha         0.0~1.0 透明度
    *  @return 是否成功
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    @When[debug]
    public open func blend (destX:Int32 ,destY:Int32 ,destW:Int32 ,destH:Int32 ,srcX:Int32 ,srcY:Int32 ,srcW:Int32 ,srcH:Int32 , alpha!:Float32 ):Bool{
        if (isValid()) {
            return unsafe{ rApiSfccorClassBlendDraw(this.objPtr,destX,destY,destW,destH,srcX,srcY,srcW,srcH,alpha) }
        }
        throw Exception(rgfErrorWinUncreated + rgfWarningNote + rgfWarnValidRdObj)
        return false // 完整性补齐
    }
    /**
    *  @brief 混合渲染
    *  @param destX         目标左边
    *  @param destY         目标顶边
    *  @param destWidth     目标宽度
    *  @param destHeight    目标高度
    *  @param srcX          源左边
    *  @param srcY          源顶边
    *  @param srcWidth      源宽度
    *  @param srcHeight     源高度
    *  @param alpha         0.0~1.0 透明度
    *  @return 是否成功
    */
    @When[!debug]
    public open func blend (destX:Int32 ,destY:Int32 ,destW:Int32 ,destH:Int32 ,srcX:Int32 ,srcY:Int32 ,srcW:Int32 ,srcH:Int32 , alpha!:Float32 ):Bool{
        if (isValid()) {
            return unsafe{ rApiSfccorClassBlendDraw(this.objPtr,destX,destY,destW,destH,srcX,srcY,srcW,srcH,alpha) }
        }
        return false
    }
    /**
    *  @brief 混合渲染
    *  @param destRect  目标矩形
    *  @param srcRect   源矩形
    *  @param alpha     0.0~1.0 透明度
    *  @return 是否成功
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @note 本函数仅在调试模式下输出异常
    */
    public open func blend (destRect:RgfRect, srcRect:RgfRect, alpha!:Float32 ):Bool{
        return blend(
            destRect.left,destRect.top,destRect.right - destRect.left,destRect.bottom-destRect.top,
            srcRect.left,srcRect.top,srcRect.right - srcRect.left,srcRect.bottom-srcRect.top,
            alpha:alpha
        )
    }
    /**
    *  @brief 混合渲染
    *  @param destRect  目标浮点矩形
    *  @param srcRect   源浮点矩形
    *  @param alpha     0.0~1.0 透明度
    *  @return 是否成功
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @note 本函数仅在调试模式下输出异常
    */
    public open func blend (destRect:RgfFRect, srcRect:RgfFRect, alpha!:Float32 ):Bool{
        return blend(
            Int32(destRect.left),Int32(destRect.top),Int32(destRect.right - destRect.left),Int32(destRect.bottom-destRect.top),
            Int32(srcRect.left),Int32(srcRect.top),Int32(srcRect.right - srcRect.left),Int32(srcRect.bottom-srcRect.top),
            alpha:alpha
        )
    }

    // ---- 以下为内部事件

    // 创建设备/渲染相关资源
    private func createDeviceResources_():Bool{ 
        let preRet:(Bool,Bool) = callbackCreateDeviceResources?() ?? (true,true)
        var retv:Bool = true
        if(preRet[1]){
            retv = createDeviceResources()
        }else{
            retv = preRet[0]
        }
        return retv; 
    }
    // 绘制窗口
    private func onPaint_(wRect:RgfRect):Unit{
        if(callbackOnPaint?(wRect) ?? true){
            onPaint(wRect)
        }
    }
    // 销毁设备/渲染相关资源
    private func destroyDeviceResources_():Bool{ 
        let preRet:(Bool,Bool) = callbackDestroyDeviceResources?() ?? (true,true)
        var retv:Bool = true
        if(preRet[1]){
            retv = destroyDeviceResources()
        }else{
            retv = preRet[0]
        }
        return retv;
    }


    public prop rgfRenderer:RgfRendererCore{
        get() { 
            return this
        }
    }
    // ---- 以下为事件方法

    // 创建设备/渲染相关资源
    public open func createDeviceResources():Bool{ return true; }
    // 绘制窗口
    public open func onPaint(wRect:RgfRect):Unit{}
    // 销毁设备/渲染相关资源
    public open func destroyDeviceResources():Bool{ return true; }

    // ---- 以下为lambda事件回调注册

    /** @brief 监听式事件 - 创建设备相关/渲染相关资源 */
    public func listenCreateDeviceResources(callback:()->(Bool,Bool)){callbackCreateDeviceResources = callback}
    /** @brief 监听式事件 - 绘制 */
    public func listenOnPaint(callback:(wRect:RgfRect)->Bool){callbackOnPaint = callback}
    /** @brief 监听式事件 - 销毁设备相关/渲染相关资源 */
    public func listenDestroyDeviceResources(callback:()->(Bool,Bool)){callbackDestroyDeviceResources = callback}
}