/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package rgf.rgf_core

public interface IDrawCreater{
    /**
    *  @brief 创建纯色画刷
    *  @param r         0.0~1.0 红
    *  @param g         0.0~1.0 绿
    *  @param b         0.0~1.0 蓝
    *  @param a         0.0~1.0 透明度
    *  @param width     线宽
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createSolidColorBrush(r:Float32 , g:Float32, b:Float32, a:Float32, width:Float32):SolidColorBrush
    /**
    *  @brief 创建纯色画刷
    *  @param rgba      色彩值
    *  @param width     线宽
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createSolidColorBrush(color:RgfFColor, width:Float32):SolidColorBrush
    /**
    *  @brief 创建纯色画刷
    *  @param ref       对象引用
    *  @param r         0.0~1.0 红
    *  @param g         0.0~1.0 绿
    *  @param b         0.0~1.0 蓝
    *  @param a         0.0~1.0 透明度
    *  @param width     线宽
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createSolidColorBrush(ref:SolidColorBrush,r:Float32 , g:Float32, b:Float32, a:Float32, width:Float32):Unit
    /**
    *  @brief 创建纯色画刷
    *  @param ref       对象引用
    *  @param rgba      色彩值
    *  @param width     线宽
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createSolidColorBrush(ref:SolidColorBrush,color:RgfFColor, width:Float32):Unit
    
    
    
    /**
    *  @brief 创建线性渐变画刷
    *  @param startColor            开始颜色
    *  @param endColor              结束颜色
    *  @param startPoint            开始点
    *  @param endPoint              结束点
    *  @param midPctColor           中间点与颜色
    *  @param useGammaCorrection    是否启用伽马校正
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createLinearGradientBrush (
        startColor:RgfFColor, 
        endColor:RgfFColor, 
        startPoint:RgfFPoint, 
        endPoint:RgfFPoint,
        midPctColor!:Array<RgfPctColor>,
        useGammaCorrection!:Bool
    ):LinearGradientBrush
    /**
    *  @brief 创建线性渐变画刷
    *  @param ref                   对象引用
    *  @param startColor            开始颜色
    *  @param endColor              结束颜色
    *  @param startPoint            开始点
    *  @param endPoint              结束点
    *  @param midPctColor           中间点与颜色
    *  @param useGammaCorrection    是否启用伽马校正
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createLinearGradientBrush (
        ref:LinearGradientBrush,
        startColor:RgfFColor, 
        endColor:RgfFColor, 
        startPoint:RgfFPoint, 
        endPoint:RgfFPoint,
        midPctColor!:Array<RgfPctColor>,
        useGammaCorrection!:Bool
    ):Unit
    
    
    
    /**
    *  @brief 创建位图画刷
    *  @param img 位图
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createBitmapBrush (img:Image):BitmapBrush
    /**
    *  @brief 创建位图画刷
    *  @param ref 对象引用
    *  @param img 位图
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createBitmapBrush (ref:BitmapBrush,img:Image):Unit
    
    
    
    /**
    *  @brief 创建线段风格
    *  @param startCap      开始线条帽样式
    *  @param endCap        结束线条帽样式
    *  @param dashCap       虚线条帽样式
    *  @param lineJoin      线条联接样式
    *  @param miterLimit    斜接长度
    *  @param dashStyle     虚线样式
    *  @param dashOffset    虚线偏移量
    *  @param dashes        虚线段定义数组
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createStrokeStyle(
        startCap!:RgfCapStyle,
        endCap!:RgfCapStyle,
        dashCap!:RgfCapStyle,
        lineJoin!:RgfLineJoin,
        miterLimit!:Float32,
        dashStyle!:RgfDashStyle,
        dashOffset!:Float32,
        dashes!:Array<Float32>
    ):StrokeStyle
    /**
    *  @brief 创建线段风格
    *  @param ref           对象引用
    *  @param startCap      开始线条帽样式
    *  @param endCap        结束线条帽样式
    *  @param dashCap       虚线条帽样式
    *  @param lineJoin      线条联接样式
    *  @param miterLimit    斜接长度
    *  @param dashStyle     虚线样式
    *  @param dashOffset    虚线偏移量
    *  @param dashes        虚线段定义数组
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createStrokeStyle(
        ref:StrokeStyle,
        startCap!:RgfCapStyle,
        endCap!:RgfCapStyle,
        dashCap!:RgfCapStyle,
        lineJoin!:RgfLineJoin,
        miterLimit!:Float32,
        dashStyle!:RgfDashStyle,
        dashOffset!:Float32,
        dashes!:Array<Float32>
    ):Unit
    
    
    
    /**
    *  @brief 创建位图
    *  @param width  宽度
    *  @param height 高度
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createBitmap(width:Int32, height:Int32):Bitmap
    /**
    *  @brief 创建位图
    *  @param size  尺寸
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createBitmap(size:RgfSize):Bitmap
    /**
    *  @brief 创建位图
    *  @param ref    对象引用
    *  @param width  宽度
    *  @param height 高度
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createBitmap(ref:Bitmap,width:Int32, height:Int32):Unit
    /**
    *  @brief 创建位图
    *  @param size  尺寸
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createBitmap(ref:Bitmap,size:RgfSize):Unit
    
    
    
    /**
    *  @brief 创建位图(从文件)
    *  @param uri   路径字符串
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createBitmapFromFile(uri:String):Bitmap
    /**
    *  @brief 创建位图(从文件)
    *  @param ref   对象引用
    *  @param uri   路径字符串
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createBitmapFromFile(ref:Bitmap,uri:String):Unit
    
    
    
    /**
    *  @brief 创建位图(从内存位图)
    *  @param pMemBmp   内存位图
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createBitmapFromMemory (pMemBmp:MemBitmap):Bitmap
    /**
    *  @brief 创建位图(从内存位图)
    *  @param ref       对象引用
    *  @param pMemBmp   内存位图
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createBitmapFromMemory (ref:Bitmap,pMemBmp:MemBitmap):Unit
    
    
    
    /**
    *  @brief 创建路径
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createPath():Path
    /**
    *  @brief 创建路径
    *  @param ref       对象引用
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createPath(ref:Path):Unit
    
    
    
    /**
    *  @brief 创建图层
    *  @param width  宽度
    *  @param height 高度
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createLayer (width:Int32, height:Int32):Layer
    /**
    *  @brief 创建图层
    *  @param size  尺寸
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createLayer (size:RgfSize):Layer
    /**
    *  @brief 创建图层
    *  @param ref    对象引用
    *  @param width  宽度
    *  @param height 高度
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createLayer (ref:Layer,width:Int32, height:Int32):Unit
    /**
    *  @brief 创建图层
    *  @param ref   对象引用
    *  @param size  尺寸
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createLayer (ref:Layer,size:RgfSize):Unit
    
    
    
    /**
    *  @brief 重置图层尺寸
    *  @param layer         图层对象
    *  @param width         宽度
    *  @param height        高度
    *  @param copyBitmap    是否拷贝源位图内容
    *  @return 是否成功
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func resetLayerSize (layer:Layer,width:Int32, height:Int32, copyBitmap!:Bool):Bool
    /**
    *  @brief 重置图层尺寸
    *  @param layer         图层对象
    *  @param size          尺寸
    *  @param copyBitmap    是否拷贝源位图内容
    *  @return 是否成功
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func resetLayerSize (layer:Layer,size:RgfSize, copyBitmap!:Bool):Bool
    
    
    
    /**
    *  @brief 创建文本核心
    *  @param gamma             伽马值
    *  @param enhancedContrast  增强对比度 0.0~1.0
    *  @param clearTypeLevel    清晰度级别(字体平滑) 0.0~1.0
    *  @param renderingMode     渲染模式
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createTextCore(gamma!:Float32,enhancedContrast!:Float32,clearTypeLevel!:Float32,renderingMode!:RgfTxtRenderingMode):TextCore
    /**
    *  @brief 创建文本核心
    *  @param ref               对象引用
    *  @param gamma             伽马值
    *  @param enhancedContrast  增强对比度 0.0~1.0
    *  @param clearTypeLevel    清晰度级别(字体平滑) 0.0~1.0
    *  @param renderingMode     渲染模式
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createTextCore(ref:TextCore,gamma!:Float32,enhancedContrast!:Float32,clearTypeLevel!:Float32,renderingMode!:RgfTxtRenderingMode):Unit
    
    
    
    /**
    *  @brief 创建文本行内对象
    *  @param x         左边
    *  @param y         顶边
    *  @param width     宽度
    *  @param height    高度
    *  @param img       图像
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createTextInlineObject(x:Float32,y:Float32,width:Float32,height:Float32,img:Image):TextInlineObj
    /**
    *  @brief 创建文本行内对象
    *  @param rect  浮点矩形
    *  @param img   图像
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createTextInlineObject(rect:RgfFRect, img:Image):TextInlineObj
    /**
    *  @brief 创建文本行内对象
    *  @param rect  矩形
    *  @param img   图像
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createTextInlineObject(rect:RgfRect, img:Image):TextInlineObj
    /**
    *  @brief 创建文本行内对象
    *  @param ref       对象引用
    *  @param x         左边
    *  @param y         顶边
    *  @param width     宽度
    *  @param height    高度
    *  @param img       图像
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createTextInlineObject(ref:TextInlineObj,x:Float32,y:Float32,width:Float32,height:Float32,img:Image):Unit
    /**
    *  @brief 创建文本行内对象
    *  @param ref   对象引用
    *  @param rect  浮点矩形
    *  @param img   图像
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createTextInlineObject(ref:TextInlineObj,rect:RgfFRect, img:Image):Unit
    /**
    *  @brief 创建文本行内对象
    *  @param ref   对象引用
    *  @param rect  矩形
    *  @param img   图像
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createTextInlineObject(ref:TextInlineObj,rect:RgfRect, img:Image):Unit
    
    
    
    /**
    *  @brief 创建文本格式
    *  @param size          尺寸
    *  @param familyName    字体族
    *  @param locale        区域设置名称
    *  @param weight        字体粗细
    *  @param style         字体样式
    *  @param stretch       字体拉伸
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createTextFormat(size:Float32,familyName!:String,locale!:String,weight!:RgfTxtFontWeight,style!:RgfTxtFontStyle,stretch!:RgfTxtFontStretch):TextFormat;
    /**
    *  @brief 创建文本格式
    *  @param ref           对象引用
    *  @param size          尺寸
    *  @param familyName    字体族
    *  @param locale        区域设置名称
    *  @param weight        字体粗细
    *  @param style         字体样式
    *  @param stretch       字体拉伸
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createTextFormat(ref:TextFormat,size:Float32,familyName!:String,locale!:String,weight!:RgfTxtFontWeight,style!:RgfTxtFontStyle,stretch!:RgfTxtFontStretch):Unit;
    
    
    
    /**
    *  @brief 创建文本布局
    *  @param string    字符串内容
    *  @param format    文本格式
    *  @param maxWidth  最大宽度  
    *  @param maxHeight 最大高度
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createTextLayout(string:String,format:TextFormat,maxWidth:Float32,maxHeight:Float32):TextLayout;
    /**
    *  @brief 创建文本布局
    *  @param string    字符串内容
    *  @param format    文本格式
    *  @param maxSize   最大尺寸 
    *  @return 返回对象
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    func createTextLayout(string:String,format:TextFormat,maxSize:RgfSize):TextLayout;
    /**
    *  @brief 创建文本布局
    *  @param ref       对象引用
    *  @param string    字符串内容
    *  @param format    文本格式
    *  @param maxWidth  最大宽度  
    *  @param maxHeight 最大高度
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createTextLayout(ref:TextLayout,string:String,format:TextFormat,maxWidth:Float32,maxHeight:Float32):Unit;
    /**
    *  @brief 创建文本布局
    *  @param ref       对象引用
    *  @param string    字符串内容
    *  @param format    文本格式
    *  @param maxSize   最大尺寸 
    *  @exception 若请求时底层对象未创建，则抛出异常
    *  @attention 引用式创建(本方式创建会自动释放原对象，修改对象指向)
    */
    func createTextLayout(ref:TextLayout,string:String,format:TextFormat,maxSize:RgfSize):Unit;
}