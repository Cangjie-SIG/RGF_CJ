/**
 * Copyright 2025 RaoZiJun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */
package rgf.rgf_core

public open class Brush <: RgfSfcSubObjBase{
    public init(){
        super(CPointer<Unit>(),RgfSfcSubObjType.Brush)
    }
    internal init(oPtr:CPointer<Unit>){
        super(oPtr,RgfSfcSubObjType.Brush)
    }

    /**
    *  @brief 获取画刷线宽
    *  @return 线宽
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    public func getLineWidth ():Float32{
        if(!isValid()){ throw Exception(rgfErrorCpluapluaObjNotExist) }
        return unsafe{ rApiBrhClassGetLineWidth(objPtr) }
    }
    /**
    *  @brief 设置画刷线宽
    *  @param width 线宽
    *  @return 是否成功
    *  @exception 若请求时底层对象未创建，则抛出异常
    */
    public func setLineWidth (width:Float32):Bool{
        if(!isValid()){ throw Exception(rgfErrorCpluapluaObjNotExist) }
        return unsafe{ rApiBrhClassSetLineWidth(objPtr,width) }
    }
}