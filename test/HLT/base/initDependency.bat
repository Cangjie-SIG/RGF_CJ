@echo off
setlocal enabledelayedexpansion
echo ... initDependency.bat ...
REM 获取当前目录
set "current_dir=%~dp0"

cd /d "%current_dir%..\..\.."

REM 获取项目主目录
set "main_dir=%cd%"

:: 初始化目录
if not exist "%current_dir%target\release\unittest_bin" (
    mkdir "%current_dir%target\release\unittest_bin"
)

:: 复制文件到指定目录
xcopy "%main_dir%\res" "%current_dir%target\release\unittest_bin\res" /E /I
:: 拷贝DLL
echo Copy "%main_dir%\libs\libRgf.dll" to "%current_dir%target\release\unittest_bin"
copy "%main_dir%\libs\libRgf.dll" "%current_dir%target\release\unittest_bin" /Y

echo ... initDependency.bat ...
endlocal