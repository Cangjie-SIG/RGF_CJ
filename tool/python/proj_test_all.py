# -*- coding: utf-8 -*-
import sys
import os
# 默认全局配置项
def_glb_config = {}
##
## 此脚本用于全量执行所有单元测试，并且合并测试结果
## 最终测试结果会以报告形式输出到文件中
##
# --------------------- 配置 ---------------------

CONST_PROJECT_NAME = "RGF"

# ------------------------------------------------

# --------------------- 参数 ---------------------

# 启动参数 -lang
# 表示脚本的输出语言: en、zh-cn
def_glb_config['lang'] = "en"

# 启动参数 -jp
# 传递(true/false,t/f,y/n,1/0)
# 留空则等待用户输入
# 表示若主库已经编译，是否重新编译
def_glb_config['jp'] = ""

# 启动参数 --nsty 输出不携带风格
def_glb_config['nsty'] = False

# ------------------------------------------------

import utils.pathUtil as projpath
import utils.cmdUtil as projcmd
import utils.consoleUtil as projcon
import utils.cjpmUtil as projcjpm
import utils.logUtil as projlog
import utils.dateUtil as projdate

##
##
## 初始化
##
##

# 获取全局参数
def_glb_config.update(projcmd.get_args(sys.argv))

# 更新输出风格
projcon.can_use_con_style = not def_glb_config['nsty']

# 初始化日志对象
log = projlog.log(CONST_PROJECT_NAME)
# 清空控制台
projcon.clear()

# 获取脚本所在目录
script_directory = os.path.dirname(os.path.abspath(__file__))
# 获取项目主目录 # 获取两次上级目录
project_directory = projpath.get_project_directory(script_directory)


# 读取语言配置文件
if def_glb_config['lang'] != "en":
    # 使用非默认语言配置，则设置控制台为 utf-8编码
    sys.stdout.reconfigure(encoding='utf-8')
lang_file_path = script_directory + f"\\lang\\{def_glb_config['lang']}.lang"
if not projpath.file_exists(lang_file_path):
    def_glb_config['lang'] = "en"
    lang_file_path = script_directory + f"\\lang\\{def_glb_config['lang']}.lang"
# 获取语言数据列表
lang_data = projpath.file_read_lines(lang_file_path)
if len(lang_data) < 20:
    log.output(f"Incomplete data in language configuration file \"{def_glb_config['lang']}\"",level = projlog.Level.ERROR)
    # 语言文件不完整
    sys.exit(1) # 0 成功; 非0 失败

##
##
##  编译主体
##
##

# 编译项目
def build_main_proj():
    log.output(lang_data[4] + " {")
    retv = projcjpm.cjpm_build_proj_print(project_directory)
    log.output("}")
    return retv
# 编译标志
proj_core_compiled_flag = False

# 判断 项目依赖主体是否已经编译
dir_proj_cjo = project_directory + "\\" + projpath.DIR_TARGET + "\\" + projpath.DIR_RELEASE
if len(projpath.get_files(dir_proj_cjo,projpath.FILE_CJO,True)) != 0 :
    input_is_true = False
    if def_glb_config['jp'] == "":
        # 询问是否需要重新编译项目
        log.output(f"{lang_data[5].replace('*',f'{CONST_PROJECT_NAME}')}\n" + projcmd.INPUT_TIP,level = projlog.Level.ASK,end = "")
        if projcon.can_use_con_style:
            print("F",end="")
            projcon.move_left(1)
        input_is_true = projcmd.input_true()
    else:
        input_is_true = projcmd.is_true(def_glb_config['jp'])
    # 根据选项决定操作
    if input_is_true:
        # 开始编译
        proj_core_compiled_flag = build_main_proj()
    else:
        log.output(lang_data[6].replace('*',f'{CONST_PROJECT_NAME}'))
        proj_core_compiled_flag = True
else:
    # 开始编译项目
    proj_core_compiled_flag = build_main_proj()

# 如果核心部分编译失败，则跳出
if not proj_core_compiled_flag:
    log.output(lang_data[7],level = projlog.Level.ERROR)
    # 编译错误，退出
    sys.exit(1) # 0 成功; 非0 失败
else:
    log.output(f"{lang_data[8].replace('*',f'{CONST_PROJECT_NAME}')}\n",level = projlog.Level.SUCCESS)


##
##
##  测试相关
##
##

# 测试项目
def test_target_proj(path):
    log.output(lang_data[9] + " {")
    projpath.execute_bat_if_exists(f"{path}\\initDependency.bat")
    result = projcjpm.cjpm_test_proj(path)
    projpath.execute_bat_if_exists(f"{path}\\clearDependency.bat")
    if result["returncode"] == 0:
        log.output(result["stdout"],head=False)
        retv = True
    else:
        log.output(result["stderr"],head=False)
        retv = False
    log.output("}")
    return retv

# 检索所有测试用例
log.output(lang_data[10] + " {")
dir_test = project_directory + "\\" + projpath.DIR_TEST
i = 0
test_path_arr = projpath.get_files(dir_test,projpath.FILE_TOML,True)
for toml_path in test_path_arr:
    i += 1
    log.output(f"    [{i}]: {toml_path}",level = projlog.Level.BASE)
log.output("}")

if i==0:
    log.output(lang_data[11],level = projlog.Level.SUCCESS)
    # 没有测试用例
    sys.exit(0) # 0 成功; 非0 失败
else:
    # 开始启用报告获取
    log.use_report(True)
    for path in test_path_arr:
        # 开始测试
        dir_test_path = projpath.get_parent(path)
        log.output( "////////////////////////////////////////////////////////////////////////////////////")
        log.output( "////////////////////////////////////////////////////////////////////////////////////")
        log.output(f"//>>  {lang_data[14]}\"{dir_test_path}\"")
        log.output( "////////////////////////////////////////////////////////////////////////////////////")
        log.output( "////////////////////////////////////////////////////////////////////////////////////")
        # 测试编译标志
        proj_test_compiled_flag = False
        # 开始测试
        proj_test_compiled_flag = test_target_proj(dir_test_path)
        # 如果核心部分编译失败，则跳出
        if not proj_test_compiled_flag:
            log.output(lang_data[15],level = projlog.Level.ERROR)
        else:
            log.output(f"{lang_data[16]}\n",level = projlog.Level.SUCCESS)

    # 准备报告输出目录
    dir_report = script_directory + "\\" + projpath.DIR_REPORT
    if not projpath.dir_exists(dir_report):
        projpath.dir_mk(dir_report)
    # 获取全量报告数据，并且输出到文件中
    projpath.file_write_all(log.get_report(),dir_report + f"\\{projdate.get_now_YYYY_MM_DD_HH_mm_ss()}.txt")



# 程序成功退出
sys.exit(0)