from enum import Enum
from . import consoleUtil as projcon
from . import dateUtil as projdate


class Level(Enum):
    BASE        = 0
    DEBUG       = 1
    INFO        = 2
    WARNING     = 3
    ERROR       = 4
    CRITICAL    = 5
    SUCCESS     = 6
    ASK         = 7

class log:
    mod_name = ""
    report_content = ""
    report_use = False
    def __init__(self, name):
        self.mod_name = name
    def use_report(self,use = True):
        self.report_use = use
        if(self.report_use):
            self.report_content = ""
    def get_report(self):
        return self.report_content

    def output_get(self, content,level = Level.INFO,head = True):
        tmp = ""
        if head:
            tmp += f"[{self.mod_name}] <{projdate.get_now_YYYYMMDDHHmmss()}> : "
        
        if level == Level.DEBUG:
            # 基础高亮 + 反显
            return tmp + projcon.make(projcon.LIGHT_HIGHT + projcon.INVERTING,content)
        
        elif level == Level.INFO:
            # 蓝色
            return tmp + projcon.make(projcon.CLR_BLUE ,content)
        
        elif level == Level.WARNING:
            # 黄色
            return tmp + projcon.make(projcon.CLR_YELLOW ,content)
        
        elif level == Level.ERROR:
            # 红色
            return tmp + projcon.make(projcon.CLR_RED ,content)
        
        elif level == Level.CRITICAL:
            # 红色 + 闪烁 + 下划线
            return tmp + projcon.make(projcon.CLR_RED + projcon.FLICKER + projcon.UNDERLINE,content)

        elif level == Level.SUCCESS:
            # 绿色 + 闪烁 + 下划线
            return tmp + projcon.make(projcon.CLR_GREEN + projcon.FLICKER + projcon.UNDERLINE,content)

        elif level == Level.ASK:
            # 紫色 + 下划线
            return tmp + projcon.make(projcon.CLR_PURPLE + projcon.UNDERLINE,content)

        elif level == Level.BASE:
            # 基础
            return tmp + content

    def output_get_tip(self, content,level = Level.INFO,head = True):
        tmp = ""
        if head:
            tmp += f"[{self.mod_name}] <{projdate.get_now_YYYYMMDDHHmmss()}> "
            if level == Level.DEBUG:
                tmp += "(DEBUG   ): "
            elif level == Level.INFO:
                tmp += "(INFO    ): "
            elif level == Level.WARNING:
                tmp += "(WARNING ): "
            elif level == Level.ERROR:
                tmp += "(ERROR   ): "
            elif level == Level.CRITICAL:
                tmp += "(CRITICAL): "
            elif level == Level.SUCCESS:
                tmp += "(SUCCESS ): "
            elif level == Level.ASK:
                tmp += "(ASK     ): "
            elif level == Level.BASE:
                tmp += "(BASE    ): "
        return tmp + content
    def output(self, content,level = Level.INFO,head = True, end = '\n'):
        print(self.output_get(content,level = level,head = head),end = end)
        if self.report_use:
            self.report_content += self.output_get_tip(content,level = level,head = head) + end
    