import os
import glob
import shutil
import subprocess

DIR_BUILD = "build"
DIR_REPORT = "report"
DIR_LIBS = "libs"
DIR_TARGET = "target"
DIR_RELEASE = "release"
DIR_SAMPLES = "samples"
DIR_TEST = "test"
FILE_EXE = "*.exe"
FILE_DLL = "*.dll"
FILE_TOML = "*.toml"
FILE_CJO = "*.cjo"


"""
    @brief 基于运行脚本位置，获取项目主目录
    @param path 执行脚本路径
    @note 本方法限定执行脚本在  项目主目录/tool/python下，或 项目主目录/*/*下
""" 
def get_project_directory(path):
    # 获取项目主目录 # 获取两次上级目录
    return os.path.dirname(os.path.dirname(path))

"""
    @brief 获取路径父级
    @param path 文件路径或目录路径
"""
def get_parent(path):
    return os.path.dirname(path)

"""
    @brief 目录是否存在
    @param path 目录路径
""" 
def dir_exists(path):
    # 判断目录是否存在，并且是目录而不是文件
    if os.path.exists(path) and os.path.isdir(path):
        return True
    else:
        return False
"""
    @brief 创建目录
"""
def dir_mk(path):
    os.mkdir(path)

"""
    @brief 获取目录下所有指定类型文件
    @param directory_path 目录路径
    @param filter 过滤文件 例如：*.exe
    @param recursion 是否递归
""" 
def get_files(directory_path,filter = '*.exe',recursion = False):
    # 使用递归搜索所有 .exe 文件
    pattern = os.path.join(directory_path, '**' if recursion else '', filter)
    exe_files = glob.glob(pattern, recursive=True)
    return exe_files

"""
    @brief 文件是否存在
    @param file_path 文件路径
""" 
def file_exists(file_path):
    if os.path.exists(file_path) and os.path.isfile(file_path):
        return True
    else:
        return False

"""
    @brief 删除文件
    @param file_path 文件路径
""" 
def file_delete(file_path):
    if file_exists(file_path):
        os.remove(file_path)

"""
    @brief 获取文件名
    @param path 路径
""" 
def file_name(path):
    if file_exists(path):
        return os.path.basename(path)
    else:
        return ""

"""
    @brief 文件复制
    @param src 源文件路径
    @param dest_dir 目标目录
""" 
def file_copy(src,dest_dir):
    if file_exists(src) and dir_exists(dest_dir):
        shutil.copy(src,dest_dir + "\\" +os.path.basename(src))

"""
    @brief 读取文件行列表
    @return 文件行列表
""" 
def file_read_lines(path):
    lst = []
    if file_exists(path):
        # 打开文件并逐行读取
        with open(path, 'r', encoding='utf-8') as file:
            for line in file:
                # 使用 strip() 去除每行末尾的换行符
                lst.append(line.strip())
    return lst

"""
    @brief 读取文件所有数据
    @return 文件字符串
""" 
def file_read_all(path):
    if file_exists(path):
        # 打开文件并逐行读取
        with open(path, 'r', encoding='utf-8') as file:
            return file.read()
    return ""

"""
    @brief 写数据到文件
    @param str 字符串
    @param path 文件路径
"""
def file_write_all(str,path):
    with open(path, 'w',encoding='utf-8') as file:
        file.write(str)

"""
    @brief 判断是否存在bat，若存在则执行
    @param str 字符串
    @param path 文件路径
"""
def execute_bat_if_exists(path):
    # 检查文件是否存在
    if os.path.isfile(path):
        # 使用 subprocess.Popen 来执行 .bat 文件
        # shell=True 表示通过系统 shell 执行命令
        process = subprocess.Popen(path, shell=True)
        
        # 等待进程结束并获取返回码
        return process.wait()