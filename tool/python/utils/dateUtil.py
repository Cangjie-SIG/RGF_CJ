from datetime import datetime


"""
    @brief 获取当前 YYYY/MM/DD HH:mm:ss 格式时间
    @return 时间字符串
""" 
def get_now_YYYYMMDDHHmmss():
    # 获取当前时间
    now = datetime.now()
    # 格式化时间为 YYYY/MM/DD HH:mm:ss
    return now.strftime("%Y/%m/%d %H:%M:%S")

"""
    @brief 获取当前 YYYY_MM_DD_HH_mm_ss 格式时间
    @return 时间字符串
""" 
def get_now_YYYY_MM_DD_HH_mm_ss():
    # 获取当前时间
    now = datetime.now()
    # 格式化时间为 YYYY/MM/DD HH:mm:ss
    return now.strftime("%Y_%m_%d_%H_%M_%S")