
head_value = "\x1b"

can_use_con_style = True

def make(param,str):
    if can_use_con_style:
        return f"{head_value}[{param[:-1]}m" + str + f"{head_value}[0m"
    else:
        return str
# 亮度配置
LIGHT_HIGHT =       "1;"
LIGHT_LOW =         "2;"
# 下划线
UNDERLINE =         "4;"
# 闪烁
FLICKER =           "5;"
# 反显
INVERTING =         "7;"
# 消隐
OCCLUSION =         "8;"
# 文字色彩配置
CLR_BLACK =         "30;"
CLR_RED =           "31;"
CLR_GREEN =         "32;"
CLR_YELLOW =        "33;"
CLR_BLUE =          "34;"
CLR_PURPLE =        "35;"
CLR_LIGHT_BLUE =    "36;"
CLR_WHITE =         "37;"
# 背景色
CLRB_BLACK =        "40;"
CLRB_RED =          "41;"
CLRB_GREEN =        "42;"
CLRB_YELLOW =       "43;"
CLRB_BLUE =         "44;"
CLRB_PURPLE =       "45;"
CLRB_LIGHT_BLUE =   "46;"
CLRB_WHITE =        "47;"
#光标上移n行
def move_up(num):
    if can_use_con_style:
        print(f"{head_value}[{num}A",end = '')
#光标下移n行
def move_down(num):
    if can_use_con_style:
        print(f"{head_value}[{num}B",end = '')
#光标右移n列
def move_right(num):
    if can_use_con_style:
        print(f"{head_value}[{num}C",end = '')
#光标左移n列
def move_left(num):
    if can_use_con_style:
        print(f"{head_value}[{num}D",end = '')
#设置光标位置（y行,x列）
def move_pos(row,col):
    if can_use_con_style:
        print(f"{head_value}[{row};{col}H",end = '')
#清屏
def clear():
    if can_use_con_style:
        print(f"{head_value}[2J",end = '')
#清除从光标到行尾的内容
def clear_to_line_end():
    if can_use_con_style:
        print(f"{head_value}[K",end = '')
#保存光标位置
def cursor_store():
    if can_use_con_style:
        print(f"{head_value}[s",end = '')
#恢复光标位置
def cursor_restore():
    if can_use_con_style:
        print(f"{head_value}[u",end = '')
#隐藏光标
def cursor_hide():
    if can_use_con_style:
        print(f"{head_value}[?25l",end = '')
#显示光标
def cursor_show():
    if can_use_con_style:
        print(f"{head_value}[?25h",end = '')