# -*- coding: utf-8 -*-
import sys
import os
# 默认全局配置项
def_glb_config = {}
##
## 此脚本用于清理、编译、测试、拷贝全链路执行
## 脚本会先清理build目录；
## 然后确保全局依赖编译完成；
## 此后根据选择的测试项目进行测试；
## 最后将编译结果文件拷贝入build目录
##
# --------------------- 配置 ---------------------

CONST_PROJECT_NAME = "RGF"

# ------------------------------------------------

# --------------------- 参数 ---------------------

# 启动参数 -lang
# 表示脚本的输出语言: en、zh-cn
def_glb_config['lang'] = "en"

# 启动参数 -jp
# 传递(true/false,t/f,y/n,1/0)
# 留空则等待用户输入
# 表示若主库已经编译，是否重新编译
def_glb_config['jp'] = ""

# 启动参数 -sel
# 传递数值
# 表示选中的测试用例序号，
# 若值不符合范围，则等待用户输入
# 留空也等待用户输入
def_glb_config['sel'] = 0

# 启动参数 --help 输出帮助页
def_glb_config['help'] = False

# 启动参数 --nsty 输出不携带风格
def_glb_config['nsty'] = False

# 启动参数 --sp 处理 sample 案例
def_glb_config['sp'] = False
# ------------------------------------------------

import utils.pathUtil as projpath
import utils.cmdUtil as projcmd
import utils.consoleUtil as projcon
import utils.cjpmUtil as projcjpm
import utils.logUtil as projlog
import utils.strUtil as projstr

##
##
## 初始化
##
##

# 获取全局参数
def_glb_config.update(projcmd.get_args(sys.argv))

# 更新输出风格
projcon.can_use_con_style = not def_glb_config['nsty']

# 初始化日志对象
log = projlog.log(CONST_PROJECT_NAME)
# 清空控制台
projcon.clear()

# 获取脚本所在目录
script_directory = os.path.dirname(os.path.abspath(__file__))
# 获取项目主目录 # 获取两次上级目录
project_directory = projpath.get_project_directory(script_directory)


# 读取语言配置文件
if def_glb_config['lang'] != "en":
    # 使用非默认语言配置，则设置控制台为 utf-8编码
    sys.stdout.reconfigure(encoding='utf-8')
lang_file_path = script_directory + f"\\lang\\{def_glb_config['lang']}.lang"
if not projpath.file_exists(lang_file_path):
    def_glb_config['lang'] = "en"
    lang_file_path = script_directory + f"\\lang\\{def_glb_config['lang']}.lang"
# 获取语言数据列表
lang_data = projpath.file_read_lines(lang_file_path)
if len(lang_data) < 20:
    log.output(f"Incomplete data in language configuration file \"{def_glb_config['lang']}\"",level = projlog.Level.ERROR)
    # 语言文件不完整
    sys.exit(1) # 0 成功; 非0 失败

# 若启用 help 则输出帮助文档
if def_glb_config['help']:
    print(projpath.file_read_all(script_directory + f"\\lang\\{def_glb_config['lang']}_help.lang"))
    print("\n\n")
    sys.exit(0) # 0 成功; 非0 失败


##
##
##  清理
##
##

# 判断当前编译目录是否存在
dir_build = project_directory + "\\" + projpath.DIR_BUILD
if(projpath.dir_exists(dir_build)):
    # 开始清理构建目录
    log.output(lang_data[0] + " {")
    for exe_path in projpath.get_files(dir_build,projpath.FILE_EXE,True):
        projpath.file_delete(exe_path)
        log.output(f"    {lang_data[1]}: {exe_path}",level = projlog.Level.BASE)
    for dll_path in projpath.get_files(dir_build,projpath.FILE_DLL,True):
        projpath.file_delete(dll_path)
        log.output(f"    {lang_data[2]}: {dll_path}",level = projlog.Level.BASE)
    log.output("}")
else:
    projpath.dir_mk(dir_build)
# 清理完成
log.output(f"{lang_data[3]}\n",level = projlog.Level.SUCCESS)


##
##
##  编译主体
##
##


# 编译项目
def build_main_proj():
    log.output(lang_data[4] + " {")
    retv = projcjpm.cjpm_build_proj_print(project_directory)
    log.output("}")
    return retv
# 编译标志
proj_core_compiled_flag = False

# 判断 项目依赖主体是否已经编译
dir_proj_cjo = project_directory + "\\" + projpath.DIR_TARGET + "\\" + projpath.DIR_RELEASE
if len(projpath.get_files(dir_proj_cjo,projpath.FILE_CJO,True)) != 0 :
    input_is_true = False
    if def_glb_config['jp'] == "":
        # 询问是否需要重新编译项目
        log.output(f"{lang_data[5].replace('*',f'{CONST_PROJECT_NAME}')}\n" + projcmd.INPUT_TIP,level = projlog.Level.ASK,end = "")
        if projcon.can_use_con_style:
            print("F",end="")
            projcon.move_left(1)
        input_is_true = projcmd.input_true()
    else:
        input_is_true = projcmd.is_true(def_glb_config['jp'])
    # 根据选项决定操作
    if input_is_true:
        # 开始编译
        proj_core_compiled_flag = build_main_proj()
    else:
        log.output(lang_data[6].replace('*',f'{CONST_PROJECT_NAME}'))
        proj_core_compiled_flag = True
else:
    # 开始编译项目
    proj_core_compiled_flag = build_main_proj()

# 如果核心部分编译失败，则跳出
if not proj_core_compiled_flag:
    log.output(lang_data[7],level = projlog.Level.ERROR)
    # 编译错误，退出
    sys.exit(1) # 0 成功; 非0 失败
else:
    log.output(f"{lang_data[8].replace('*',f'{CONST_PROJECT_NAME}')}\n",level = projlog.Level.SUCCESS)


##
##
##  测试相关
##
##

# 编译项目
def test_target_proj(path):
    log.output(lang_data[9] + " {")
    projpath.execute_bat_if_exists(f"{path}\\initDependency.bat")
    retv = projcjpm.cjpm_test_proj_print(path)
    projpath.execute_bat_if_exists(f"{path}\\clearDependency.bat")
    log.output("}")
    return retv
# 编译项目
def sample_target_proj(path):
    log.output(lang_data[9] + " {")
    projpath.execute_bat_if_exists(f"{path}\\initDependency.bat")
    retv = projcjpm.cjpm_build_proj_execute_wait(path)
    projpath.execute_bat_if_exists(f"{path}\\clearDependency.bat")
    log.output("}")
    return retv
# 选中项标志
proj_test_select_flag = 0
# 检索所有测试用例
log.output(lang_data[10] + " {")
if def_glb_config['sp']:
    dir_test = project_directory + "\\" + projpath.DIR_SAMPLES
else:
    dir_test = project_directory + "\\" + projpath.DIR_TEST
i = 0
test_path_arr = projpath.get_files(dir_test,projpath.FILE_TOML,True)
for toml_path in test_path_arr:
    i += 1
    log.output(f"    [{i}]: {toml_path}",level = projlog.Level.BASE)
log.output("}")

if i==0:
    log.output(lang_data[11],level = projlog.Level.SUCCESS)
    # 没有测试用例
    sys.exit(0) # 0 成功; 非0 失败
else:
    sel = def_glb_config['sel']
    if projstr.is_integer(sel) and int(sel) > 0 and int(sel) <= len(test_path_arr) :
        proj_test_select_flag = int(sel) - 1
    else:
        while True:
            # 选择要编译的项目
            log.output(lang_data[12],level = projlog.Level.ASK,end = "")
            if projcon.can_use_con_style:
                print("1",end="")
                projcon.move_left(1)
            sel = projcmd.input_val()
            sel = sel if sel != "" else "1"
            if projstr.is_integer(sel) and int(sel) > 0 and int(sel) <= len(test_path_arr) :
                proj_test_select_flag = int(sel) - 1
                break
            else:
                log.output(lang_data[13],level = projlog.Level.WARNING)

# 开始测试
dir_test_path = projpath.get_parent(test_path_arr[proj_test_select_flag])
log.output(f"{lang_data[14]}\"{dir_test_path}\"")
# 测试编译标志
proj_test_compiled_flag = False
# 开始测试
if def_glb_config['sp']:
    proj_test_compiled_flag = sample_target_proj(dir_test_path)
else:
    proj_test_compiled_flag = test_target_proj(dir_test_path)
# 如果核心部分编译失败，则跳出
if not proj_test_compiled_flag:
    log.output(lang_data[15],level = projlog.Level.ERROR)
    # 编译错误，退出
    sys.exit(1) # 0 成功; 非0 失败
else:
    log.output(f"{lang_data[16]}\n",level = projlog.Level.SUCCESS)

##
##
## 将所有编译结果移入 build 目录
##
##


log.output(lang_data[17].replace('*',"build") + " {")
# 拷贝编译结果到build
for exe_path_src in projpath.get_files(dir_test_path + "\\" + projpath.DIR_TARGET + "\\" + projpath.DIR_RELEASE,projpath.FILE_EXE,True):
    # 不拷贝以 std.开头的文件
    if not projpath.file_name(exe_path_src).startswith(("std.")):
        projpath.file_copy(exe_path_src,dir_build)
        log.output(f"    {lang_data[18]}{exe_path_src}",level = projlog.Level.BASE)

# 拷贝库到build
dir_libs = project_directory + "\\" + projpath.DIR_LIBS
if projpath.dir_exists(dir_libs):
    # 若库目录存在，就拷贝所有dll到指定目录
    for lib_path_src in projpath.get_files(dir_libs,projpath.FILE_DLL,True):
        projpath.file_copy(lib_path_src,dir_build)
        log.output(f"    {lang_data[19]}{lib_path_src}",level = projlog.Level.BASE)

log.output("}")
log.output(f"{lang_data[20]}\n",level = projlog.Level.SUCCESS)
# 程序成功退出
sys.exit(0)