import os  

#------------------------ 配置项 ------------------------{
file_name = "statistical_results"
# 生成注解后缀
use_note = True
# 总长
note_and_path_max_len = 50
# 生成尾注
generate_tail = True
# 注解后缀补齐内容
CHAR_SPACE = "-"
# 注解后缀最终符
CHAR_NOTE = "#"
#------------------------ 配置项 ------------------------}

# 获取当前脚本的绝对路径
current_path = os.path.abspath(__file__)
# 获取当前脚本所在的目录
script_directory = os.path.dirname(current_path)


def ergodicStack(stack,br,nbsp):
    output = []

    tplist = []

    max_len = len(stack)
    for i in range(max_len):
        now_level = stack[i][0]

        # 更新当前目录后续是否还有同级或父级目录
        if len(tplist) < now_level + 1:
            tplist.append(0)
        elif len(tplist) > now_level + 1:
            tplist = tplist[:now_level + 1]

        tplist[now_level] = 0
        asc = True#递增
        for j in range(i+1,max_len):
            if not (asc and stack[j][0] >= now_level):
                asc = False
            if asc and now_level >= stack[j][0]:
                tplist[now_level] = 1
                break
        
        #print(tplist,now_level)
        indent = ''
        for j in range(len(tplist)):
            if(j > 0):
                if tplist[j] == 1 and j == len(tplist) - 1:
                    if stack[i][2]:
                        indent += ' ├┈'
                    else:
                        indent += ' ├─'
                elif tplist[j] == 1:
                    indent +=  ' │ '
                elif tplist[j] == 0 and j == len(tplist) - 1:
                    if stack[i][2]:
                        indent += ' └┈'
                    else:
                        indent += ' └─'
                else:
                    indent += ' ' * 3
        
        addtxt = indent + stack[i][1]
        if br :addtxt += "<br>"
        if nbsp :addtxt = addtxt.replace(" ","&nbsp;")
        #print(addtxt)
        output.append(addtxt)
        #tplist.append(stack[i])
    #print(stack[0][0])
    return output


# 本算法自动过滤以 . 为开头的目录
def traverse_dir_with_os(root_dir,only_dir = False,br = False,nbsp = False):  
    filter_level = -1

    stack = []

    for root, dirs, files in os.walk(root_dir):  
        level = root.replace(root_dir, '').count(os.sep)  
        if not (filter_level != -1 and level > filter_level) :
            filter_level = -1

            dir_name = os.path.basename(root)
            if dir_name != "":
                if(dir_name[0] == "."):
                    filter_level = level
                else:
                    #print('{}{}/'.format(indent, os.path.basename(root)))  
                    stack.append((level,f'{os.path.basename(root)}/',False))
                    if not only_dir:
                        for f in files:  
                            stack.append((level + 1,f'{f}',True))
                            #print('{}{}'.format(subindent, f))  
    #print(stack)
    return ergodicStack(stack,br,nbsp)


dir_count = 0
file_count = 0

root_directory = script_directory + "\\..\\..\\src\\"

# 生成目录
out_str = "## 目录 \n"
list_data = traverse_dir_with_os(root_directory,True,False,False)
out_str += "<pre>\n"
for e in list_data:
    out_str += e
    if use_note and len(e) < note_and_path_max_len - 3:
        out_str += " " + (note_and_path_max_len - 3 - len(e)) * CHAR_SPACE  + CHAR_NOTE

    out_str += "\n"
out_str += "</pre>\n"

dir_count = len(list_data)

# 生成目录及文件
out_str += "## 文件 \n"
list_data = traverse_dir_with_os(root_directory,False,False,False)
out_str += "<pre>\n"
for e in list_data:
    out_str += e
    if use_note and len(e) < note_and_path_max_len - 3:
        out_str += " " + (note_and_path_max_len - 3 - len(e)) * CHAR_SPACE  + CHAR_NOTE

    out_str += "\n"
out_str += "</pre>\n"

file_count = len(list_data) - dir_count
if generate_tail:
    out_str += f"""
<br/>
<br/>
<br/>

----
*@Directory Count:{dir_count}<br/>@File Count:{file_count}*"""

with open(f'{script_directory}\{file_name}.md', "w",encoding="UTF-8") as file:
    file.write(out_str)
print(out_str)