@echo off
setlocal enabledelayedexpansion

cd /d "%cd%\..\.."

REM 获取当前目录
set "current_dir=%cd%"

:: 设置目标目录路径
set "testDir=%current_dir%\test"

if not exist "%current_dir%\build\" (
    REM 构建编译结果目录
    mkdir "%current_dir%\build"
) else (
    echo --------------------------------- Cleaning ---------------------------------{
    :: 清除本层中的Exe
    if exist "%current_dir%\build\*.exe" (
        del /F /Q "%current_dir%\build\*.exe"
        echo Cleaning EXE.
    )
    :: 清除本层中的Dll
    if exist "%current_dir%\build\*.dll" (
        del /F /Q "%current_dir%\build\*.dll"
        echo Cleaning DLL.
    )
    echo ----------------------------------------------------------------------------}
    echo Cleaning completed.
    echo.
)


REM 判断 RGF 是否已经编译
if exist "%current_dir%\target\release\rgf\*.cjo" (
    echo RGF has been compiled.

    :: 提示用户输入逻辑值
    :prompt
    set "choiceRC="
    set /p choiceRC="Do you want to recompile? (Y/N):" 

    :: 检查用户输入
    if /i "!choiceRC!"=="Y" (
        goto recompile
    ) else if /i "!choiceRC!"=="N" (
        echo Skip compilation RGF.
    ) else (
        echo Invalid value.
        goto prompt
    )
) else (
    :recompile
    echo ----------------------------- Compiling RGF -------------------------------{
    cjpm update
    cjpm build
    echo ----------------------------------------------------------------------------}
    echo RGF compilation completed.
    echo.
)

:: 检查test是否存在
if not exist "%testDir%" (
    echo The directory does not exist: %testDir%
    pause
    exit /b
)

:: 初始化变量
set count=0

:: 递归查找包含 cjpm.toml 文件的子目录
echo Searching for project path...
echo ----------------------------- Test List ------------------------------------{
for /r "%testDir%" %%D in (.) do (
    if exist "%%D\cjpm.toml" (
        set /a count+=1
        set "dir!count!=%%~fD"
        echo !count!: %%~fD
    )
)
echo ----------------------------------------------------------------------------}
echo.

:: 检查是否找到了任何符合条件的子目录
if %count% equ 0 (
    echo Project path not found.
    pause
    exit /b
)

:: 提供选择
set /p choice="Please select (Num):"

:: 验证用户输入
if %choice% gtr %count% (
    echo Invalid selection option.
    pause
    exit /b
)
if %choice% lss 1 (
    echo Invalid selection option.
    pause
    exit /b
)

:: 显示用户选择的子目录
set "selectedDir=!dir%choice%!"
echo [Selected: %selectedDir%]
echo.
:: 打开所选文件夹
cd /d "!dir%choice%!"

:: 编译测试
echo ----------------------------- Compiling Test ------------------------------{
    cjpm update
    cjpm build
echo ---------------------------------------------------------------------------}
echo [!dir%choice%!] test compilation completed.
echo.
echo ------------------------------- Copy Result -------------------------------{
:: 拷贝Exe
echo "!dir%choice%!\target\release\bin"
cd /d "!dir%choice%!\target\release\bin"
for /r %%i in (*.exe) do (
    echo Copy %%i to "%current_dir%\build"
    copy "%%i" "%current_dir%\build" /Y
)
:: 检查DLL
if not exist "%current_dir%\libs\libRgf.dll" (
    echo 'libRgf.dll' not found, Skip.
) else (
    :: 拷贝DLL
    echo Copy libRgf.dll to "%current_dir%\build"
    copy "%current_dir%\libs\libRgf.dll" "%current_dir%\build" /Y
)
echo ---------------------------------------------------------------------------}
echo.

:: 是否执行 main.exe
set /p runExe="Execute main.exe (Y/N):"
echo.
:: 检查用户输入
if /i "!runExe!"=="Y" (
    echo ---------------------------------- Main ----------------------------------{
    cd /d "%current_dir%\build"
    main.exe
    echo ---------------------------------------------------------------------------}
) else if /i "!runExe!"=="y" (
    echo ---------------------------------- Main ----------------------------------{
    cd /d "%current_dir%\build"
    main.exe
    echo ---------------------------------------------------------------------------}
)

echo completed...
pause
endlocal