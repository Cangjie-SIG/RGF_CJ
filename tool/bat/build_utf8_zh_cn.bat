@echo off
:: 更改代码页为 UTF-8 (65001)
chcp 65001 >nul
setlocal enabledelayedexpansion

cd /d "%cd%\..\.."

REM 获取当前目录
set "current_dir=%cd%"

:: 设置目标目录路径
set "testDir=%current_dir%\test"

if not exist "%current_dir%\build\" (
    REM 构建编译结果目录
    mkdir "%current_dir%\build"
) else (
    echo --------------------------------- 清理中 -----------------------------------{
    :: 清除本层中的Exe
    if exist "%current_dir%\build\*.exe" (
        del /F /Q "%current_dir%\build\*.exe"
        echo 清理EXE可执行文件
    )
    :: 清除本层中的Dll
    if exist "%current_dir%\build\*.dll" (
        del /F /Q "%current_dir%\build\*.dll"
        echo 清理DLL动态链接库文件
    )
    echo ----------------------------------------------------------------------------}
    echo 清理完成
    echo.
)


REM 判断 RGF 是否已经编译
if exist "%current_dir%\target\release\rgf\*.cjo" (
    echo RGF库已编译

    :: 提示用户输入逻辑值
    :prompt
    set "choiceRC="
    set /p choiceRC="是否重新编译？ (Y/N):" 

    :: 检查用户输入
    if /i "!choiceRC!"=="Y" (
        goto recompile
    ) else if /i "!choiceRC!"=="N" (
        echo 跳过编译
    ) else (
        echo 无效值
        goto prompt
    )
) else (
    :recompile
    echo ----------------------------- 编译 RGF -------------------------------{
    cjpm update
    cjpm build
    echo ----------------------------------------------------------------------}
    echo RGF 编译完成
    echo.
)

:: 检查test是否存在
if not exist "%testDir%" (
    echo test目录不存在: %testDir%
    pause
    exit /b
)

:: 初始化变量
set count=0

:: 递归查找包含 cjpm.toml 文件的子目录
echo 检索test项目路径...
echo ------------------------------ 测试列表 ------------------------------------{
for /r "%testDir%" %%D in (.) do (
    if exist "%%D\cjpm.toml" (
        set /a count+=1
        set "dir!count!=%%~fD"
        echo !count!: %%~fD
    )
)
echo ----------------------------------------------------------------------------}
echo.

:: 检查是否找到了任何符合条件的子目录
if %count% equ 0 (
    echo 未找到任何test项目路径
    pause
    exit /b
)

:: 提供选择
set /p choice="请选择 (序号):"

:: 验证用户输入
if %choice% gtr %count% (
    echo 无效的选项值
    pause
    exit /b
)
if %choice% lss 1 (
    echo 无效的选项值
    pause
    exit /b
)

:: 显示用户选择的子目录
set "selectedDir=!dir%choice%!"
echo [已选: %selectedDir%]
echo.
:: 打开所选文件夹
cd /d "!dir%choice%!"

:: 编译测试
echo ----------------------------- 编译test项目 --------------------------------{
    cjpm update
    cjpm build
echo ---------------------------------------------------------------------------}
echo [!dir%choice%!] test 编译完成.
echo.
echo ------------------------------- 复制结果 ----------------------------------{
:: 拷贝Exe
echo "!dir%choice%!\target\release\bin"
cd /d "!dir%choice%!\target\release\bin"
for /r %%i in (*.exe) do (
    echo 复制 %%i 到 "%current_dir%\build"
    copy "%%i" "%current_dir%\build" /Y
)
:: 检查DLL
if not exist "%current_dir%\libs\libRgf.dll" (
    echo 'libRgf.dll' 未找到, 跳过复制.
) else (
    :: 拷贝DLL
    echo 复制 libRgf.dll 到 "%current_dir%\build"
    copy "%current_dir%\libs\libRgf.dll" "%current_dir%\build" /Y
)
echo ---------------------------------------------------------------------------}
echo.

:: 是否执行 main.exe
set /p runExe="是否执行 main.exe (Y/N):"
echo.
:: 检查用户输入
if /i "!runExe!"=="Y" (
    echo ---------------------------------- Main ----------------------------------{
    cd /d "%current_dir%\build"
    main.exe
    echo ---------------------------------------------------------------------------}
) else if /i "!runExe!"=="y" (
    echo ---------------------------------- Main ----------------------------------{
    cd /d "%current_dir%\build"
    main.exe
    echo ---------------------------------------------------------------------------}
)

echo 完成...
pause
endlocal