# [1.3.1]

## Bugfix

+ **DLL 修复文件加载部分对象未释放问题**

## Feature
+ **DLL 增加API rApiMemBmpClassTraversePixels**

+ MemBitmap 增加 traversePixels 函数

# [1.2.2]

## Feature
+ 完成对 0.59.6 版本适配（但并非普适的方法，不建议基于此版本开发）

# [1.2.1]

## Bugfix

+ 修复 Layer 宏对象指针多继承接口缺失

# [1.2.0]

## Feature
+ **DLL 增加API rApiMemBmpClassSaveBitmapToFile**
+ **DLL 增加API rApiSfcutsClassSaveBitmapToMemory**
+ **DLL 增加API rApiMemBmpClassSaveLayerToFile**
+ **DLL 增加API rApiSfcutsClassSaveLayerToMemory**

+ Renderer 增加 saveImgToFile 重载函数

# [1.1.0]

## Feature
+ **DLL 增加API rApiMemBmpClassSaveToFile**
+ **DLL 增加API rApiSfcutsClassSaveSurfaceToMemory**

+ Renderer 增加 saveImgToFile 重载函数
+ MemBitmap 增加 saveToFile 函数

# [1.0.2]

## Feature
+ 增加 @Dependent() 全局依赖宏

# [1.0.1]

## Feature

+ 适配 0.58.3 版本编译器

# [1.0.0]

## Feature

+ 增加 @Paint 宏错误调用链编译期错误提示
+ 一定程度上适配了代码在 cangjie 0.53.4 版本上的适配度

## Bugfix

+ 修复 @Paint 宏错误嵌套调用链的参数补齐

# [1.0.0]

## Feature

+ 添加 @Painter 和 @Paint 宏，自动管理绘制方法
+ 添加生命周期管理宏 @LifeCycle，自动接管设备相关资源、设备无关资源
+ 添加跳过宏 @Skip，跳过接管设备无关资源
+ 添加置换宏 @Abbr，允许以 # + H16 值表示色彩
+ 添加事件钩子宏 @HookProc，允许自动挂钩事件
+ 添加绘制者宏 @Painter，自动包装绘制方法
+ 添加 IEventMethod 接口，扩展窗口类事件回调
+ 新增大量类、枚举、接口
+ C++侧引入原子锁以及超时保护机制

## Bugfix

+ 解决由于仓颉侧用户态线程切换导致的渲染流程崩溃问题

<br>

# [0.57.3]

## Feature

+ 添加桌面光标定位获取方法
+ 添加根据坐标获取所属显示器工作区宽高方法

<br>

# [0.55.7]

## Feature

+ 追加 RgfBindRenderer 实现绑定式渲染

## Bugfix

+ 修改C++侧主窗口标志初始值未初始化导致的随机跳出消息循环问题

## Remove

+ Remove 移除独立的 LayoutWindow 接口，改为统一创建方法
