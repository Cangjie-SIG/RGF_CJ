<div align="center">
<img alt="" src="./doc/assets/Logo_D_64.png" style="display: inline-block;"/>
<h1>RGF_CJ</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-1.3.2-red" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.58.3-yellow" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-50+%25-blue" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/state-孵化-green" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/domain-Computer_Graphics-8A2BE2" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/Apache_2.0_License-gold" style="display: inline-block;" />
</p>

## 介绍

&nbsp;&nbsp;&nbsp;&nbsp;RGF是一个Windows系统下基于Direct3D、Direct2D、DXGI、DirectWrite、WIC、GDI、GDIplus等技术开发的通用渲染框架。RGF仓颉版（后续简称"RGF"）基于RGF(C/C++版)封装优化而来。RGF为开发者提供轻量化、安全、高性能以及高度一致性的2D渲染能力，并且提供对接Direct3D的相关接口，以满足开发者对3D画面渲染的需求。

### 视频教程

本项目长期维护，关注 [链接 Bilibili UID:3546853029185667](https://space.bilibili.com/3546853029185667)，观看视频教程与最新动态 !

### 项目特性

- 高度一致性的跨技术渲染结果

&nbsp;&nbsp;&nbsp;&nbsp;RGF支持用户按需选择底层技术实现，目前提供Direct2D和GDI/GDI+两种选择。当用户基于RGF框架完成开发后，可以无感切换到另一种底层技术实现中。RGF将会保障渲染结果的一致性。

- 渲染目标高度兼容

&nbsp;&nbsp;&nbsp;&nbsp;RGF支持各类窗口类型，包括但不限于：普通窗口、弹出式窗口、分层窗口（半透明）、异形窗口等。并且RGF支持基于绑定式渲染表面将内容渲染到非客户区（NC）或任意支持HDC的设备（例如：打印机）。

- 安全的使用机制

&nbsp;&nbsp;&nbsp;&nbsp;RGF采用生命周期的设计理念，将相关对象的创建、使用、销毁规范管理，力保开发者不会造成内存泄漏等问题。RGF内置生命周期管理宏，可实现自动管理相关对象的释放。

- 平台原生

&nbsp;&nbsp;&nbsp;&nbsp;RGF专注于Windows操作系统，无跨平台相关的冗余兼容代码和性能浪费。

- 线程安全

&nbsp;&nbsp;&nbsp;&nbsp;RGF核心渲染流程由C++侧原子操作保护，确保在系统多线程以及仓颉用户态线程下始终安全渲染。此外，锁机制带有超时保护，当出现极端意外情况时自动释放锁，避免程序长时间卡死。

### 项目计划

 - v1.4.0 

&nbsp;&nbsp;&nbsp;&nbsp;计划向RGF中引入特效管理，以满足开发者对图像特殊处理的需求，包括但不限于：高斯模糊等

 - v2.0.0

&nbsp;&nbsp;&nbsp;&nbsp;计划基于DirectX 12实现RGF v2.0，以满足开发者的更多使用需求

*(注' 以上仅为粗略计划，详细计划后续将会逐步公开)*

## 项目架构

<img alt="" src="./doc/assets/architecture.png" style="display: inline-block;" width=100%/>

### 源码目录

```shell
 .
 ├┈.gitignore ----------------------------------# GIT 忽略描述
 ├┈CHANGELOG.md --------------------------------# 修改更新日志
 ├┈cjpm.lock
 ├┈cjpm.toml
 ├┈LICENSE -------------------------------------# 开源许可描述
 ├┈README.md -----------------------------------# 整体介绍
 ├┈README.OpenSource ---------------------------# 开源描述
 ├┈build*.bat ----------------------------------# 自动编译脚本
 ├─doc/ ----------------------------------------# 文档目录，包括设计文档，API接口文档等
 │  └─assets/ ----------------------------------# 资源文件
 ├─libs/ ---------------------------------------# 相关库文件
 │  ├┈*.a
 │  ├┈*.lib
 │  ├┈*.dll
 ├─src/ ----------------------------------------# 源码目录
 │  ├┈rgf.cj
 │  └─rgf_core/ --------------------------------# RGF库核心
 │     └─win_def/ ------------------------------# Windows平台接口封装
 └─test/ ---------------------------------------# 测试相关源码
```

### 接口说明

主要类和函数接口说明，详见 [API](./doc/feature_api.md)


## 使用说明
### 编译构建

*(注‘ 本项目提供Build脚本进行自动化编译，编译结果自动置入Build文件夹中。使用自动化脚本前，请确保已经完整安装Cangjie编译环境，并且正确配置环境变量)*

1. 选择并下载合适的 Release/Debug 版本库，重命名为`libRgf.dll`后，置入`./libs/`目录

2. *(非必须)选择合适版本的静态库`libuser32.a`、`libimm32.a`置入`./libs/`目录*

3. 确认`cjpm.toml`中的库路径配置与链接配置

>指定库路径：-L ./libs<br/>
>链接静态库：-l user32 -l imm32<br/>
>链接项目库：-l:libRgf.dll<br/>
>指定子系统：-subsystem windows

*(注‘ 若采用静态链接库，则至少应确保程序链接列举的所有库`-l:libRgf.a -l stdc++ -l gcc -l user32 -l imm32 -l gdi32 -l ole32 -l gdiplus -l dwrite -l d3d10_1 -l dxgi -l d2d1 -l windowscodecs -l msimg32"`)*

4. 配置异常抛出语言 *(注' 可跳过，默认英文)*

>英文：--cfg \"RGF_LANG=en\"<br/>
>中文：--cfg \"RGF_LANG=zh-cn\"

5. 编译

```shell
cjpm update
cjpm build
```

### 功能示例

*注' 详细使用示例请等待后续整理的开发者文档*

#### RGF库的基础使用 功能示例

1. 构建窗口环境，获得RGF上下文

    示例代码如下：

    ```cangjie
    // 1.引入RGF库
    import rgf.rgf_ui.*
    // 2.进入主方法
    rgfMain(
        {context:RgfContext=>
            //3. 注册窗口类
            //4. 创建窗口
        },
        {context:RgfContext=>
            //5. 释放窗口 (可省略)
            //6. 注销窗口类
        }
    )
    ```

2. 注册窗口类

    示例代码如下：<br/>
    注册窗口类部分的传参请参考 [链接](https://learn.microsoft.com/zh-cn/windows/win32/intl/registering-window-classes)

    ```cangjie
    // 此变量置于RgfMain函数之前的公共域内
    let winClass:RgfWinContext = RgfWinContext()
    // 此方法置入RgfMain函数第一个Lambda中
    rgfRegisterWinClass(winClass,"窗口类名",context,/* 画刷 */,/* style */)
    ```

3. 创建自己的窗口类

    示例代码如下：<br/>
    创建窗口部分的传参请参考 [链接](https://learn.microsoft.com/zh-cn/windows/win32/learnwin32/creating-a-window)

    ```cangjie
    // 窗口类
    class example <: RgfBase {
        // 内部实现自己的逻辑
    }
    // 此变量置于RgfMain函数之前的公共域内
    let winExample:example = example()
    // 将以下创建方法置入RgfMain函数第一个Lambda中
    winExample.createWin(winClass,"窗口名",左边,顶边,宽度,高度,父窗口句柄,窗口扩展风格,窗口风格,
        context,substrate:RgfSubstrate.底层技术选择,layered:是否半透明
    )
    ```

4. 释放资源

    程序执行结束后需要释放注册的资源，示例代码如下：<br/>

    ```cangjie
    /* 
     * 此方法置入RgfMain函数第二个Lambda中
     * 当主窗口释放时，窗口类实际已经释放。
     * 此处写出只是为了确保生命周期的完整性
     * 若需要确保窗口被释放或手动释放窗口，
     * 可以在必要时调用以下方法。
    */
    winExample.destroyWin();
    // 此方法置入RgfMain函数第二个Lambda中
    rgfUnregisterWinClass(winClass)
    ```

## 约束与限制

<table>
	<tr>
		<th>Windows 7</th>
		<th>Windows 8</th>
		<th>Windows 10</th>
		<th>Windows 11</th>
	</tr>
	<tr>
		<th>√</th>
		<th>√</th>
		<th>√</th>
		<th>√</th>
	</tr>
 </tr>
</table>

- RGF不同库版本所兼容的运行环境略有差异，具体差异如下：

 	- D3D兼容版(DX10)

 	>本版本最低支持<br/>
 	>`Windows 7、带 SP2 的 Windows Vista 和适用于 Windows Vista 的平台更新 [桌面应用 |UWP 应用]` <br/>
 	>并且后续支持与Direct3D互操作
 
 	- D3D兼容版(DX11)

 	>本版本最低支持<br/>
 	>`适用于 Windows 7 的Windows 8和平台更新 [桌面应用 |UWP 应用]` <br/>
 	>并且后续支持与Direct3D互操作
 
 	- 非D3D兼容版(D2D 1.0)

 	>本版本最低支持<br/>
 	>`Windows 7、带 SP2 的 Windows Vista 和适用于 Windows Vista 的平台更新 [桌面应用 |UWP 应用]` <br/>

 	- 非D3D兼容版(D2D 1.1)

 	>本版本最低支持<br/>
 	>`适用于 Windows 7 的Windows 8和平台更新 [桌面应用 |UWP 应用]` <br/>

 	- 屏蔽版本(暂无实现计划)

 	>本版本最低支持<br/>
 	>`Windows XP、Windows 2000 Professional [仅限桌面应用]`<br/>
 	>但本版本将会限制仅使用底层 GDI+ 技术，并且剔除 DirectWrite 的文本渲染能力

*(注' 以上各版本自身还区分启用并行加速和未启用并行加速、32位版本和64位版本)*

## 开源协议

详见文件：LICENSE

## 首版作者信息 ##

*（添加请备注：“RGF”）*<br/>
@Author：饶子俊<br/>
@WeChat：*<br/>
@QQ    ：2874148643<br/>
@E-mail: 2874148643@qq.com<br/>

## 参与贡献

欢迎大家提交PR、Issue，欢迎大家参与任何形式的贡献。