@echo off
setlocal enabledelayedexpansion
echo ... clearDependency.bat ...
REM 获取当前目录
set "current_dir=%~dp0"

cd /d "%current_dir%..\.."

REM 获取项目主目录
set "main_dir=%cd%"

:: 拷贝DLL
echo Del "%current_dir%target\release\bin\libRgf.dll"
del /Q "%current_dir%target\release\bin\libRgf.dll"

echo ... clearDependency.bat ...
endlocal